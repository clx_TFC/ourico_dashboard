/**
 * Functions to load data to be presented in the Dashboard
 */

import * as Common from '../api/common'
import * as Data   from '../api/data'


/**
 * Load client and mote statistics
 *
 * @param {string}   from             initial date to consider
 * @param {string}   to               final date to consider
 * @param {string[]} excluded_motes   motes to exlcude from query
 * @param {string[]} excluded_clients clients to exlcude from query
 *
 * @return object
 */
export async function load (from, to, excluded_motes, excluded_clients) {
    let filter = [
        { 'date_time': { '>=': from } },
        { 'date_time': { '<=': to   } }
    ]

    let filter_motes = []
    if (excluded_motes !== undefined && excluded_motes.length !== 0)
        filter_motes = [{ 'id': { 'notin': excluded_motes } }]

    let filter_clients = []
    if (excluded_clients !== undefined && excluded_clients.length !== 0)
        filter_clients = [{ 'id': { 'notin': excluded_clients } }]

    let query = { query: [
         { // mote query
            'resource': Data.Resources.Mote.name,
            'fields': [
                Data.Resources.Mote.fields.id,
                Data.Resources.Mote.fields.name,
                Data.Resources.Mote.fields.active
            ],
            'filters': filter_motes,
            'options': [ { 'ordasc': Data.Resources.Mote.fields.name } ],
            'include': [
                {
                    // include the packets received in the interval considered
                    'resource': Data.Resources.Mote.related.packets,
                    'filters'  : filter
                },
                {
                    // include the errors received in the interval considered
                    'resource': Data.Resources.Mote.related.errors,
                    'filters'  : filter
                }
            ]
        },
        { // users
            'resource': Data.Resources.Client.name,
            'fields': [
                Data.Resources.Client.fields.id,
                Data.Resources.Client.fields.name
            ],
            'filters': filter_clients,
            'options': [ { 'ordasc': Data.Resources.Mote.fields.name } ],
            'include' : [
                {
                    // include the client logs
                    'resource': Data.Resources.Client.related.logs,
                    'filters'  : filter
                }
            ]
        },
        { // applications
            'resource': Data.Resources.ApiClient.name,
            'fields': [
                Data.Resources.ApiClient.fields.id,
                Data.Resources.ApiClient.fields.name
            ],
            'filters': filter_clients,
            'include' : [
                {
                    // include the client logs
                    'resource': Data.Resources.ApiClient.related.logs,
                    'filters'  : filter
                }
            ]
        },
    ]}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    console.log(results.data)
    return format(results.data)
}


/**
 * Format data the way it is intended to be presented
 */
function format (data) {
    let result = {
        mote_data: {
            summary: {
                nr_active       : 0,
                nr_inactive     : 0,
                total_rx_packets: 0,
                total_errors    : 0
            },
            motes: []
        },
        client_data: {
            summary: {
                nr_users           : 0,
                nr_apps            : 0,
                total_rx_requests  : 0,
                total_rejected_reqs: 0
            },
            clients: []
        }
    }

    let motes = data[0]
    for (let i = 0; i < motes.length; i++) {
        let mote = motes[i]

        if (mote.active)
            result.mote_data.summary.nr_active++
        else
            result.mote_data.summary.nr_inactive++

        result.mote_data.summary.total_rx_packets += mote.packets.length
        result.mote_data.summary.total_errors     += mote.errors.length

        result.mote_data.motes.push(mote)
    }

    let users = data[1]
    for (let i = 0; i < users.length; i++) {
        let user = users[i]

        result.client_data.summary.nr_users++
        result.client_data.summary.total_rx_requests += user.logs.length
        result.client_data.summary.total_rejected_reqs += user.logs.filter((log) => {
            return (log.level !== 'info')
        }).length

        result.client_data.clients.push(user)
    }

    let apps  = data[2]
    for (let i = 0; i < apps.length; i++) {
        let app = apps[i]

        result.client_data.summary.nr_apps++
        result.client_data.summary.total_rx_requests += app.logs.length
        result.client_data.summary.total_rejected_reqs += app.logs.filter((log) => {
            return (log.level !== 'info')
        }).length

        result.client_data.clients.push(app)
    }

    return result
}


/**
 * Group data by hour, day, week or month and
 * sum the values in the same group
 *
 * @param {string}   by    'hour', 'day', 'week' or 'month'
 * @param {object}   data  should have attr date_time
 * @param {function} count gets the value to be added to group for each item
 */
export function count_data_by_date (by, data, count) {

    // to hold function to check if two dates
    // are in the same group (same hour, or same day, ...)
    let same = undefined

    switch (by) {
    case 'hour':
        same = (p, c) => {
            // let p = Moment(prev)
            // let c = Moment(curr)
            return (
                p.hour()  === c.hour()  &&
                p.date()  === c.date()  &&
                p.month() === c.month() &&
                p.year()  === c.year()
            )
        }
        break

    case 'day':
        same = (p, c) => {
            // let p = Moment(prev)
            // let c = Moment(curr)
            return (
                p.date()  === c.date()  &&
                p.month() === c.month() &&
                p.year()  === c.year()
            )
        }
        break

    case 'week':
        same = (p, c) => {
            // let p = Moment(prev)
            // let c = Moment(curr)
            return (
                p.date()  === c.date()  &&
                p.month() === c.month() &&
                p.year()  === c.year()
            )
        }
        break

    case 'month':
        same = (p, c) => {
            // let p = Moment(prev)
            // let c = Moment(curr)
            return (
                p.month() === c.month() &&
                p.year()  === c.year()
            )
        }
        break

    default:
        // eslint-disable-next-line
        throw `Expected values for \`by\` are "hour", "day", "week" or "month" received "${by}"`
    }

    // make representation of a group
    // showing only relevant parts
    let repr = (representation) => {
        // let representation = Moment(dt)
        representation.seconds(0)
        representation.minutes(0)
        // show just hour day month and year
        if (by === 'hour') return representation.format('HH:00 DD-MM-YY')
        representation.hours(0)
        // show just day month and year
        if (by === 'day') return representation.format('DD-MM-YYYY')
        // show just day month and year
        representation.day(0)
        if (by === 'week') return representation.format('DD-MM-YYYY')
        // show just month and year
        return representation.format('MMM YYYY')
    }

    let result = { values: [], labels: [] }
    if (data.length === 0)
        return result

    let prev = data[0]
    let p_repr = repr(prev.date_time)

    let nr_points = 0
    result.labels[nr_points] = p_repr
    result.values[nr_points] = count(prev)

    // sum values in same group
    for (let i = 1; i < data.length; i++) {
        let curr = data[i]
        if (same(prev.date_time, curr.date_time)) { // same group
            result.values[nr_points] += count(curr)
        } else {
            prev = curr
            p_repr = repr(prev.date_time)
            nr_points++
            result.labels[nr_points] = p_repr
            result.values[nr_points] = count(prev)
        }
    }

    return result
}