/**
 * Functions to store and retrieve application settings
 */


/**
 * Dashboard settings
 */
const Dashboard = {
    Defaults: {
        SHOW_INACTIVE_MOTES           : true,
        DATA_REFRESH_RATE             : 10000, // 10 seconds
        MOTE_PACKETS_CHART_GROUP_BY   : 'day',
        MOTE_BYTES_CHART_GROUP_BY     : 'day',
        CLIENT_REQUESTS_CHART_GROUP_BY: 'day'
    },
    // whether or not inactive motes should be presented
    // in the dashboard
    SHOW_INACTIVE_MOTES: 'ourico.settings.dashboard.show_inactive_motes',
    // period to referesh data presented in dashboar
    DATA_REFRESH_RATE  : 'outico.settings.dashboard.data_refresh_rate',
    // how to display mote charts
    MOTE_PACKETS_CHART_GROUP_BY   : 'outico.settings.dashboard.mote_packets_chart_group_by',
    MOTE_BYTES_CHART_GROUP_BY     : 'outico.settings.dashboard.mote_bytes_chart_group_by',
    CLIENT_REQUESTS_CHART_GROUP_BY: 'outico.settings.dashboard.client_requests_chart_group_by'
}


/**
 * Global settings
 */
const Global = {
    Defaults: {
        RECEIVE_EVENTS: false
    },
    RECEIVE_EVENTS: 'ourico.settings.global.receive_events'
}



/**
 * Default values of all the settings
 */
let Defaults = {}

Defaults[Global.RECEIVE_EVENTS]                    = Global.Defaults.RECEIVE_EVENTS

Defaults[Dashboard.SHOW_INACTIVE_MOTES]            = Dashboard.Defaults.SHOW_INACTIVE_MOTES
Defaults[Dashboard.DATA_REFRESH_RATE]              = Dashboard.Defaults.DATA_REFRESH_RATE
Defaults[Dashboard.MOTE_PACKETS_CHART_GROUP_BY]    = Dashboard.Defaults.MOTE_PACKETS_CHART_GROUP_BY
Defaults[Dashboard.MOTE_BYTES_CHART_GROUP_BY]      = Dashboard.Defaults.MOTE_BYTES_CHART_GROUP_BY
Defaults[Dashboard.CLIENT_REQUESTS_CHART_GROUP_BY] = Dashboard.Defaults.CLIENT_REQUESTS_CHART_GROUP_BY


export function get (setting) {
    let value = null
    try {
        value = window.localStorage.getItem(setting)
    } catch (error) {
        console.error('Unable to use local storage', error)
    }
    return value !== null ? value : Defaults[setting]
}

export function set (setting, value) {
    try {
        window.localStorage.setItem(setting, value)
    } catch (error) {
        console.error('Unable to use local storage', error)
    }
}


export { Dashboard, Global }