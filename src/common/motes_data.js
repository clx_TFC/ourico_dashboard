import * as Common from '../api/common'
import * as Data   from '../api/data'



/**
 * Get a group of motes
 * 
 * @param {number} skip how many items to skip 
 * @param {number} take how many items to return
 * 
 * @return {object}
 * @throws {string}
 */
export async function load (skip, take) {
    let query = { query: {
        "resource": Data.Resources.Mote.name,
        "fields"  : Data.Resources.Mote.fields.all,
        "options" : [
            { 'skip': skip }, { 'take': take },
            { 'ordasc': Data.Resources.Mote.fields.id }
        ]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]
}


/**
 * Get all motes
 * 
 * @return {object}
 * @throws {string}
 */
export async function load_all () {
    let query = { query: {
        "resource": Data.Resources.Mote.name,
        "fields"  : Data.Resources.Mote.fields.all,
        "options" : [
            { 'ordasc': Data.Resources.Mote.fields.id }
        ]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]
}


/**
 * Create or update a mote
 * 
 * @param {object}  mote   the data to save
 * @param {boolean} update if true entry will be updated
 * 
 * @return {boolean} true if saved successfuly
 */
export async function save_mote (mote, update=false) {
    let op_data = {
        resource: Data.Resources.Mote.name,
        data    : mote
    }

    let results

    if (update) {
        op_data.where = [
            { [Data.Resources.Mote.fields.id]: mote.id }
        ]
        console.log('op_data', op_data)
        op_data.data = {
            [Data.Resources.Mote.fields.name]                   : mote.name,
            [Data.Resources.Mote.fields.location_lat]           : mote.location_lat,
            [Data.Resources.Mote.fields.location_lng]           : mote.location_lng,
            [Data.Resources.Mote.fields.max_time_with_no_report]: mote.max_time_with_no_report,
            [Data.Resources.Mote.fields.crypto_alg]             : mote.crypto_alg,
            [Data.Resources.Mote.fields.crypto_key]             : mote.crypto_key,
            [Data.Resources.Mote.fields.other_data]             : mote.other_data,
            [Data.Resources.Mote.fields.purpose_description]    : mote.purpose_description,
        }
        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Delete a mote
 * 
 * @param {numer} mote_id id of the mote
 */
export async function delete_mote (mote_id) {
    let op_data = {
        resource: Data.Resources.Mote.name,
        where: [
            { [Data.Resources.Mote.fields.id]: mote_id }
        ]
    }

    let results =
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Loads configurations, connections, sensors, actuators, alerts and triggers
 * associated with a mote
 * 
 * @param {number} mote_id
 * 
 * @return {object[]}
 * @throws {string}
 */
export async function load_details (mote_id) {

    let filter = {
        [Data.Resources.Mote.fields.id]: mote_id
    }

    let query = { query: {
        'resource': Data.Resources.Mote.name,
        'fields'  : [ Data.Resources.Mote.fields.id ],
        'filters' : [filter],
        'include' : [
            {
                'resource': Data.Resources.Mote.related.configurations,
                'fields'  : Data.Resources.MoteConfiguration.fields.all,
            },
            {
                'resource': Data.Resources.Mote.related.conns_from_here,
                'fields'  : Data.Resources.MoteConnection.fields.all,
                'include' : [{ 
                    'resource': Data.Resources.MoteConnection.related.mote_there,
                    'fields': [ Data.Resources.Mote.fields.id, Data.Resources.Mote.fields.name ]
                 }]
            },
            {
                'resource': Data.Resources.Mote.related.conns_to_here,
                'fields'  : Data.Resources.MoteConnection.fields.all,
                'include' : [{ 
                    'resource': Data.Resources.MoteConnection.related.mote_here,
                    'fields'  : [ Data.Resources.Mote.fields.id, Data.Resources.Mote.fields.name ]
                 }]
            },
            {
                'resource': Data.Resources.Mote.related.sensors,
                'fields'  : Data.Resources.SensorUse.fields.all,
                'include' : [{
                    'resource': Data.Resources.SensorUse.related.sensor,
                    'fields'  : Data.Resources.Sensor.fields.all
                }]
            },
            {
                'resource': Data.Resources.Mote.related.actuators,
                'fields'  : Data.Resources.ActuatorUse.fields.all,
                'include' : [{
                    'resource': Data.Resources.ActuatorUse.related.actuator,
                    'fields'  : Data.Resources.Actuator.fields.all
                }]
            },
            {
                'resource': Data.Resources.Mote.related.alerts,
                'fields'  : Data.Resources.AlertUse.fields.all,
                'include' : [{
                    'resource': Data.Resources.AlertUse.related.alert,
                    'fields'  : Data.Resources.Alert.fields.all
                }]
            },
            {
                'resource': Data.Resources.Mote.related.triggers,
                'fields'  : Data.Resources.TriggerUse.fields.all,
                'include' : [{
                    'resource': Data.Resources.TriggerUse.related.trigger,
                    'fields'  : Data.Resources.Trigger.fields.all
                }]
            }
        ]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))
    

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0][0]
}


/**
 * Load the measurements from a group of sensors in a mote
 * in an interval of time
 * 
 * @param {number} mote_id id of mote
 * @param {string} from    initial date to consider
 * @param {string} to      final date to consider
 * 
 * @return {object[]}
 * @throws {string}
 */
export async function load_measurements (mote_id, from, to) {
    
    let filter = {
        [Data.Resources.SensorUse.fields.mote_id]: mote_id
    }

    let query = { query: {
        'resource': Data.Resources.SensorUse.name,
        'fields'  : [ Data.Resources.SensorUse.fields.num ],
        'filters' : [filter],
        'include' : [{
            'resource': Data.Resources.SensorUse.related.sensor,
            'fields'  : Data.Resources.Sensor.fields.all,
            'include' : [{
                'resource': Data.Resources.Sensor.related.parameters,
                'fields'  : Data.Resources.Parameter.fields.all,
                'include' : [{
                    'resource': Data.Resources.Parameter.related.measurements,
                    'fields'  : Data.Resources.Measurement.fields.all,
                    'filters' : [
                        { 'date_time': { '>=': from } },
                        { 'date_time': { '<=': to } }
                    ]
                }]
            }]
        }]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]    

}


/**
 * Load status reports, errors, alerts, triggers and action results
 * from a group of sensors in a mote in an interval of time
 * 
 * @param {number} mote_id id of mote
 * @param {string} from    initial date to consider
 * @param {string} to      final date to consider
 * 
 * @return {object[]}
 * @throws {string}
 */
export async function load_extra_data (mote_id, from, to) {

    let filter = [
        { 'mote_id': mote_id }
    ]

    let filter_2 = [
        { 'date_time': { '>=': from } },
        { 'date_time': { '<=': to   } }
    ]

    let query = { query: [
        { // action results
            'resource': Data.Resources.ActuatorUse.name,
            'fields': [Data.Resources.ActuatorUse.fields.num],
            'filters': filter,
            'include': [
                {
                    'resource': Data.Resources.ActuatorUse.related.results,
                    'fields'  : Data.Resources.ActionResult.fields.all,
                    'filters' : filter_2,
                    'include' : [{
                        'resource': Data.Resources.ActionResult.related.action,
                        'fields'  : Data.Resources.Action.fields.all
                    }]
                },
                {
                    'resource': Data.Resources.ActuatorUse.related.actuator,
                    'fields'  : Data.Resources.Actuator.fields.all
                }
            ]
            
        },
        { // status reports
            'resource': Data.Resources.MoteStatusReport.name,
            'fields'  : Data.Resources.MoteStatusReport.fields.all,
            'filters' : filter
        },
        { // errors
            'resource': Data.Resources.MoteError.name,
            'fields'  : Data.Resources.MoteError.fields.all,
            'filters' : filter
        },
        { // alerts
            'resource': Data.Resources.AlertUse.name,
            'fields'  : Data.Resources.AlertUse.fields.all,
            'filters' : filter,
            'include' : [
                {
                    'resource': Data.Resources.AlertUse.related.occurrences,
                    'fields'  : Data.Resources.AlertOccurrence.fields.all
                },
                {
                    'resource': Data.Resources.AlertUse.related.alert,
                    'fields'  : Data.Resources.Alert.fields.all
                }
            ]
        },
        { // triggers
            'resource': Data.Resources.TriggerUse.name,
            'fields'  : Data.Resources.TriggerUse.fields.all,
            'filters' : filter,
            'include' : [
                {
                    'resource': Data.Resources.TriggerUse.related.occurrences,
                    'fields'  : Data.Resources.TriggerOccurrence.fields.all
                },
                {
                    'resource': Data.Resources.TriggerUse.related.trigger,
                    'fields'  : Data.Resources.Trigger.fields.all
                }
            ]
        }
    ]}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return ({
        action_results: results.data[0],
        status_reports: results.data[1],
        errors        : results.data[2],
        alerts        : results.data[3],
        triggers      : results.data[4],
    })
}


/**
 * Get all the alerts that are not being used in a mote
 * 
 * @param {number} mote_id id of the mote
 */
export async function load_non_used_alerts (mote_id) {
    let query = { query: {
        resource: Data.Resources.Alert.name,
        fields  : Data.Resources.Alert.fields.all,
        include : [{
            resource: Data.Resources.Alert.related.uses,
            filters : [{
                [Data.Resources.AlertUse.fields.mote_id]: { "=": mote_id }
            }],
        }]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }
    
    return results.data[0]   
}


/**
 * Get all the triggers that are not being used in a mote
 * 
 * @param {number} mote_id id of the mote
 */
export async function load_non_used_triggers (mote_id) {
    let query = { query: {
        resource: Data.Resources.Trigger.name,
        fields  : Data.Resources.Trigger.fields.all,
        include : [{
            resource: Data.Resources.Trigger.related.uses,
            filters : [{
                [Data.Resources.TriggerUse.fields.mote_id]: { "=": mote_id }
            }]
        }]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]   
}


/**
 * Get all the sensors that are not being used in a mote
 * 
 * @param {number} mote_id id of the mote
 */
export async function load_non_used_sensors (mote_id) {
    let query = { query: {
        resource: Data.Resources.Sensor.name,
        fields  : Data.Resources.Sensor.fields.all,
        include : [{
            resource: Data.Resources.Sensor.related.uses,
            filters : [{
                [Data.Resources.SensorUse.fields.mote_id]: { "=": mote_id }
            }],
        }]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]   
}


/**
 * Get all the actuators that are not being used in a mote
 * 
 * @param {number} mote_id id of the mote
 */
export async function load_non_used_actuators (mote_id) {
    let query = { query: {
        resource: Data.Resources.Actuator.name,
        fields  : Data.Resources.Actuator.fields.all,
        include : [{
            resource: Data.Resources.Actuator.related.uses,
            filters : [{
                [Data.Resources.ActuatorUse.fields.mote_id]: { "=": mote_id }
            }]
        }]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]   
}



/**
 * Save or update a mote configuration
 * 
 * @param {num}     mote_id if of the mote
 * @param {object}  configuration data of the configuration
 * @param {boolean} update wheter to update or create a new one
 * 
 * @return {boolean} true if successful
 * @throws {string}
 */
export async function save_configuration (mote_id, configuration, update=false) {

    let op_data = {
        resource: Data.Resources.MoteConfiguration.name,
        data    : configuration
    }

    let results

    if (update) {
        op_data.where = [
            { [Data.Resources.MoteConfiguration.fields.mote_id]: mote_id            },
            { [Data.Resources.MoteConfiguration.fields.name]   : configuration.name }
        ]
        console.log('op_data', op_data)
        op_data.data = {
            [Data.Resources.MoteConfiguration.fields.active]              : configuration.active,
            [Data.Resources.MoteConfiguration.fields.use_crypto]          : configuration.use_crypto,
            [Data.Resources.MoteConfiguration.fields.send_data_period]    : configuration.send_data_period,
            [Data.Resources.MoteConfiguration.fields.measurement_period]  : configuration.measurement_period,
            [Data.Resources.MoteConfiguration.fields.status_report_period]: configuration.status_report_period
        }
        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Delete a configuration from a mote
 * 
 * @param {number} mote_id   id of the mote
 * @param {number} conf_name name of the configuration
 */
export async function delete_configuration (mote_id, conf_name) {
    let op_data = {
        resource: Data.Resources.MoteConfiguration.name,
        where   : [
            { [Data.Resources.MoteConfiguration.fields.mote_id]: mote_id   },
            { [Data.Resources.MoteConfiguration.fields.name]   : conf_name }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Save or update a connection
 * 
 * @param {num}     mote_id    if of the mote
 * @param {object}  connection data of the connection
 * 
 * @return {boolean} true if successful
 * @throws {string}
 */
export async function save_connection (mote_id, connection) {

    let op_data = {
        resource: Data.Resources.MoteConnection.name,
        data    : connection
    }

    let results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Delete a connection between motes
 * 
 * @param {number} mote_id   id of the mote
 * @param {number} other     id of the other mote
 * @param {string} direction connection direction
 */
export async function delete_connection (mote_id, other, direction) {
    let op_data = {
        resource: Data.Resources.MoteConnection.name,
        where   : [
            { [Data.Resources.MoteConnection.fields.mote_id]: mote_id      },
            { [Data.Resources.MoteConnection.fields.other_mote]: other     },
            { [Data.Resources.MoteConnection.fields.direction] : direction }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Save an alert use
 * 
 * @param {number} mote_id id of the mote
 * @param {object} alert   data of the alert use
 */
export async function save_alert_use (mote_id, alert) {
    let op_data = {
        resource: Data.Resources.AlertUse.name,
        data    : alert
    }

    let results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}

/**
 * Delete an alert use
 * 
 * @param {number} mote_id   id of the mote
 * @param {number} alert_id  id of alert
 */
export async function delete_alert_use (mote_id, alert_id) {
    let op_data = {
        resource: Data.Resources.AlertUse.name,
        where   : [
            { [Data.Resources.AlertUse.fields.mote_id] : mote_id  },
            { [Data.Resources.AlertUse.fields.alert_id]: alert_id }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Save an trigger use
 * 
 * @param {number} mote_id id of the mote
 * @param {object} trigger data of the trigger use
 */
export async function save_trigger_use (mote_id, trigger) {
    let op_data = {
        resource: Data.Resources.TriggerUse.name,
        data    : trigger
    }

    let results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}

/**
 * Delete an trigger use
 * 
 * @param {number} mote_id    id of the mote
 * @param {number} trigger_id id of trigger
 */
export async function delete_trigger_use (mote_id, trigger_id) {
    let op_data = {
        resource: Data.Resources.TriggerUse.name,
        where   : [
            { [Data.Resources.TriggerUse.fields.mote_id]  : mote_id  },
            { [Data.Resources.TriggerUse.fields.trigger_id]: trigger_id }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Save an sensor use
 * 
 * @param {number} mote_id    id of the mote
 * @param {object} sensor_use data of the sensor_use
 * @param {boolean} update    if true entry will be updated instead of created
 */
export async function save_sensor_use (mote_id, sensor_use, update=false) {
    let op_data = {
        resource: Data.Resources.SensorUse.name,
        data    : sensor_use
    }

    let results

    if (update) {
        op_data.where = [
            { [Data.Resources.SensorUse.fields.mote_id]  : mote_id              },
            { [Data.Resources.SensorUse.fields.sensor_id]: sensor_use.sensor_id },
            { [Data.Resources.SensorUse.fields.num]      : sensor_use.num       }
        ]
        console.log('op_data', op_data)
        op_data.data = {
            [Data.Resources.SensorUse.fields.interface_num] : sensor_use.interface_num,
            [Data.Resources.SensorUse.fields.interface_name]: sensor_use.interface_name,
        }
        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}

/**
 * Delete an sensor use
 * 
 * @param {number} mote_id   id of the mote
 * @param {number} sensor_id id of the sensor
 * @param {number} num       sensor use number
 */
export async function delete_sensor_use (mote_id, sensor_id, num) {
    let op_data = {
        resource: Data.Resources.SensorUse.name,
        where   : [
            { [Data.Resources.SensorUse.fields.mote_id]  : mote_id   },
            { [Data.Resources.SensorUse.fields.sensor_id]: sensor_id },
            { [Data.Resources.SensorUse.fields.num]      : num       }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Save an actuator use
 * 
 * @param {number}  mote_id      id of the mote
 * @param {object}  actuator_use data of the actuator use
 * @param {boolean} update       if true entry will be updated instead of created 
 */
export async function save_actuator_use (mote_id, actuator_use, update=false) {
    let op_data = {
        resource: Data.Resources.ActuatorUse.name,
        data    : actuator_use
    }

    let results

    if (update) {
        op_data.where = [
            { [Data.Resources.ActuatorUse.fields.mote_id]    : mote_id                  },
            { [Data.Resources.ActuatorUse.fields.actuator_id]: actuator_use.actuator_id },
            { [Data.Resources.ActuatorUse.fields.num]        : actuator_use.num         }
        ]
        console.log('op_data', op_data)
        op_data.data = {
            [Data.Resources.ActuatorUse.fields.interface_num] : actuator_use.interface_num,
            [Data.Resources.ActuatorUse.fields.interface_name]: actuator_use.interface_name,
        }
        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}

/**
 * Delete an actuator use
 * 
 * @param {number} mote_id     id of the mote
 * @param {number} actuator_id id of actuator
 * @param {number} num         actuator use number
 */
export async function delete_actuator_use (mote_id, actuator_id, num) {
    let op_data = {
        resource: Data.Resources.ActuatorUse.name,
        where   : [
            { [Data.Resources.ActuatorUse.fields.mote_id]    : mote_id     },
            { [Data.Resources.ActuatorUse.fields.actuator_id]: actuator_id },
            { [Data.Resources.ActuatorUse.fields.num]        : num         }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}