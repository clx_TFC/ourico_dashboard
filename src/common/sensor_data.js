import * as Common from '../api/common'
import * as Data   from '../api/data'



/**
 * Get all the sensors
 */
export async function load () {
    let query = { query: {
        resource: Data.Resources.Sensor.name,
        fields  : Data.Resources.Sensor.fields.all,
        options : [{
            ordasc: Data.Resources.Sensor.fields.name
        }]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]
}


/**
 * Get one sensor
 * 
 * @param {number} sensor_id id of the sensor
 * 
 * @return {object}
 */
export async function get_one (sensor_id) {
    let query = {
        query: {
            "resource": Data.Resources.Sensor.name,
            "fields"  : Data.Resources.Sensor.fields.all,
            "filters" : [{
                [Data.Resources.Sensor.fields.id]: sensor_id
            }]
        }
    }

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0][0]
}



/**
 * Get the paramters of a sensor and the motes where
 * it is being used
 * 
 * @param {number} sensor_id id of the sensor
 * 
 * @returns {object}
 * @throws {string}
 */
export async function load_details (sensor_id) {
    let query = { query: {
        resource: Data.Resources.Sensor.name,
        filters : [{
            [Data.Resources.Sensor.fields.id]: sensor_id
        }],
        fields  : [
            Data.Resources.Sensor.fields.id,
            Data.Resources.Sensor.fields.name
        ],
        include : [
            { // parameters
                resource: Data.Resources.Sensor.related.parameters,
                fields  : Data.Resources.Parameter.fields.all
            },
            { // motes where it is being used
                resource: Data.Resources.Sensor.related.uses,
                fields  : Data.Resources.SensorUse.fields.all,
                include : [{
                    resource: Data.Resources.SensorUse.related.mote,
                    fields  : [Data.Resources.Sensor.fields.name]
                }]
            }
        ]
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0][0]
}


/**
 * Update a sensor or save a new record
 * 
 * @param {object}  sensor data to save
 * @param {boolean} update entry will be created if false
 * 
 * @return {boolean}
 * @throws {string}
 */
export async function save_sensor (sensor, update=false) {

    let op_data = {
        resource: Data.Resources.Sensor.name,
        data    : sensor
    }

    let results

    if (update) {
        op_data.where = [{
            [Data.Resources.Sensor.fields.id]: sensor.id
        }]

        op_data.data = {
            [Data.Resources.Sensor.fields.name        ]: sensor.name,
            [Data.Resources.Sensor.fields.description ]: sensor.description,
            [Data.Resources.Sensor.fields.async_      ]: sensor.async,
            [Data.Resources.Sensor.fields.output_size ]: sensor.output_size
        }

        results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
    } else {
        results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
    }

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Create or update a parameter in a sensor
 * 
 * @param {object}  parameter       the parameter to save
 * @param {number}  sensor_id       sensor that the parameter will belong to
 * @param {object}  new_output_size new output size of the sensor
 * @param {boolean} update          if false a new entry will be created instead of updated
 */
export async function save_parameter (parameter, sensor_id, new_output_size, update=false) {

    let op_data = {
        resource: Data.Resources.Parameter.name,
        data    : parameter
    }

    if (update) {
        op_data.where = [{
            [Data.Resources.Parameter.fields.id]: parameter.id
        }]

        op_data.data = {
            [Data.Resources.Parameter.fields.name]       : parameter.name,
            [Data.Resources.Parameter.fields.units]      : parameter.units,
            [Data.Resources.Parameter.fields.description]: parameter.description,
            [Data.Resources.Parameter.fields.type_]      : parameter.type,
            [Data.Resources.Parameter.fields.position]   : parameter.position,
            [Data.Resources.Parameter.fields.size]       : parameter.size,
        }

        let results = await Common.http_request('put', Data.update_url, JSON.stringify(op_data))
        if (results.is_error()) {
            let err = { error_msg: results.error_msg }
            throw err
        }

    } else {
        // save parameter
        let results = await Common.http_request('post', Data.write_url, JSON.stringify(op_data))
        if (results.is_error()) {
            let err = { error_msg: results.error_msg }
            throw err
        }
    }

    let sns_data = {
        [Data.Resources.Sensor.fields.id]         : sensor_id,
        [Data.Resources.Sensor.fields.output_size]: new_output_size
    }
    return save_sensor(sns_data, true) // update the sensor
}


/**
 * Delete a sensor
 * 
 * @param {numer} sensor_id id of the sensor
 */
export async function delete_sensor (sensor_id) {
    let op_data = {
        resource: Data.Resources.Sensor.name,
        where   : [
            { [Data.Resources.Sensor.fields.id]: sensor_id }
        ]
    }

    let results = 
        await Common.http_request('delete', Data.delete_url, JSON.stringify(op_data))
    
    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}

