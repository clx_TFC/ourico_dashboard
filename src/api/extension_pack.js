import * as Common from './common'
import * as Data   from './data'


/**
 * Extension pack operations base url
 */
const base_url = '/api.extension_pack/'


/**
 * Load extension pack data and the logs
 * 
 * @return {object|undefined}
 * @throws {object} 
 */
export async function load_data () {

    let query = { query: {
        resource: Data.Resources.ExtensionPack.name,
        include : [{ resource: Data.Resources.ExtensionPack.related.logs }] 
    }}

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return results.data[0][0]
}



/**
 * Create extension pack
 * 
 * @param {string} prog_language programming language used in the extension pack
 * @param {string} repo_url      url of the code repository
 * 
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function create (prog_language, repo_url) {

    let data = {
        language      : prog_language,
        repository_url: repo_url
    }

    let results =
        await Common.http_request('post', base_url + 'create', JSON.stringify(data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}



/**
 * Update the extension pack
 * 
 * @param {string} prog_language programming language used in the extension pack
 * @param {string} repo_url      url of the code repository
 *
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function update (prog_language, repo_url) {
    let data = {
        language: prog_language,
        repository_url: repo_url
    }

    let results =
        await Common.http_request('post', base_url + 'update', JSON.stringify(data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/** 
 * Delete the extension pack
 * 
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function delete_ () {
    let results = await Common.http_request('post', base_url + 'delete', "{}")

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Start the extension pack operation
 *  
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function start () {
    let results = await Common.http_request('post', base_url + 'start', "{}")

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Stop the extension pack operation
 * 
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function stop () {
    let results = await Common.http_request('post', base_url + 'stop', "{}")

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Subscribe extension pack to receive events of a motes
 * 
 * @param {number} motes id of the motes to subscribe to
 * 
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function subscribe (motes) {
    let data = { motes: motes }

    let results = 
        await Common.http_request('post', base_url + 'subscribe_to_motes', JSON.stringify(data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}


/**
 * Unubscribe extension pack from receiving events of a motes
 *
 * @param {number} motes id of the motes to subscribe to
 * 
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function unsubscribe (motes) {
    let data = { motes: motes }

    let results =
        await Common.http_request('post', base_url + 'unsubscribe_to_motes', JSON.stringify(data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}



/**
 * Send a custom client request
 * 
 * @param {object} request_data data to send to extension pack
 * 
 * @return {boolean} true if sucessful
 * @throws {object}
 */
export async function send_request (request_data) {
    let results =
        await Common.http_request('post', base_url + 'client_request', JSON.stringify(request_data))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    }

    return true
}



/** 
 * Get motes to subscribe/unsubscribe
 *  
 */
export async function get_motes () {
    let query = {
        query: {
            "resource": Data.Resources.Mote.name,
            "fields": [
                Data.Resources.Mote.fields.name,
                Data.Resources.Mote.fields.id
            ],
            "options": [
                { 'ordasc': Data.Resources.Mote.fields.id }
            ]
        }
    }

    let results =
        await Common.http_request('post', Data.read_url, JSON.stringify(query))

    if (results.is_error()) {
        let err = { error_msg: results.error_msg }
        throw err
    } else if (results.data.length === 0) {
        let err = { error_msg: 'Sem dados' }
        throw err
    }

    return results.data[0]
}