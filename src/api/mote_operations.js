import * as Common from './common'


const MOTE_OPERATIONS = {
    CHANGE_CONFIGURATION : 'change_configuration',
    RESET                : 'reset',
    ACTIVATE_ACTUATOR    : 'activate_actuator',
    DEACTIVATE_ACTUATOR  : 'deactivate_actuator',
    ACTIVATE_SENSOR      : 'activate_sensor',
    DEACTIVATE_SENSOR    : 'deactivate_sensor',
    SET_TRIGGER          : 'set_trigger',
    REMOVE_TRIGGER       : 'remove_trigger',
    SET_ALERT            : 'set_alert',
    REMOVE_ALERT         : 'remove_alert',
    REQUEST_MEASUREMENTS : 'request_measurements',
    REQUEST_STATUS_REPORT: 'request_status_report'
}

const mote_op_url = '/api.mote/'


/**
 * Send a message to a mote
 * 
 * @param {number} mote_id the of the mote
 * @param {string} operation the operation to perform
 * @param {object} data operation parameters
 */
async function send_cmd (mote_id, operation, data) {
    let body = { mote_id, operation, data }

    let result = await Common.http_request('post', mote_op_url, JSON.stringify(body))

    if (result.is_error()) {
        let err = { error_msg: result.error_msg }
        throw err
    }

    return true
}


/**
 * Change the state of an actuator in a mote (active/inactive)
 * 
 * @param {number}  mote_id       id of the mote
 * @param {number}  actuator_id   id of the actuator
 * @param {number}  interface_num inteface of the actuator
 * @param {boolean} active        the new state
 * 
 * @return {boolean}
 */
export async function change_actuator_state (mote_id, actuator_id, interface_num, active) {
    let operation = active ? 
        MOTE_OPERATIONS.ACTIVATE_ACTUATOR :
        MOTE_OPERATIONS.DEACTIVATE_ACTUATOR
    return await send_cmd(mote_id, operation, { actuator_id, num: interface_num })
}


/**
 * Change the state of an sensor in a mote (active/inactive)
 * 
 * @param {number}  mote_id       id of the mote
 * @param {number}  sensor_id     id of the sensor
 * @param {number}  interface_num inteface of the sensor
 * @param {boolean} active        the new state
 * 
 * @return {boolean}
 */
export async function change_sensor_state (mote_id, sensor_id, interface_num, active) {
    let operation = active ?
        MOTE_OPERATIONS.ACTIVATE_SENSOR :
        MOTE_OPERATIONS.DEACTIVATE_SENSOR
    return await send_cmd(mote_id, operation, { sensor_id, num: interface_num })
}


/**
 * Change the state of an alert in a mote (active/inactive)
 * 
 * @param {number}  mote_id  id of the mote
 * @param {number}  alert_id id of the alert
 * @param {boolean} active   the new state
 * 
 * @return {boolean}
 */
export async function change_alert_state (mote_id, alert_id, active) {
    let operation = active ?
        MOTE_OPERATIONS.SET_ALERT :
        MOTE_OPERATIONS.REMOVE_ALERT
    return await send_cmd(mote_id, operation, { alert_id })
}


/**
 * Change the state of an trigger in a mote (active/inactive)
 * 
 * @param {number}  mote_id    id of the mote
 * @param {number}  trigger_id id of the trigger
 * @param {boolean} active     the new state
 * 
 * @return {boolean}
 */
export async function change_trigger_state(mote_id, trigger_id, active) {
    let operation = active ?
        MOTE_OPERATIONS.SET_TRIGGER :
        MOTE_OPERATIONS.REMOVE_TRIGGER
    return await send_cmd(mote_id, operation, { trigger_id })
}


/**
 * Request a mote to send measurements
 * 
 * @param {number} mote_id id of the mote
 * 
 * @return {boolean}
 */
export async function request_measurements (mote_id) {
    return await send_cmd(mote_id, MOTE_OPERATIONS.REQUEST_MEASUREMENTS, {})
}


/**
 * Request a mote to send a status report
 * 
 * @param {number} mote_id id of the mote
 * 
 * @return {boolean}
 */
export async function request_status_report (mote_id) {
    return await send_cmd(mote_id, MOTE_OPERATIONS.REQUEST_STATUS_REPORT, {})
}


/**
 * Change the current configuration of a mote
 * 
 * @param {number} mote_id   id of the mote
 * @param {string} conf_name name of the configuration
 * 
 * @return {boolean}
 */
export async function change_configuration (mote_id, conf_name) {
    return await send_cmd(mote_id, MOTE_OPERATIONS.CHANGE_CONFIGURATION, { conf_name })
}


/**
 * Reset a mote's state
 * 
 * @param {number} mote_id   id of the mote
 * 
 * @return {boolean}
 */
export async function reset (mote_id) {
    return await send_cmd(mote_id, MOTE_OPERATIONS.RESET, {})
}
