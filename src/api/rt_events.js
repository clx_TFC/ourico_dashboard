import * as Common            from './common'
import * as Data              from './data'
import * as AlertsAndTriggers from '../common/alerts_triggers_data'
import * as Sensors           from '../common/sensor_data'
import * as Actuators         from '../common/actuator_data'


// websocket connection to server
let ws         = null
const base_url = 'ws://localhost:3000/ws'
let connected  = false

// callbacks to call when a new event is received
let listeners  = {}

export class RtEvent {
    constructor (data, mote, extra_data) {
        this.data       = data
        this.mote       = mote
        this.extra_data = extra_data
    }
}

export class RtEventError {
    constructor (msg) {
        this.message = msg
    }
}


/**
 * Make connection
 *
 * @param {function} onopen called when connection is opened
 */
export function connect (onopen) {
    let token = Common.get_token()
    ws = new WebSocket(base_url + `?access_token=${token}`)
    ws.onclose = () => { connected = false }
    ws.onopen  = onopen
    ws.onerror = (event) => {
        connected = false
        console.warn(event)
    }
    ws.onmessage = on_message
    connected = true
}


/**
 * End connection
 *
 * @param {number} code   optional close code
 * @param {string} reason optional reason to close connection
 */
export function disconnect (code, reason) {
    ws.close(code, reason)
    connected = false
}


/**
 * Subcribe to a mote's events
 *
 * @param {number} mote_id
 *
 * @throws RtEventError
 */
export function sub (mote_id) {
    if (!connected) throw new RtEventError('Conexão ainda não foi estabelecida')
    ws.send(JSON.stringify({ sub: mote_id }))
}


/**
 * Unsunscribe from mote's events
 *
 * @param {number} mote_id
 *
 * @throws RtEventError
 */
export function unsub (mote_id) {
    if (!connected) throw new RtEventError('Conexão ainda não foi estabelecida')
    ws.send(JSON.stringify({ unsub: mote_id }))
}


/**
 * Register a new listener to be called when an event is received
 *
 * @param {EventHandler} listener a function that takes a RtEvent as argument
 *
 * @return a key to be used to remove the listener
 */
export function register_listener (listener) {
    let key = (Math.random()*1000).toFixed()
    listeners[key] = listener
    return key
}


/**
 * Remove a listener
 *
 * @param {string} key identifier of the listener
 */
export function remove_listener (key) {
    delete listeners[key]
}


/**
 * Called when a new message is received
 * Passes the received message to all the registered listeners
 *
 * @param {WebSocket.MessageEvent} message
 */
async function on_message (message) {
    let data
    let mote
    let extra_data

    try {
        data = JSON.parse(message.data)
        console.log('data_on_message', data)

        if (data.status !== undefined)
            return

        let query = {
            query: {
                "resource": Data.Resources.Mote.name,
                "fields": [
                    Data.Resources.Mote.fields.name,
                    Data.Resources.Mote.fields.id
                ],
                "filters": [{ [Data.Resources.Mote.fields.id]: data.mote_id }],
                "options": [
                    { 'ordasc': Data.Resources.Mote.fields.id }
                ]
            }
        }
        console.log('the notification query', query)

        let results =
            await Common.http_request('post', Data.read_url, JSON.stringify(query))

        if (results.is_error()) {
            let err = { error_msg: results.error_msg }
            throw err
        } else if (results.data.length === 0) {
            let err = { error_msg: 'Sem dados' }
            throw err
        }

        mote = results.data[0][0]

        extra_data = await get_extra_event_data(data, mote)

    } catch (exception) {
        console.log(exception)
        console.log('rt_events@on_message', message)
        return
    }


    let event = new RtEvent(data, mote, extra_data)
    for (var l in listeners) {
        listeners[l](event)
    }
}



/**
 * Get data associated with type of event
 */
async function get_extra_event_data (event_data, mote) {

    switch (event_data.msg) {
    case 'alert': case 'alert_change_state':
        return await AlertsAndTriggers.get_alert(event_data.alert_id)
    case 'trigger': case 'trigger_change_state':
        return await AlertsAndTriggers.get_trigger(event_data.trigger_id)
    case 'sensor_change_state':
        return {} //await Sensors.get_one(event_data.sensor_id)
    case 'actuator_change_state':
        return {} // await Actuators.get_one(event_data.actuator_id)
    default:
        return null
    }

}


/**
 * Event handler callback
 *
 * @callback EventHandler
 * @param {RtEvent} event the real time event
 */
