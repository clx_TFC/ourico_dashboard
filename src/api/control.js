import * as Common    from    './common'
import * as Jsrsasign from 'jsrsasign'


export const base_url =   '/api.control/'
export const update_client_url = base_url + 'update_client/'
export const delete_client_url = base_url + 'delete_client/'


const OURICO_PUBLIC_KEY = (
`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvKJ6cR9x5S7bNTmV52iH
P9bshi7u+N633L+vhqIMxGSfXVGTSiMuVaCEAl9TqPjL0eq7tNgUxuMtZ9Qer9N/
2reml0LTIZ34JdsaP3h0KihpVBzblDb2nyFHLCL82F02q791OpvvpTWbvCuQQnu+
E8R4NFD56XWWftikLQC8FmPljyPapRp13GbbArR4PEfuJGJ1a3EWEvogdus0B7pJ
O5Y5Y2VGKFZT8mVsSHwq9+rj1ZFzNJgGe2IEh772z2+0zgMLTFGH4kFe8BIzSqoC
Ryg+iJc5weiAwajPKRIkr+dlHlHxq5LnM0WFtiMy1qfwl21A6LQNA3TIVaPKWNlQ
CwIDAQAB
-----END PUBLIC KEY-----`)
const PUBLIC_KEY = Jsrsasign.KEYUTIL.getKey(OURICO_PUBLIC_KEY)


export const ClientType = {
    User: 1,
    App : 2
}


/**
 * @return Common.ApiResponse with new token in `data` field
 */
export async function login (username, password) {
    try {

        const response = await window.fetch(base_url + `login`, {
            method : 'post',
            cache  : 'no-store', // don't cache result
            body   : JSON.stringify({ username, password }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        if (response.status === 401) {
            let result = await response.text()
            return new Common.ApiResponse(response.status, {}, result)
        } else if (response.status !== 200) {
            return new Common.ApiResponse(response.status, null, 'Pedido ao servidor falhou')
        }

        let new_token = await response.text()
        let is_valid = Jsrsasign.jws.JWS.verifyJWT(new_token, PUBLIC_KEY, { alg: ['RS256'] })
        if (!is_valid)
            return new Common.ApiResponse(-1, null, 'Dados corrompidos recebidos')

        return new Common.ApiResponse(200, new_token, null)

    } catch (exception) {
        console.warn(exception)
        return new Common.ApiResponse(-1, null, 'Pedido ao servidor falhou')
    }
}


/**
 * Register new account
 * 
 * @param {string} name
 * @param {string} email 
 * @param {string} username 
 * @param {string} password 
 */
export async function create_account (name, email, username, password) {
    let data = { data: { name, email, username, password } }
    try {

        const response = await window.fetch(base_url + `new_admin`, {
            method: 'post',
            cache: 'no-store', // don't cache result
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        
        if (response.status === 400) {
            let res = await response.text()
            return new Common.ApiResponse(response.status, null, res)
        } else if (response.status !== 200) {
            return new Common.ApiResponse(response.status, null, 'Pedido ao servidor falhou')
        }

        let new_token = await response.text()
        let is_valid = Jsrsasign.jws.JWS.verifyJWT(new_token, PUBLIC_KEY, { alg: ['RS256'] })
        if (!is_valid)
            return new Common.ApiResponse(-1, null, 'Dados corrompidos recebidos')

        return new Common.ApiResponse(200, new_token, null)

    } catch (exception) {
        console.warn(exception)
        return new Common.ApiResponse(-1, null, 'Pedido ao servidor falhou')
    }
}


/**
 * @param token token currently being used
 *
 * @return Common.ApiResponse
 */
export async function logout (token) {
    try {
        let response = await window.fetch(base_url + `logout`, {
            method : 'post',
            headers: { 'Authorization': `Bearer ${token}` }
        })
        if (response.status !== 200) {
            return new Common.ApiResponse(response.status, {}, null)
        }
        else
            return new Common.ApiResponse(200, {}, null)

    } catch (exception) {
        console.warn(exception)
        return new Common.ApiResponse(-1, null, 'Pedido ao servidor falhou')
    }
}


/**
 * Update the authentication token
 *
 * @param current_token token to update
 *
 * @return Common.ApiResponse with new token in `data` field
 */
export async function update_token (current_token) {
    try {

        let response = await window.fetch(base_url + 'update_token', {
            method : 'post',
            headers: { 'Authorization': `Bearer ${current_token}` }
        })
        if (response.status !== 200)
            return new Common.ApiResponse(
                response.status, {}, 'Atualização de credenciais falhou'
            )

        let result = await response.json()
        
        let is_valid = Jsrsasign.jws.JWS.verifyJWT(result.new_token, PUBLIC_KEY, { alg: ['RS256'] })
        if (!is_valid)
            return new Common.ApiResponse(-1, null, 'Dados corrompidos recebidos')
        
        return new Common.ApiResponse(200, result.new_token, null)

    } catch (exception) {
        console.warn(exception)
        return new Common.ApiResponse(-1, null, 'Pedido ao servidor falhou')
    }
}


export function is_valid_password (password) {
    if (password.length < 8)
        return false

    let contains_int = /\d/g
    let contains_uc = /[A-Z]/g
    let contains_lc = /[a-z]/g

    if (!contains_int.test(password)
        || !contains_lc.test(password)
        || !contains_uc.test(password))
        return false
    
    return true
}