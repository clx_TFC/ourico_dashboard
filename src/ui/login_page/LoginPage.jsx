import React from 'react'

import { Button, Form, Grid, Input, Message, Modal } from 'semantic-ui-react'

import * as Control from '../../api/control'
import * as Common  from '../../api/common'


export default class LoginPage extends React.Component {

    constructor (props) {
        super(props)

        this.state = {

            login_modal_open: true,
            
            username: '',
            password: '',

            with_error: false,
            with_username_error: false,
            with_password_error: false,
            error_header: 'Erro',
            error_message: '',
            doing_login: false,

            present_state: 'login',

            register_with_error: false,
            new_name: '',
            new_email: '',
            new_username: '',
            new_password: '',
            repeated_password: '',
            new_password_with_error: false,
            repeated_password_with_error: false,
            doing_registration: false
        }
    }

    reset_state = () => {
        this.setState({
            username: '',
            password: '',

            with_error: false,
            with_username_error: false,
            with_password_error: false,
            error_header: 'Erro',
            error_message: '',
            doing_login: false,

            present_state: 'login',

            register_with_error: false,
            new_name: '',
            new_email: '',
            new_username: '',
            new_password: '',
            repeated_password: '',
            new_password_with_error: false,
            repeated_password_with_error: false,
            new_username_with_error: false,
            new_name_with_error: false,
            new_email_with_error: false,
            doing_registration: false
        })
    }

    open_login_modal = () => {
        this.reset_state()
        this.setState({ present_state: 'login' })
    }

    open_register_modal = () => {
        this.reset_state()
        this.setState({ present_state: 'register' })
    }


    handle_login = async () => {
        this.setState({ 
            doing_login: true,
            with_error: false,
            with_username_error: false,
            with_password_error: false
         })

        try {

            let result = await Control.login(this.state.username, this.state.password)

            if (result.is_error()) {
                if (result.error_msg.includes('username')) {
                    this.setState({
                        username: '', password: '',
                        with_username_error: true,
                        with_error: true,
                        doing_login: false,
                        error_message: 'Nome de utilizador inválido'
                    })
                } else if (result.error_msg.includes('password')) {
                    this.setState({
                        password: '',
                        with_password_error: true,
                        with_error: true,
                        doing_login: false,
                        error_message: 'Senha inválida'
                    })
                } else {
                    this.setState({
                        doing_login: false,
                        with_error: true,
                        error_message: result.error_msg
                    })
                }
            } else {
                Common.set_token(result.data)
                this.props.handle_login_done()
            }

        } catch (exception) {
            console.log('handle_login falhou', exception)
            this.setState({
                doing_login: false,
                with_error: true,
                error_message: 'Pedido ao servidor falhou'
            })
        }
    }


    /**
     * Register new client
     */
    handle_register = async () => {
        this.setState({
            doing_registration: true,
            with_error: false,
            new_password_with_error: false,
            repeated_password_with_error: false,
            new_email_with_error: false,
            new_username_with_error: false,
            new_name_with_error: false,
        })

        

        if (this.state.new_name.length === 0) {
            this.setState({
                doing_registration: false,
                with_error: true,
                new_name_with_error: true,
                error_message: 'Todos os campos devem ser preenchidos'
            })
            return
        }

        else if (this.state.new_email.length === 0) {
            this.setState({
                doing_registration: false,
                with_error: true,
                new_email_with_error: true,
                error_message: 'Todos os campos devem ser preenchidos'
            })
            return
        }

        else if (this.state.new_username.length === 0) {
            this.setState({
                doing_registration: false,
                with_error: true,
                new_username_with_error: true,
                error_message: 'Todos os campos devem ser preenchidos'
            })
            return
        }

        else if (this.state.new_password !== this.state.repeated_password) {
            this.setState({
                doing_registration: false,
                with_error: true,
                repeated_password_with_error: true,
                error_message: 'A duas senhas não são iguais'
            })
            return
        }

        else if (!Control.is_valid_password(this.state.new_password)) {
            this.setState({
                doing_registration: false,
                with_error: true,
                new_password_with_error: true,
                error_message: ('A senha deve ter pelo menos 8 caracteres, ' +
                    'ter números, letras minúsculas e letras maiúsculas')
            })
            return
        }


        try {

            let result = await Control.create_account(
                this.state.new_name,
                this.state.new_email,
                this.state.new_username,
                this.state.new_password
            )

            if (result.is_error()) {
                if (result.includes('username')) {
                    this.setState({
                        doing_registration: false,
                        with_error: true,
                        new_username_with_error: true,
                        error_message: 'O nome de utilizador que escolheu não está disponivel'
                    })
                    return
                } else if (result.includes('email')) {
                    this.setState({
                        doing_registration: false,
                        with_error: true,
                        new_email_with_error: true,
                        error_message: 'O email que introduziu não está disponivel'
                    })
                    return
                }
            } else {
                Common.set_token(result.data)
                this.props.handle_login_done()
            }
            
        } catch (exception) {
            console.log('handle_register falhou', exception)
            this.setState({
                doing_registration: false,
                with_error: true,
                error_message: 'Pedido ao servidor falhou'
            })
        }
    }



    render () {

        let login_modal_style = {
            marginTop: '5em',
            marginBottom: 'auto',
            marginLeft: 'auto',
            marginRight: 'auto'
        }
        let register_modal_style = {
            marginTop: '0',
            marginBottom: 'auto',
            marginLeft: 'auto',
            marginRight: 'auto'
        }

        let background_div_style = {
            backgroundImage: 'url("/arduino.jpg")',
            width: '100vw', height: '100vh'
        }

        let login_modal = (
            <Modal open={this.state.login_modal_open} dimmer='blurring' size='tiny' style={login_modal_style}>
                <Modal.Header >
                    <Grid padded={false}>
                        <Grid.Row>
                            <Grid.Column width='2'>
                                <img src={this.props.logo} alt='logo' />
                            </Grid.Column>
                            <Grid.Column width='14'>
                                <h2 >Login</h2>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Modal.Header>
                <Modal.Content >
                    <Form style={{ paddingTop: '1em', paddingBottom: '1em' }}>
                        <Form.Group widths='equal' style={{ margin: '0 2em 2em 2em' }}>
                            <Form.Field
                                label='Nome de Utilizador'
                                error={this.state.with_username_error}
                                control={Input}
                                fluid
                                placeholder='Nome de Utilizador'
                                icon='user'
                                value={this.state.username}
                                onChange={(e, { value }) => this.setState({ username: value })}
                            />
                        </Form.Group>
                        <Form.Group widths='equal' style={{ margin: '0 2em 2em 2em' }} >
                            <Form.Field
                                label='Senha'
                                error={this.state.with_password_error}
                                control={Input}
                                type='password'
                                fluid
                                placeholder='Senha'
                                icon='key'
                                value={this.state.password}
                                onChange={(e, { value }) => this.setState({ password: value })}
                            />
                        </Form.Group>
                    </Form>
                    <Message
                        style={{ margin: '0 2em 0 2em' }}
                        attached='bottom' error hidden={!this.state.with_error}
                        header={this.state.error_header}
                        content={this.state.error_message}
                    />
                </Modal.Content>

                <Modal.Actions >
                    <Button
                        basic
                        color='blue'
                        icon='add user'
                        labelPosition='right'
                        content='Criar Conta'
                        onClick={this.open_register_modal}
                        disabled={this.state.doing_login}
                    />
                    <Button
                        color='blue'
                        icon='checkmark'
                        labelPosition='right'
                        content='Confirmar'
                        loading={this.state.doing_login}
                        disabled={this.state.doing_login}
                        onClick={this.handle_login}
                    />
                </Modal.Actions>
            </Modal>
        )

        let register_modal = (
            <Modal open={this.state.login_modal_open} dimmer='blurring' size='tiny' style={register_modal_style}>
                <Modal.Header >
                    <Grid padded={false}>
                        <Grid.Row>
                            <Grid.Column width='2'>
                                <img src={this.props.logo} alt='logo' />
                            </Grid.Column>
                            <Grid.Column width='14'>
                                <h2>Criar Conta</h2>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Modal.Header>
                <Modal.Content >
                    <Form style={{ paddingBottom: '1em' }}>
                        <Form.Group widths='equal' style={{ margin: '0 2em 1em 2em' }}>
                            <Form.Field
                                label='Name'
                                error={this.state.new_name_with_error}
                                control={Input}
                                fluid
                                placeholder='Nome'
                                value={this.state.new_name}
                                onChange={(e, { value }) => this.setState({ new_name: value })}
                            />
                        </Form.Group>
                        <Form.Group widths='equal' style={{ margin: '0 2em 1em 2em' }}>
                            <Form.Field
                                label='Email'
                                error={this.state.new_email_with_error}
                                control={Input}
                                fluid
                                type='email'
                                placeholder='Email'
                                value={this.state.new_email}
                                onChange={(e, { value }) => this.setState({ new_email: value })}
                            />
                        </Form.Group>
                        <Form.Group widths='equal' style={{ margin: '0 2em 1em 2em' }}>
                            <Form.Field
                                label='Nome de Utilizador'
                                error={this.state.new_username_with_error}
                                control={Input}
                                fluid
                                placeholder='Nome de Utilizador'
                                value={this.state.new_username}
                                onChange={(e, { value }) => this.setState({ new_username: value })}
                            />
                        </Form.Group>
                        <Form.Group widths='equal' style={{ margin: '0 2em 1em 2em' }} >
                            <Form.Field
                                label='Password'
                                error={this.state.new_password_with_error}
                                control={Input}
                                type='password'
                                fluid
                                placeholder='Password'
                                icon='key'
                                value={this.state.new_password}
                                onChange={(e, { value }) => this.setState({ new_password: value })}
                            />
                        </Form.Group>
                        <Form.Group widths='equal' style={{ margin: '0 2em 1em 2em' }} >
                            <Form.Field
                                label='Repetir Password'
                                error={this.state.repeated_password_with_error}
                                control={Input}
                                type='password'
                                fluid
                                placeholder='Password'
                                icon='key'
                                value={this.state.repeated_password}
                                onChange={(e, { value }) => this.setState({ repeated_password: value })}
                            />
                        </Form.Group>
                    </Form>
                    <Message
                        style={{ margin: '0 2em 0 2em' }}
                        attached='bottom' error hidden={!this.state.with_error}
                        header={this.state.error_header}
                        content={this.state.error_message}
                    />
                </Modal.Content>

                <Modal.Actions >
                    <Button
                        basic
                        color='blue'
                        icon='sign in'
                        labelPosition='right'
                        content='Já tenho uma conta'
                        disabled={this.state.doing_registration}
                        onClick={this.open_login_modal}
                    />
                    <Button
                        color='blue'
                        icon='checkmark'
                        labelPosition='right'
                        content='Criar'
                        onClick={this.handle_register}
                        disabled={this.state.doing_registration}
                        loading={this.state.doing_registration}
                    />
                </Modal.Actions>
            </Modal>
        )

        if (this.state.present_state === 'login')
            return (
                <div>
                    <div style={background_div_style}></div>
                    {login_modal}
                </div>
            )
        else 
            return (
                <div>
                    <div style={background_div_style}></div>
                    {register_modal}
                </div>
            )
    }

}