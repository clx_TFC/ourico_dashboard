import React from 'react'

import { Accordion, Menu } from 'semantic-ui-react'


export default class MoteConfigurations extends React.Component {

    constructor (props) {
        super(props)

        this.state = {
            active_items_holder_pane: 0
        }
    }


    handle_items_holder_click = (_, { index }) => {
        this.setState({ active_items_holder_pane: index })
    }


    render () {

        let container_style = { minHeight: "80vh", width: "100vw" }
        let items_holder_style = {
            zIndex      : 100,
            position    : 'fixed',
            marginLeft  : '80%',
            marginTop   : 'auto',
            marginBottom: 'auto'
        }

        return (
        <canvas style={container_style} id='mote_conf_canvas'>

            {/* items holder */}
            <Accordion style={items_holder_style} as={Menu} vertical>
                <Menu.Item>
                <Accordion.Title 
                    onClick={this.handle_items_holder_click}
                    index={0}
                    active={this.state.active_items_holder_pane === 0} content={'Motes'}
                />
                    <Accordion.Content
                        active={this.state.active_items_holder_pane === 0}>
                        <p>Something</p>
                        <p>Something else</p>
                    </Accordion.Content>
                </Menu.Item>

                <Menu.Item>
                <Accordion.Title 
                    onClick={this.handle_items_holder_click}
                    index={1}
                    active={this.state.active_items_holder_pane === 1}
                    content={'Configurações'}
                />
                    <Accordion.Content
                        active={this.state.active_items_holder_pane === 1}>

                    </Accordion.Content>
                </Menu.Item>
                
                <Menu.Item>
                <Accordion.Title 
                    onClick={this.handle_items_holder_click}
                    index={2}
                    active={this.state.active_items_holder_pane === 2}
                    content={'Sensores'}
                />
                    <Accordion.Content
                        active={this.state.active_items_holder_pane === 2}>

                    </Accordion.Content>
                </Menu.Item>
                
                <Menu.Item>
                <Accordion.Title 
                    onClick={this.handle_items_holder_click}
                    index={3}
                    active={this.state.active_items_holder_pane === 3}
                    content={'Atuadores'}
                />
                    <Accordion.Content
                    active={this.state.active_items_holder_pane === 3}>

                    </Accordion.Content>
                </Menu.Item>
                
                <Menu.Item>
                <Accordion.Title 
                    onClick={this.handle_items_holder_click}
                    index={4}
                    active={this.state.active_items_holder_pane === 4}
                    content={'Alertas'}
                />
                    <Accordion.Content
                    active={this.state.active_items_holder_pane === 4}>

                    </Accordion.Content>
                </Menu.Item>
                
                <Menu.Item>
                <Accordion.Title 
                    onClick={this.handle_items_holder_click}
                    index={5}
                    active={this.state.active_items_holder_pane === 5}
                    content={'Triggers'}
                />
                    <Accordion.Content
                        active={this.state.active_items_holder_pane === 5}>

                    </Accordion.Content>
                </Menu.Item>

            </Accordion>
        </canvas>
        )
    }

}