import React from 'react'

import { Grid } from 'semantic-ui-react'

import { Draggable, Droppable } from 'react-beautiful-dnd'


/**
 * A mote in the configuration board
 * 
 * @prop {object}   position x and y coordinates to position view in the screen
 * @prop {object[]} data     data to display in droppable containers
 */
export default class MoteItem extends React.Component {

    render () {
        return (
        <Draggable>
            <Grid>
                <Grid.Row>
                    
                </Grid.Row>

                <Grid.Row>
                </Grid.Row>

                <Grid.Row>
                    <Droppable>
                        {/*{this.props.data.actuators.map(actuator => ())}*/}
                    </Droppable>
                </Grid.Row>
            </Grid>
        </Draggable>
        )
    }

}