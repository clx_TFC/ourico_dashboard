import React from 'react'

import Content           from './Content'
import MoteListing       from './mote_listing/MoteListing'
import MoteLocations     from './mote_locations_map/MoteLocations'
// import MoteConfiguration from './mote_configuration/MoteConfiguration'

import { Tab } from 'semantic-ui-react'


/**
 * Motes section
 */
export default class Motes extends React.Component {

    render () {
    
        let locations_pane = {
            menuItem: 'Localizações', 
            render: () => (<Tab.Pane><MoteLocations/></Tab.Pane>)
        }


        // let configuration_pane = {
        //     menuItem: 'Configurações',
        //     render: () => (<Tab.Pane><MoteConfiguration/></Tab.Pane>)
        // }

        let listing_pane = {
            menuItem: 'Listagem',
            render: () => (<Tab.Pane><MoteListing/></Tab.Pane>)
        }


        let the_content
        if (this.props.location !== undefined && this.props.location.state !== undefined) {
            if (this.props.location.state.pane === 'configurations') {
                the_content = (
                    <Tab
                        activeIndex={1}
                        panes={[listing_pane, locations_pane]}
                        style={{ marginTop: '0.5em' }}
                        className='dashboard_tabbed_content'
                    />
                )
            }
            else if (this.props.location.state.pane === 'listing') {
                the_content = (
                    <Tab
                        activeIndex={2}
                        panes={[listing_pane, locations_pane]}
                        style={{ marginTop: '0.5em' }}
                        className='dashboard_tabbed_content'
                    />
                )
            } else { // locations
                the_content = (
                    <Tab
                        activeIndex={0}
                        panes={[listing_pane, locations_pane]}
                        style={{ marginTop: '0.5em' }}
                        className='dashboard_tabbed_content'
                    />
                )
            }
        } else {
            the_content = (
                <Tab
                    panes={[listing_pane, locations_pane]}
                    style={{ marginTop: '0.5em' }}
                    className='dashboard_tabbed_content'
                />
            )
        }

        return (
            <Content name='Motes' content={the_content}/>
        )

    }

}