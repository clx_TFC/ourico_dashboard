import React from 'react'

import FormModal from '../FormModal'

import { Select } from 'semantic-ui-react'

import * as ExtPackData from '../../../api/extension_pack'


export default class SubscriptionModal extends React.Component {

    FORM_SPEC = [
        {
            fields: [
                {
                    key: 'motes',
                    label: 'Motes',
                    required: true,
                    placeholder: 'Motes',
                    control: Select,
                    multiple: true,
                }
            ],
            group_key: 'a'
        }
    ]


    constructor (props) {
        super(props)

        this.state = {
            motes_choice: []
        }
    }



    save = async (data) => {

        let err_msg = 'Não foi concluir a operação'

        try {

            if (this.props.sub)
                await ExtPackData.subscribe(data.motes)
            else
                await ExtPackData.unsubscribe(data.motes)

            return true

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }


    async componentDidMount () {
        try {
            let motes = await ExtPackData.get_motes()
            console.log('the motes', motes)
            let options = motes.map(mote => ({
                key: mote.id, value: mote.id, text: mote.name
            }))
            this.FORM_SPEC[0].fields[0].options = options
            this.setState({ motes_choice: options })
        } catch (exception) {
            console.error('Failed to get list of motes', exception)
        }
    }


    render () {
        let title = this.props.sub ? 'Adicionar subscrições' : 'Remover subscrições'
        return (
        <FormModal
            open={this.props.open}
            handle_close={this.props.handle_close}
            title={title}
            form_groups={this.FORM_SPEC}
            save={this.save}
        />
        )
    }

}