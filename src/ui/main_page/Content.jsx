import * as React from 'react'
import { Breadcrumb, Menu } from 'semantic-ui-react'


export default class Content extends React.Component {

    render () {
        const with_breadcrumb = this.props.with_breadcrumb === true
        const path_components = this.props.path_components || []
        const content         = this.props.content

        let breadcrumb = null
        if (path_components.length > 0) {
            breadcrumb = (
                <Menu.Item fitted>
                    <Breadcrumb>
                    {path_components.map(cmp => (
                        <span key={cmp}>
                        <Breadcrumb.Divider icon='right chevron' />
                        <Breadcrumb.Section >{cmp}</Breadcrumb.Section>
                        </span>
                    ))}
                    </Breadcrumb>
                </Menu.Item>
            )
        }

        return (with_breadcrumb ? (
            <div>
                <Menu secondary>
                    <Menu.Item className='section_navigation' fitted>
                        <Menu.Menu >
                            <Menu.Item fitted>
                                <span className='section_name'>{this.props.name}</span>
                            </Menu.Item>
                            {breadcrumb}
                        </Menu.Menu>
                    </Menu.Item>
                    <Menu.Item
                        className="section_navigation_top_right"
                        fitted='vertically'
                        position='right'>
                        {this.props.top_right_content}
                    </Menu.Item>
                </Menu>
                {content}
            </div>
        ) : (
            <div>{content}</div>
        ))
    }

}
