import React from 'react'

import { Grid, Header, Menu } from 'semantic-ui-react'

import Chart               from '../Chart'
import TableWithPagination from '../TableWithPagination'
import DateIntervalPicker  from '../DateIntervalPicker'

import * as MoteData      from '../../../common/motes_data'
import { DataFetchState, date_format } from '../../../common/misc'

import * as Moment from 'moment'



/**
 * View to present measurements from a mote
 * 
 * @prop {number} mote_id
 */
export default class MoteMeasurements extends React.Component {

    measurement_table_columns = [
        { text: 'Valor'      , key: 'value'     },
        { text: 'Data e Hora', key: 'date_time' }
    ]

    constructor (props) {
        super(props)
        
        const from = Moment().subtract(8, 'days') // a week ago
        const to   = Moment().add(1, 'days')      // tomorrow

        this.state = {
            data_fetch_state: DataFetchState.PENDING,
            data_fetch_error: 'Verifique a sua conexão á Internet',
            data            : [],
            // get data since this date
            data_from_date  : from,
            // get data until this date
            data_until_date : to,
        }
    }


    /**
     * Called when user changes data fetch interval
     * 
     * @param new_dates { startDate, endDate } interval to fetch data
     */
    handle_dates_change = async (new_dates) => {
        let start_date = new_dates.startDate !== null ?
            new_dates.startDate.hours(0) : null
        let end_date   = new_dates.endDate !== null ?
            new_dates.endDate.hours(23).minutes(59) : null
        this.setState({
            data_from_date : start_date,
            data_until_date: end_date 
        }, () => this.load_data())
    }


    /**
     * Load measurements
     */
    load_data = async () => {
        try {
            let data = await MoteData.load_measurements(
                this.props.mote_id,
                date_format(this.state.data_from_date),
                date_format(this.state.data_until_date),
            )
            console.log('data', data)
            this.setState({
                data: data,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    async componentDidMount () {
        await this.load_data()
    }


    render () {

        return (
            <div>
            <Menu fluid size='small' secondary >
                <Menu.Item position='right' >
                    <DateIntervalPicker
                        data_from_date={this.state.data_from_date}
                        data_until_date={this.state.data_until_date}
                        handle_dates_change={this.handle_dates_change}
                        label='Ver dados no intervalo'
                    />
                </Menu.Item>
            </Menu>
            {this.state.data.map(su => (
                <Grid key={su.num} >
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Header as='h2'>
                                <Header.Content >
                                    {`${su.sensor.name} (#${su.num})`}
                                    <Header.Subheader>
                                        {su.sensor.description}
                                    </Header.Subheader>
                                </Header.Content>
                            </Header>
                        </Grid.Column>
                    </Grid.Row>
                    {su.sensor.parameters.map(p => (
                        p.type_ === 'int' || p.type_ === 'float' ? (
                            <Grid.Row columns={2} key={p.id} >
                                <Grid.Column width={10} >
                                    <Header as='h4'>
                                        <Header.Content >
                                            {`${p.name} (${p.units})`}
                                            <Header.Subheader>
                                                {p.description}
                                            </Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                    <Chart 
                                        accepted_charts={['bar', 'line']}
                                        default_chart='line'
                                        units={p.units}
                                        data={p.measurements.map(m => ({
                                            ...m, 
                                            date_time: Moment(m.date_time).format('HH:mm:ss DD-MM-YYYY'),
                                            value    : parseInt(m.value, 16)
                                        }))}
                                        x={'date_time'}
                                        y={'value'}
                                    />
                                </Grid.Column>
                                <Grid.Column width={6} verticalAlign='middle' >
                                    <TableWithPagination
                                        collapsing
                                        items_per_page={5}
                                        name=''
                                        row_key='date_time'
                                        header_fields={this.measurement_table_columns}
                                        data={p.measurements.map(m => ({
                                            ...m, 
                                            date_time: Moment(m.date_time).format('HH:mm:ss DD-MM-YYYY'),
                                            value    : parseInt(m.value, 16)
                                        }))}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                        ) : (
                            <Grid.Row columns={2} key={p.id} >
                                <Grid.Column width={6} >
                                    <Header as='h4'>
                                        <Header.Content >
                                            {`${p.name} (${p.units})`}
                                            <Header.Subheader>
                                                {p.description}
                                            </Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                    <Chart 
                                        accepted_charts={['pie', 'bar']}
                                        default_chart='pie'
                                        units={p.units}
                                        data={p.measurements.map(m => ({
                                            ...m, 
                                            date_time: Moment(m.date_time).format('HH:mm:ss DD-MM-YYYY')
                                        }))}
                                        x={'date_time'}
                                        y={'value'}
                                    />
                                </Grid.Column>
                                <Grid.Column width={10} verticalAlign='middle'>
                                    <TableWithPagination
                                        items_per_page={5}
                                        name=''
                                        row_key='date_time'
                                        header_fields={this.measurement_table_columns}
                                        data={p.measurements.map(m => ({
                                            ...m, 
                                            date_time: Moment(m.date_time).format('HH:mm:ss DD-MM-YYYY')
                                        }))}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                        )
                    ))}
                </Grid>
            ))}
            </div>
        )
    }

}