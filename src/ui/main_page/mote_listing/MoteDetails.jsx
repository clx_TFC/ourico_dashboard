import React from 'react'

import { 
    Button, Breadcrumb, 
    Divider, Dropdown, Grid, 
    Header, Icon,
    Item, Modal, Radio
} from 'semantic-ui-react'

import TableWithPagination    from '../TableWithPagination'
import Map                    from '../Map'
import MoteMeasurements       from './MoteMeasurements'
import MoteExtraData          from './MoteExtraData'
import MoteConfigurationModal from './MoteConfigurationModal'
import MoteConnectionModal    from './MoteConnectionModal'
import AlertUseModal          from './AlertUseModal'
import TriggerUseModal        from './TriggerUseModal'
import SensorUseModal         from './SensorUseModal'
import ActuatorUseModal       from './ActuatorUseModal'
import MoteModal              from './MoteModal'

import * as Moment from 'moment'

import * as MoteData       from '../../../common/motes_data'
import * as MoteOperations from '../../../api/mote_operations'


/**
 * Presents data from a mote
 * 
 * @prop {object} mote data associated with the mote
 */
export default class MoteDetails extends React.Component {
    
    configurations_table_columns = [
        { text: 'Nome', key: 'name' },
        { text: 'Periodo entre medições', key: 'measurement_period' },
        { text: 'Periodo entre envio de dados', key: 'send_data_period' },
        { text: 'Periodo entre envio de relatórios', key: 'status_report_period' },
        { text: 'Criptografar pacotes?', key: 'use_crypto' },
        { text: 'Ativa?', key: 'active' }
    ]

    connections_table_columns = [
        { text: 'Direção', key: 'direction' },
        { text: 'Mote conectado', key: 'other_mote_name' }
    ]

    sensors_table_columns = [
        { text: 'Nome', key: 'name' },
        // { text: 'Número', key: 'num' },
        { text: 'Número da interface', key: 'interface_num' },
        { text: 'Nome da interface', key: 'interface_name' },
        { text: 'Ativo desde', key: 'activated_at' },
        { text: 'Inativo desde', key: 'deactivated_at' },
        { text: 'Ativo?', key: 'active' }
    ]

    actuators_table_columns = [
        { text: 'Nome', key: 'name' },
        // { text: 'Número', key: 'num' },
        { text: 'Número da interface', key: 'interface_num' },
        { text: 'Nome da interface', key: 'interface_name' },
        { text: 'Ativo desde', key: 'activated_at' },
        { text: 'Inativo desde', key: 'deactivated_at' },
        { text: 'Ativo?', key: 'active' }
    ]

    alerts_table_columns = [
        { text: 'Nome', key: 'name' },
        { text: 'Ativo desde', key: 'activated_at' },
        { text: 'Inativo desde', key: 'deactivated_at' },
        { text: 'Ativo?', key: 'active' }
    ]

    triggers_table_columns = [
        { text: 'Nome', key: 'name' },
        { text: 'Ativo desde', key: 'activated_at' },
        { text: 'Inativo desde', key: 'deactivated_at' },
        { text: 'Ativo?', key: 'active' }
    ]


    constructor (props) {
        super(props)

        this.state = {
            mote_active      : props.data.active,
            crypto_key_hidden: true,

            // indicator to open modal to create/edit configuration
            conf_modal_open        : false,
            // to open modal to create/edit connection
            connection_modal_open  : false,
            // to open modal to create alert use
            alert_use_modal_open   : false,
            // to open modal to create trigger use
            trigger_use_modal_open : false,
            // to open modal to create sensor use
            sensor_use_modal_open  : false,
            // to open modal to create actuator use
            actuator_use_modal_open: false,

            // confirm modal state
            confirm_modal_open          : false,
            confirm_modal_header        : '',
            confirm_modal_message       : '',
            confirm_modal_handle_confirm: undefined,
            confirm_modal_loading_state : false,

            change_state_modal_open: false,
            items_to_change_state: [],
            change_state_save_function: () => undefined
        }
    }


    edit_configuration = (conf) => {
        this.setState({ 
            conf_modal_open      : true,
            configuration_to_edit: conf,
            when_conf_updated    : (new_data) => {
                conf.measurement_period   = new_data.measurement_period
                conf.send_data_period     = new_data.send_data_period
                conf.status_report_period = new_data.status_report_period
                conf.use_crypto           = new_data.use_crypto
                this.forceUpdate()
            }
        })
    }


    edit_sensor_use = (sensor_use) => {
        this.setState({
            sensor_to_edit       : sensor_use,
            sensor_use_modal_open: true
        })
    }


    edit_actuator_use = (actuator_use) => {
        this.setState({
            actuator_to_edit       : actuator_use,
            actuator_use_modal_open: true
        })
    }


    delete_connection = (connection) => {
        if (connection.mote_there === undefined) {
            this.setState({
                confirm_modal_open          : true,
                confirm_modal_header        : 'Erro',
                confirm_modal_message       : 'A conexão deve ser removida a partir do mote onde for criada',
                confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
            })
            return
        }
        this.delete_item(
            connection,
            'Conexão',
            _ => MoteData.delete_connection(this.props.data.id, connection.mote_there.id, connection.direction)
        )
    }

    delete_configuration = (conf) => {
        if (conf.active) {
            this.setState({
                confirm_modal_open          : true,
                confirm_modal_header        : 'Erro',
                confirm_modal_message       : 'Não é possivel remover uma configuração ativa',
                confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
            })
            return
        }
        this.delete_item(
            conf, 
            'Configuração', 
            _ => MoteData.delete_configuration(this.props.data.id, conf.name)
        )
    }


    delete_trigger_use = (trigger_use) => {
        if (trigger_use.active) {
            this.setState({
                confirm_modal_open          : true,
                confirm_modal_header        : 'Erro',
                confirm_modal_message       : 'Não é possivel remover um trigger ativo',
                confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
            })
            return
        }
        this.delete_item(
            trigger_use, 
            'Trigger', 
            _ => MoteData.delete_trigger_use(this.props.data.id, trigger_use.trigger.id)
        )
    }


    delete_sensor_use = (sensor_use) => {
        if (sensor_use.active) {
            this.setState({
                confirm_modal_open          : true,
                confirm_modal_header        : 'Erro',
                confirm_modal_message       : 'Não é possivel remover um sensor ativo',
                confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
            })
            return
        }
        this.delete_item(
            sensor_use, 
            'Sensor', 
            _ => MoteData.delete_sensor_use(this.props.data.id, sensor_use.sensor.id, sensor_use.num)
        )
    }


    delete_actuator_use = (actuator_use) => {
        if (actuator_use.active) {
            this.setState({
                confirm_modal_open          : true,
                confirm_modal_header        : 'Erro',
                confirm_modal_message       : 'Não é possivel remover um atuador ativo',
                confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
            })
            return
        }
        this.delete_item(
            actuator_use, 
            'Atuador', 
            _ => MoteData.delete_actuator_use(this.props.data.id, actuator_use.actuator.id, actuator_use.num)
        )
    }


    delete_alert_use = (alert_use) => {
        if (alert_use.active) {
            this.setState({
                confirm_modal_open          : true,
                confirm_modal_header        : 'Erro',
                confirm_modal_message       : 'Não é possivel eliminar um alerta ativo',
                confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
            })
            return
        }
        this.delete_item(
            alert_use, 
            'Alerta', 
            _ => MoteData.delete_alert_use(this.props.data.id, alert_use.alert.id)
        )
    }


    /**
     * Delete the mote
     * 
     * @param {number} mote_id id of the mote
     * 
     */
    delete_mote = (mote_id) => {
        this.delete_item(undefined, 'o Mote', _ => MoteData.delete_mote(mote_id))
    }


    /**
     * Generic function to confirm action when a delete operation is requested
     * 
     * @param {object} item          the item to be deleted
     * @param {name}   name          of the model where the item belongs
     * @param {function} server_call function to be called if user confirms action
     */
    delete_item = (item, name, server_call) => {
        let do_delete_item = async () => {
            this.setState({ confirm_modal_loading_state: true })
            try {
                let result = await server_call(item)
                if (result === true) {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Sucesso',
                        confirm_modal_message       : `Registo eliminado`,
                        confirm_modal_handle_confirm: () => {
                            this.setState({ confirm_modal_open: false })
                            this.props.handle_data_change()
                        }
                    })
                } else {
                    // eslint-disable-next-line
                    throw  `Não foi possivel eliminar ${name}`
                }
            } catch (exception) {
                if (typeof exception.error_msg !== 'undefined') {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : exception.error_msg,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                } else {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : `Não foi possivel eliminar ${name}`,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                }
            }
            this.setState({ confirm_modal_loading_state: false })
        }

        this.setState({
            confirm_modal_open          : true,
            confirm_modal_header        : `Tem a certeza que deseja eliminar ${name}`,
            confirm_modal_message       : 'O registo será removido permanentemente',
            confirm_modal_handle_confirm: do_delete_item
        })
    }


    activate_configuration = async (configuration, new_state) => {
        if (new_state === false) {
            this.setState({
                confirm_modal_open: true,
                confirm_modal_header: 'Erro',
                confirm_modal_message: 'Deve escolher uma configuração para ativar e essa irá substituir a atual.',
                confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
            })
            return
        }
        this.send_mote_command(
            `Ativar configuração`,
            'Tem a certeza que deseja enviar o comando?',
            async () => await MoteOperations.change_configuration(
                configuration.mote_id,
                configuration.name
            )
        )
    }
    

    change_actuator_state = async (actuator_use, new_state) => {
        this.send_mote_command(
            `${new_state ? 'Ativar' : 'Desativar'} atuador`,
            'Tem a certeza que deseja enviar o comando?',
            async () => await MoteOperations.change_actuator_state(
                actuator_use.mote_id,
                actuator_use.actuator_id,
                actuator_use.num,
                new_state
            )
        )
    }


    change_sensor_state = async (sensor_use, new_state) => {
        this.send_mote_command(
            `${new_state ? 'Ativar' : 'Desativar'} sensor`,
            'Tem a certeza que deseja enviar o comando?',
            async () => await MoteOperations.change_sensor_state(
                sensor_use.mote_id,
                sensor_use.sensor_id,
                sensor_use.num,
                new_state
            )
        )
    }


    change_alert_state = async (alert_use, new_state) => {
        this.send_mote_command(
            `${new_state ? 'Ativar' : 'Desativar'} alerta`,
            'Tem a certeza que deseja enviar o comando?',
            async () => await MoteOperations.change_alert_state(
                alert_use.mote_id,
                alert_use.alert_id,
                new_state
            )
        )
    }


    change_trigger_state = async (trigger_use, new_state) => {
        this.send_mote_command(
            `${new_state ? 'Ativar' : 'Desativar'} trigger`,
            'Tem a certeza que deseja enviar o comando?',
            async () => await MoteOperations.change_trigger_state(
                trigger_use.mote_id,
                trigger_use.trigger_id,
                new_state
            )
        )
    }


    request_measurements = async (mote_id) => {
        this.send_mote_command(
            `Pedir medições`,
            'Tem a certeza que deseja enviar o comando?',
            async () => await MoteOperations.request_measurements(mote_id)
        )
    }


    request_status_report = async (mote_id) => {
        this.send_mote_command(
            `Pedir relatório de estado`,
            'Tem a certeza que deseja enviar o comando?',
            async () => await MoteOperations.request_status_report(mote_id)
        )
    }

    

    send_mote_command = (header, msg, server_call) => {
        let do_send_mote_command = async () => {
            this.setState({ confirm_modal_loading_state: true })
            try {
                let result = await server_call()
                if (result === true) {
                    this.setState({
                        confirm_modal_open: true,
                        confirm_modal_header: 'Sucesso',
                        confirm_modal_message: `Comando enviado`,
                        confirm_modal_handle_confirm: () => {
                            this.setState({ confirm_modal_open: false })
                            this.props.handle_data_change()
                        }
                    })
                } else {
                    // eslint-disable-next-line
                    throw `Não foi possivel efetuar a operação`
                }
            } catch (exception) {
                if (typeof exception.error_msg !== 'undefined') {
                    this.setState({
                        confirm_modal_open: true,
                        confirm_modal_header: 'Erro',
                        confirm_modal_message: exception.error_msg,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                } else {
                    this.setState({
                        confirm_modal_open: true,
                        confirm_modal_header: 'Erro',
                        confirm_modal_message: `Não foi possivel efetuar a operação`,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                }
            }
            this.setState({ confirm_modal_loading_state: false })
        }

        this.setState({
            confirm_modal_open: true,
            confirm_modal_header: header,
            confirm_modal_message: msg,
            confirm_modal_handle_confirm: do_send_mote_command
        })
    }



    render () {

        let mote      = this.props.data
        let mote_list = this.props.mote_list


        if (this.state.present === 'measurements') {
            // display this mote's measuremets
            return (
                <div>
                    <Breadcrumb>
                        <Breadcrumb.Section link onClick={() => this.setState({ present: undefined })}>
                            {mote.name}
                        </Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section active>{'Medições'}</Breadcrumb.Section>
                    </Breadcrumb>
                    <Divider/>
                    <MoteMeasurements mote_id={mote.id}/>
                </div>
            )
        } else if (this.state.present === 'other_data') {
            // display other data from the mote
            return (
                <div>
                    <Breadcrumb>
                        <Breadcrumb.Section link onClick={() => this.setState({ present: undefined })}>
                            {mote.name}
                        </Breadcrumb.Section>
                        <Breadcrumb.Divider />
                        <Breadcrumb.Section active>{'Outros Dados'}</Breadcrumb.Section>
                    </Breadcrumb>
                    <Divider />
                    <MoteExtraData mote_id={mote.id} />
                </div>
            )
        }

 
        return (
        <Grid>

            {/* Modals */}
            <MoteConfigurationModal
                mote={mote} open={this.state.conf_modal_open}
                handle_close={() => this.setState({ conf_modal_open: false, configuration_to_edit: undefined })}
                data={this.state.configuration_to_edit} when_done={this.props.handle_data_change}
            />
            <MoteConnectionModal
                mote={mote} open={this.state.connection_modal_open}
                handle_close={() => this.setState({ connection_modal_open: false})}
                mote_list={mote_list} when_done={this.props.handle_data_change}
            />
            <AlertUseModal
                mote={mote} open={this.state.alert_use_modal_open}
                handle_close={() => this.setState({ alert_use_modal_open: false })}
                when_done={this.props.handle_data_change}
            />
            <TriggerUseModal
                mote={mote} open={this.state.trigger_use_modal_open}
                handle_close={() => this.setState({ trigger_use_modal_open: false })}
                when_done={this.props.handle_data_change}
            />
            <SensorUseModal
                mote={mote} open={this.state.sensor_use_modal_open}
                handle_close={() => this.setState({ sensor_use_modal_open: false, sensor_to_edit: undefined})}
                data={this.state.sensor_to_edit} when_done={this.props.handle_data_change}
            />
            <ActuatorUseModal
                mote={mote} open={this.state.actuator_use_modal_open}
                handle_close={() => this.setState({ actuator_use_modal_open: false, actuator_to_edit: undefined})}
                data={this.state.actuator_to_edit} when_done={this.props.handle_data_change}
            />

            <MoteModal
                open={this.state.present === 'edit_mote'}
                handle_close={() => this.setState({ present: null })}
                data={mote} when_done={this.props.handle_data_change}
            />


            <Modal 
                style={{ marginTop: '0 !important', marginLeft: 'auto', marginRight: 'auto' }}
                size='mini' 
                open={this.state.confirm_modal_open}
                onClose={() => this.setState({ confirm_modal_open: false })}>
                <Modal.Header>
                    {this.state.confirm_modal_header}
                </Modal.Header>
                <Modal.Content>
                    <p>{this.state.confirm_modal_message}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button 
                        negative onClick={() => this.setState({ confirm_modal_open: false, confirm_modal_loading_state: false })}
                        disabled={this.state.confirm_modal_loading_state} >
                        Cancelar
                    </Button>
                    <Button onClick={() => this.state.confirm_modal_handle_confirm()}
                        disabled={this.state.confirm_modal_loading_state}
                        loading={this.state.confirm_modal_loading_state}
                        positive icon='checkmark' labelPosition='right' content='Ok' />
                </Modal.Actions>
            </Modal>
            {/* end Modals */}


            <Grid.Row style={{ paddingBottom: 0 }} >
                <Grid.Column width={5} verticalAlign='middle' >
                    <Header  >
                        <Header.Content>
                            {mote.name}
                            <Header.Subheader content={`#  ${mote.id}`} />
                        </Header.Content>
                    </Header>
                </Grid.Column>
                <Grid.Column width={10}>
                    <Button.Group compact basic floated='right' size='small'>
                        <Button 
                            icon labelPosition='left' 
                            name='edit_mote' 
                            onClick={() => this.setState({ present: 'edit_mote' })} 
                        >
                            <Icon name='edit' />
                            { 'Alterar dados do mote'}
                        </Button>
                        <Button 
                            icon labelPosition='left' 
                            name='measurements' 
                            onClick={() => this.setState({ present: 'measurements' })} 
                        >
                            <Icon name='thermometer' />
                            { 'Ver Medições'}
                        </Button>
                        <Button 
                            icon labelPosition='left' 
                            name='other_data'
                            onClick={() => this.setState({ present: 'other_data' })} 
                        >
                            <Icon name='table' />
                            { 'Ver Dados Produzidos'}
                        </Button>
                    </Button.Group>
                </Grid.Column>
                <Grid.Column width={1} style={{ marginTop: '4px' }}>
                    <Dropdown item icon='ellipsis vertical' direction='left'>
                        <Dropdown.Menu>
                            <Dropdown.Item onClick={() => this.request_measurements(mote.id)}>
                                Pedir Medições
                            </Dropdown.Item>
                            <Dropdown.Item onClick={() => this.request_status_report(mote.id)}>
                                Pedir Relatório de Estado
                            </Dropdown.Item>
                            <Dropdown.Item onClick={() => this.delete_mote(mote.id)}>
                                Eliminar mote
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    <Divider />
                </Grid.Column>
            </Grid.Row>
            
            <Grid.Row>

                <Grid.Column width={9}>
                    <Item.Group>
                        <Item>
                            <Item.Content>
                                <Item.Header>Último relatório de estado</Item.Header>
                                {mote.last_status_report !== null ? (
                                    <Item.Description>
                                        <span>
                                            {Moment(mote.last_status_report).format('HH:mm DD-MM-YYYY')}
                                        </span>
                                    </Item.Description>
                                ) : (
                                    <Item.Extra>
                                        <span><i>Nenhum recebido</i></span>
                                    </Item.Extra>
                                )}
                            </Item.Content>
                        </Item>
                        <Item>
                            <Item.Content>
                                <Item.Header content='Intervalo entre relatórios'/>
                                <Item.Description>
                                    <div className='ui input small' style={{ display: 'block' }} >
                                        {mote.max_time_with_no_report !== null ? (
                                            <Item.Description>
                                                <span>{Moment(mote.max_time_with_no_report, 'HH:mm:ss').format('HH:mm:ss')}</span>
                                            </Item.Description>
                                        ) : (
                                            <Item.Extra>
                                                <span><i>Não definido</i></span>
                                            </Item.Extra>
                                        )}
                                        {/*<Flatpickr
                                            options={{ 
                                                enableTime: true, 
                                                noCalendar: true, 
                                                dateFormat: 'H:i',
                                                time_24hr: true
                                            }}
                                            ref='mote_max_time_with_no_report' 
                                        />*/}
                                    </div>
                                </Item.Description>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>

                <Grid.Column width={7}>
                    <Item.Group>
                        <Item>
                            <Item.Content>
                                <Item.Header>Localização</Item.Header>
                                <Item.Description>
                                    <Map 
                                        height='10em' 
                                        width='25em' 
                                        map_id='mote_detail_map'
                                        center={{ lat: mote.location_lat, lng: mote.location_lng }}
                                        markers={[{ lat: mote.location_lat, lng: mote.location_lng }]}
                                        zoom={15}
                                    />
                                </Item.Description>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>

            </Grid.Row>

            <Grid.Row >
                <Grid.Column width={2} >
                    <Item.Group>
                        <Item>
                            <Item.Content>
                                <Item.Header>{mote.active ? 'IP atual' : 'Último IP'}</Item.Header>
                                {mote.current_ip !== null ? (
                                    <Item.Description>
                                        <span>{mote.current_ip}</span>
                                    </Item.Description>
                                ) : (
                                    <Item.Extra>
                                        <span><i>Desconhecido</i></span>
                                    </Item.Extra>
                                )}
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>
                
                <Grid.Column width={5} >
                    <Item.Group>
                        <Item>
                            <Item.Content>
                                <Item.Header as='h6'>Algoritmo de encriptação</Item.Header>
                                <Item.Description>
                                    {mote.crypto_alg !== null ? (
                                        <Item.Description>
                                            <span>{mote.crypto_alg}</span>
                                        </Item.Description>
                                    ) : (
                                        <Item.Extra>
                                            <span><i>Não definido</i></span>
                                        </Item.Extra>
                                    )}
                                </Item.Description>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>
                
                <Grid.Column width={4} >
                    <Item.Group>
                        <Item>
                            <Item.Content>
                                <Item.Header>Chave de encriptação</Item.Header>
                                <Item.Description>
                                    {mote.crypto_key !== null ? (
                                        <Item.Description>
                                            <span>{this.state.crypto_key_hidden ? '*****************' : mote.crypto_key}</span>
                                        </Item.Description>
                                    ) : (
                                        <Item.Extra>
                                            <span><i>Não definido</i></span>
                                        </Item.Extra>
                                    )}
                                </Item.Description>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>

                <Grid.Column width={2} verticalAlign='bottom' >
                    <Button
                        basic
                        floated='left'
                        onClick={() =>
                            this.setState({ crypto_key_hidden: !this.state.crypto_key_hidden })}
                        content={this.state.crypto_key_hidden ? 'Mostrar' : 'Esconder'}
                    />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    <Divider />
                </Grid.Column>
            </Grid.Row>


            <Grid.Row columns={1} >
                <Grid.Column>
                    <TableWithPagination
                        items_per_page={10}
                        name='Configurações'
                        header_fields={this.configurations_table_columns}
                        row_key='name'
                        data={mote.configurations.map(conf => ({
                            ...conf,
                            handle_edit         : () => this.edit_configuration(conf),
                            handle_delete       : () => this.delete_configuration(conf),
                            measurement_period  : `${conf.measurement_period} segundos`,
                            send_data_period    : `${conf.send_data_period} segundos`,
                            status_report_period: `${conf.status_report_period} segundos`,
                            use_crypto          : conf.use_crypto ? 'Sim' : 'Não',
                            active              : <Radio 
                                toggle
                                checked={conf.active}
                                label={conf.active ? 'Sim' : 'Não'}
                                onChange={(e, { checked }) => this.activate_configuration(conf, checked)} />,
                            use_crypto_positive: conf.use_crypto,
                            use_crypto_negative: !conf.use_crypto,
                            active_positive    : conf.active,
                            active_negative    : !conf.active
                        }))}
                        actions='both'
                        footer_fields={[
                            {
                                props: { colSpan: '7', key: 'add' },
                                content: (
                                    <Button 
                                        floated='right' icon 
                                        labelPosition='left' 
                                        primary size='small'
                                        onClick={() => this.setState({ conf_modal_open: true })}
                                    >
                                        <Icon name='plus' /> Adicionar Configuração
                                    </Button>
                                )
                            },
                        ]}
                    />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    <Divider  />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row columns={1} >
                <Grid.Column>
                    <TableWithPagination
                        items_per_page={10}
                        name='Conexões'
                        row_key='other_mote_id'
                        header_fields={this.connections_table_columns}
                        data={mote.conns_from_here.map(conn => ({
                            handle_delete: () => this.delete_connection(conn),
                            other_mote_id: `${conn.mote_there.id}${conn.direction}`,
                            other_mote_name: conn.mote_there.name,
                            direction: conn.direction === 'Bi' 
                                ? `${mote.name} ⟷ ${conn.mote_there.name}`
                                : (conn.direction === 'Here_to_There' 
                                ? `${mote.name} → ${conn.mote_there.name}` // this mote is the here
                                : `${mote.name} ← ${conn.mote_there.name}`) // There to Here
                        })).concat(mote.conns_to_here.map(conn => ({
                            handle_delete: () => this.delete_connection(conn),
                            other_mote_id: `${conn.mote_here.id}${conn.direction}`,
                            other_mote_name: conn.mote_here.name,
                            direction: conn.direction === 'Bi'
                                ? `${mote.name} ⟷ ${conn.mote_here.name}`
                                : (conn.direction === 'Here_to_There' 
                                ? `${mote.name} ← ${conn.mote_here.name}` // this mote is the there
                                : `${mote.name} → ${conn.mote_here.name}`) // There to Here
                        })))}
                        actions='delete'
                        footer_fields={[
                            {
                                props: { colSpan: this.connections_table_columns.length+1, key: 'add' },
                                content: (
                                    <Button 
                                        floated='right' icon 
                                        labelPosition='left' 
                                        primary size='small'
                                        onClick={() => this.setState({ connection_modal_open: true })}
                                    >
                                        <Icon name='plus' /> Adicionar Conexão
                                    </Button>
                                ) 
                            },
                        ]}
                    />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    <Divider  />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row columns={1} >
                <Grid.Column>
                    <TableWithPagination
                        items_per_page={10}
                        name='Sensores utilizados'
                        row_key='num'
                        header_fields={this.sensors_table_columns}
                        data={mote.sensors.map(su => ({
                            ...su,
                            handle_edit    : () => this.edit_sensor_use(su),
                            handle_delete  : () => this.delete_sensor_use(su),
                            name           : su.sensor.name,
                            interface_num  : su.interface_num === -1 ? '-' : su.interface_num,
                            activated_at   : su.active ? 
                                Moment(su.activated_at).format('HH:mm DD-MM-YYYY') : '-',
                            deactivated_at : !su.active ? 
                                Moment(su.deactivated_at).format('HH:mm DD-MM-YYYY') : '-',
                            active         : <Radio 
                                toggle
                                checked={su.active}
                                label={su.active ? 'Sim' : 'Não'}
                                onChange={(e, { checked }) => this.change_sensor_state(su, checked)} />,
                            active_positive: su.active,
                            active_negative: !su.active
                        }))}
                        actions='both'
                        footer_fields={[
                            {
                                props: { colSpan: this.sensors_table_columns.length+1, key: 'add' },
                                content: (
                                    <Button 
                                        floated='right' icon 
                                        labelPosition='left' 
                                        primary size='small'
                                        onClick={() => this.setState({ sensor_use_modal_open: true })}
                                    >
                                        <Icon name='plus' /> Adicionar Sensor ao Mote
                                    </Button>
                                ) 
                            }
                        ]}
                    />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    <Divider  />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row columns={1} >
                <Grid.Column>
                    <TableWithPagination
                        items_per_page={10}
                        name='Atuadores utilizados'
                        row_key='num'
                        header_fields={this.actuators_table_columns}
                        data={mote.actuators.map(au => ({
                            ...au,
                            handle_delete  : () => this.delete_actuator_use(au),
                            handle_edit    : () => this.edit_actuator_use(au),
                            name           : au.actuator.name,
                            interface_num  : au.interface_num === -1 ? '-' : au.interface_num,
                            activated_at   : au.active ? 
                                Moment(au.activated_at).format('HH:mm DD-MM-YYYY') : '-',
                            deactivated_at : !au.active ? 
                                Moment(au.deactivated_at).format('HH:mm DD-MM-YYYY') : '-',
                            active         : <Radio 
                                toggle
                                checked={au.active}
                                indeterminate
                                label={au.active ? 'Sim' : 'Não'}
                                onChange={(e, { checked }) => this.change_actuator_state(au, checked)} />,
                            active_positive: au.active,
                            active_negative: !au.active
                        }))}
                        actions='both'
                        footer_fields={[
                            {
                                props: { colSpan: this.actuators_table_columns.length+1, key: 'add' },
                                content:
                                    <Button 
                                        floated='right' icon 
                                        labelPosition='left' 
                                        primary size='small'
                                        onClick={() => this.setState({ actuator_use_modal_open: true })}
                                    >
                                        <Icon name='plus' /> Adicionar Actuator ao Mote
                                    </Button>
                            },
                        ]}
                    />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    <Divider  />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row columns={1} >
                <Grid.Column>
                    <TableWithPagination
                        items_per_page={10}
                        name='Alertas utilizados'
                        row_key='id'
                        header_fields={this.alerts_table_columns}
                        data={mote.alerts.map(au => ({
                            ...au,
                            handle_delete  : () => this.delete_alert_use(au),
                            id             : au.alert.id,
                            name           : au.alert.name,
                            activated_at   : au.active ? 
                                Moment(au.activated_at).format('HH:mm DD-MM-YYYY') : '-',
                            deactivated_at : !au.active ? 
                                Moment(au.deactivated_at).format('HH:mm DD-MM-YYYY') : '-',
                            active         : <Radio 
                                toggle
                                checked={au.active}
                                indeterminate
                                label={au.active ? 'Sim' : 'Não'}
                                onChange={(e, { checked }) => this.change_alert_state(au, checked)} />, 
                            active_positive: au.active,
                            active_negative: !au.active
                        }))}
                        actions='delete'
                        footer_fields={[
                            {
                                props: { colSpan: this.alerts_table_columns.length+1, key: 'add' },
                                content: 
                                    <Button 
                                        floated='right' icon 
                                        labelPosition='left' 
                                        primary size='small'
                                        onClick={() => this.setState({ alert_use_modal_open: true })}
                                    >
                                        <Icon name='plus' /> Adicionar Alerta ao Mote
                                    </Button>
                            },
                        ]}
                    />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row>
                <Grid.Column width={16}>
                    <Divider  />
                </Grid.Column>
            </Grid.Row>

            <Grid.Row columns={1} >
                <Grid.Column>
                    <TableWithPagination
                        items_per_page={10}
                        name='Triggers utilizados'
                        row_key='id'
                        header_fields={this.triggers_table_columns}
                        data={mote.triggers.map(tu => ({
                            ...tu,
                            handle_delete  : () => this.delete_trigger_use(tu),
                            id             : tu.trigger.id,
                            name           : tu.trigger.name,
                            activated_at   : tu.active ? 
                                Moment(tu.activated_at).format('HH:mm DD-MM-YYYY') : '-',
                            deactivated_at : !tu.active ? 
                                Moment(tu.deactivated_at).format('HH:mm DD-MM-YYYY') : '-',
                            active         : <Radio 
                                toggle
                                checked={tu.active}
                                indeterminate
                                label={tu.active ? 'Sim' : 'Não'}
                                onChange={(e, { checked }) => this.change_trigger_state(tu, checked)} />,
                            active_positive: tu.active,
                            active_negative: !tu.active
                        }))}
                        actions='delete'
                        footer_fields={[
                            {
                                props: { colSpan: this.triggers_table_columns.length+1, key: 'add' },
                                content:
                                    <Button 
                                        floated='right' icon 
                                        labelPosition='left' 
                                        primary size='small'
                                        onClick={() => this.setState({ trigger_use_modal_open: true })}
                                    >
                                        <Icon name='plus' /> Adicionar Trigger ao Mote
                                    </Button>
                            },
                        ]}
                    />
                </Grid.Column>
            </Grid.Row>

        </Grid>
        )
    }

}