import React from 'react'

import {

    Button,
    Dimmer,
    Grid,
    Header,
    Icon,
    Input,
    List,
    Loader, 
    Message,
    Pagination

} from 'semantic-ui-react'

import * as MoteData from '../../../common/motes_data'
import { DataFetchState } from '../../../common/misc'
import MoteDetails        from './MoteDetails'
import MoteModal          from './MoteModal'


export default class MoteListing extends React.Component {


    constructor (props) {
        super(props)
        this.MOTES_PER_PAGE = 10

        this.state = {
            data_fetch_state: DataFetchState.PENDING,
            mote_pres_data_fetch_state: null,
            data_fetch_error: 'Verifique a sua conexão á Internet',

            pagination_position: 0,

            motes          : [],
            // mote being presented in details pane
            mote_presenting: null
        }
    }


    /**
     * Get motes
     * 
     * @param {number} skip how many items to skip
     * @param {number} take how many items to return
     * 
     * @return {object[]} the retrieved data 
     */
    get_data = async (from, to) => {
        this.setState({ data_fetch_state: DataFetchState.PENDING })
        try {

            let motes = await MoteData.load_all()
            motes.forEach((mote) => { mote.show = true; mote.with_details = false })
            this.setState({
                motes: motes,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })

            return motes

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Called when a mote is clicked in the list
     * Fetches the details of the mote (alerts, triggers, sensors, configurations, etc...)
     * 
     * @param {object} mote the mote to present 
     */
    handle_mote_choice = async (mote) => {
        this.setState({ mote_pres_data_fetch_state: DataFetchState.PENDING })

        try {
            if (mote.with_details === true) {
                this.setState({
                    mote_presenting: mote,
                    mote_pres_data_fetch_state: DataFetchState.DATA_FETCHED
                })
                return // data already fecthed
            }

            let mote_details  = await MoteData.load_details(mote.id)
            mote.with_details = true
            Object.assign(mote, mote_details)

            this.setState({ 
                mote_presenting: mote,
                mote_pres_data_fetch_state: DataFetchState.DATA_FETCHED
            })

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    mote_pres_data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ mote_pres_data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Called when data is changed in a sub component
     */
    handle_data_change = async () => {
        
        let current_id = this.state.mote_presenting != null
            ? this.state.mote_presenting.id : -1
        this.setState({ mote_presenting: null })
        
        let motes = await this.get_data()
        
        if (this.state.mote_presenting !== undefined) {
            let new_mote_presenting =
                motes.find(m => m.id === current_id)

            if (new_mote_presenting !== undefined)
                this.handle_mote_choice(new_mote_presenting)
        }
    }


    /**
     * Called when a search is made
     * 
     * @param {string} value value entered by the user
     */
    handle_search = (value) => {
        let new_motes = this.state.motes.slice()
        value = value.toLowerCase()
        new_motes.forEach(mote => {
            if (value === null || value === undefined || value === '') {
                mote.show = true
                return
            } else if (!mote.name.toLowerCase().includes(value) &&
                (mote.purpose_description != null
                && !mote.purpose_description.toLowerCase().includes(value))) {
                mote.show = false
            } else {
                mote.show = true
            }
        })

        this.setState({ motes: new_motes })
    }


    async componentDidMount () {
        // get the first list of motes
        await this.get_data(0, this.MOTES_PER_PAGE)
    }


    render () {

        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Não foi possivel carregar a lista de motes'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }

        let motes_to_show = this.state.motes.filter(mote => mote.show !== false)
        let nr_of_motes_to_show = motes_to_show.length
        motes_to_show = motes_to_show.slice( // just show `MOTES_PER_PAGE` items per page
            this.state.pagination_position*this.MOTES_PER_PAGE,
            this.state.pagination_position*this.MOTES_PER_PAGE + this.MOTES_PER_PAGE 
        )
        let nr_pages = Math.ceil(nr_of_motes_to_show / this.MOTES_PER_PAGE)
        

        return (
        <div>

        <MoteModal
            open={this.state.mote_modal_open}
            handle_close={() => this.setState({ mote_modal_open: false})}
            when_done={this.handle_data_change}
        />

        <Grid divided>
            <Grid.Row columns={2}>

                <Grid.Column width={5} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                    <span>
                        <Input 
                            iconPosition='left' 
                            placeholder='Procurar...' 
                            onChange={(_, { value }) => this.handle_search(value)}
                        >
                            <Icon name='search' />
                            <input />
                        </Input>
                        <Button
                            primary
                            floated='right'
                            icon labelPosition='right'
                            onClick={() => this.setState({ mote_modal_open: true })}
                        >
                            <Icon name='plus' />
                            { 'Novo mote'}
                        </Button>
                    </span>

                    <List divided relaxed selection >
                        {motes_to_show.map(mote => (
                            <List.Item 
                                active={this.state.mote_presenting !== null && this.state.mote_presenting.id === mote.id}
                                key={mote.id}
                                onClick={() => this.handle_mote_choice(mote)}
                            >
                                <List.Icon name='microchip' size='large' verticalAlign='middle'/>
                                <List.Content>
                                    <List.Header >{mote.name}</List.Header>
                                    <List.Description >
                                        {mote.active ? (
                                            <Header as='h5' content={'Ativo'} color='green'/>
                                        ): (
                                            <Header as='h5' content={'Inativo'} color='red' />
                                        )}
                                        {mote.purpose_description}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                        ))}
                    </List>
                    <Pagination
                        activePage={this.state.pagination_position + 1} 
                        totalPages={nr_pages} 
                        size='mini'
                    />
                </Grid.Column>

                <Grid.Column width={11} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                    {this.state.mote_pres_data_fetch_state === DataFetchState.PENDING ? (
                        <Dimmer active inverted style={{ marginTop: '10em' }}>
                            <Loader content='A carregar dados' />
                        </Dimmer>
                    ) : (this.state.mote_pres_data_fetch_state === DataFetchState.FAILED ? (
                        <Message negative style={{ margin: '5em' }}>
                            <Message.Header
                                content='Não foi possivel carregar os dados do mote'
                            />
                            <p>{this.state.data_fetch_error}</p>
                        </Message>
                    ) : (this.state.mote_presenting !== null ? (
                        <MoteDetails 
                            data={this.state.mote_presenting} 
                            mote_list={this.state.motes}
                            handle_data_change={this.handle_data_change}
                        />
                    ) : (
                        <Message info icon>
                            <Icon name='microchip' />
                            <Message.Content>
                                <Message.Header content='Motes' />
                                <p>{'Clique num item na lista para ver os seus dados'}</p>
                            </Message.Content>
                        </Message>
                    )))}
                </Grid.Column>

            </Grid.Row>
        </Grid>
        </div>
        )
    }

}