import React from 'react'

import { Input, Select, TextArea } from 'semantic-ui-react'

import * as MoteData from '../../../common/motes_data'
import FormModal     from '../FormModal'

import * as Moment   from 'moment'


/**
 * View to create or update a mote
 * 
 * @prop {number}   mote the mote to update (in case of update)
 */
export default class MoteModal extends React.Component {

    CRYPTO_ALGS = [
        { key: 'AES-CCM', value: 'AES-CCM', text: 'AES-CCM', key_len: 16  },
        // { key: 'AES-CBC', value: 'AES-CBC', text: 'AES-CBC' },
        // { key: 'DES',     value: 'DES',     text: 'DES'     }
    ]

    FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    placeholder: 'Nome do mote',
                    control    : Input,
                    type       : 'text'
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                {
                    key        : 'location_lat',
                    label      : 'Latitude da localização',
                    required   : true,
                    placeholder: 'Latitude',
                    control    : Input,
                    type       : 'number'
                },
                {
                    key        : 'location_lng',
                    label      : 'Longitude da localização',
                    required   : true,
                    placeholder: 'Longitude',
                    control    : Input,
                    type       : 'number'
                }
            ],
            group_key: 'b'
        },
        {
            fields: [{
                key        : 'max_time_with_no_report',
                label      : 'Tempo máximo sem relatórios de estado',
                required   : false,
                control    : Input,
                type       : 'text',
                placeholder: 'HH:mm:ss'
            }],
            group_key: 'c'
        },
        {
            fields: [
                {
                    key        : 'crypto_alg',
                    label      : 'Encriptação de pacotes',
                    placeholder: 'Algoritmo utilizado para criptografar pacotes',
                    required   : false,
                    control    : Select,
                    options    : this.CRYPTO_ALGS
                },
                {
                    key        : 'crypto_key',
                    label      : 'Chave de encriptação',
                    placeholder: 'Chave utilizada para criptografar pacotes',
                    required   : false,
                    control    : Input,
                    type       : 'text'
                }
            ],
            group_key: 'd'
        },
        {
            fields: [
                {
                    key        : 'other_data',
                    label      : 'Outros dados',
                    placeholder: 'Dados extra',
                    required   : false,
                    control    : TextArea,
                    type       : 'text'
                }
            ],
            group_key: 'e'
        },
        {
            fields: [
                {
                    key        : 'purpose_description',
                    label      : 'Descrição',
                    placeholder: 'Descrição',
                    required   : false,
                    control    : TextArea,
                    type       : 'text'
                }
            ],
            group_key: 'f'
        }
    ]


    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {
        if (data.max_time_with_no_report !== undefined && data.max_time_with_no_report !== null) {
            let max_time_with_no_report = Moment(data.max_time_with_no_report, 'HH:mm,ss')
            if (!max_time_with_no_report.isValid()) {
                return 'Utilize o formato "HH:mm:ss" para o campo "Tempo máxmimo sem relatórios de estado"'
            }
            data.max_time_with_no_report = max_time_with_no_report.format('HH:mm:ss')
        }

        if ((data.crypto_alg !== undefined && data.crypto_alg !== null) && 
           (data.crypto_key === undefined || data.crypto_key === null || data.crypto_key === '')) {
            return 'A chave criptográfica deve ser preenchida juntamente com o algoritmo'
        }

        console.log('the data', data)
        let alg = this.CRYPTO_ALGS.find(c => c.key === data.crypto_alg)
        if (data.crypto_key != null && data.crypto_key.length !== alg.key_len) {
            return `A chave para o algoritmo ${alg.text} deve ter ${alg.key_len} caracteres`
        }


        data.active = data.active === undefined ? false : data.active

        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        try {
            let result = await MoteData.save_mote(data, update)
            if (!result) {
                return err_msg
            } else {
                return true
            }
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }


    


    render () {
        let title = this.props.data !== undefined && this.props.data.name !== undefined 
            ? `Atualizar mote ${this.props.data.name}`
            : `Novo mote`

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }

}