import React from 'react'

import { Button, Grid, Icon, List, Message, Modal, Radio } from 'semantic-ui-react'


export default class ItemChangeState extends React.Component {

    constructor (props) {
        super(props)

        this.state = {
            items       : [],
            is_saving   : false,
            with_message: false,
            messages    : []
        }
    }

    static getDerivedStateFromProps(new_props, old_props) {
        if (new_props.open) {
            return ({
                ...new_props,
                is_saving: false,
                with_error: false,
                error_message: ''
            })
        } else
            return null 
    }


    item_state_changed = (i, new_state) => {
        let items = this.state.items.slice()
        items[i].state = new_state
        this.setState({ items: items })
    }


    save = async () => {
        console.log(this.state)
        this.setState({ is_saving: true })
        let result = await this.props.save(this.state.items)
        if (result != null) {
            this.setState({ with_error: true, error_message: result })
        }
        this.setState({ is_saving: false })
    }


    render () {

        let modal_style = {
            marginTop: '0 !important',
            marginLeft: 'auto',
            marginRight: 'auto',
        }

        return (
        <Modal
            open={this.props.open}
            style={modal_style}
            size='tiny'
            dimmer='blurring'
            onClose={() => this.props.handle_close()}>

            <Modal.Header >{this.props.title}</Modal.Header>
            <Modal.Content>
                <List>
                    {this.state.items.map((item, i) => (
                        <List.Item key={item.key}>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={1}></Grid.Column>
                                    <Grid.Column width={11}>{item.name}</Grid.Column>
                                    <Grid.Column width={2}>
                                        <Radio
                                            toggle
                                            floated='right'
                                            checked={item.state}
                                            onChange={(_, { checked }) => this.item_state_changed(i, checked)}
                                        />
                                    <Grid.Column width={2}></Grid.Column>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </List.Item>
                    ))}
                </List>
                <Message hidden={!this.state.with_message}>
                    <Message.List>
                    {this.state.messages.map(msg => (
                        <Message.Item style={{ color: msg.error ? 'red' : undefined }}>{msg.msg}</Message.Item>
                    ))}
                    </Message.List>
                </Message>              
            </Modal.Content>
            <Modal.Actions>
                <Button
                    basic
                    color='red'
                    disabled={this.state.is_saving}
                    onClick={this.props.handle_close} >
                    <Icon name='remove' /> Sair
                </Button>
                <Button
                    color='green'
                    disabled={this.state.is_saving}
                    loading={this.state.is_saving}
                    onClick={this.save} >
                    <Icon name='checkmark' /> Confirmar
                </Button>
            </Modal.Actions>
        </Modal>
        )
    }

}