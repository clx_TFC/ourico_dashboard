import React from 'react'

import { Select, Dimmer, Loader, Message, Modal } from 'semantic-ui-react'

import * as MoteData from '../../../common/motes_data'

import { DataFetchState } from '../../../common/misc'

import FormModal from '../FormModal'


/**
 * Create a new alert use in a mote
 * 
 * @prop {object}   mote         data of the mote
 * @prop {string}   open         indicates mote visibility
 * @prop {function} handle_close called when user requests to close the modal
 */
export default class AlertUseModal extends React.Component {


    FORM_SPEC = [
        {
            fields: [{ // name
                key        : 'alert_id',
                label      : 'Alerta',
                dont_update: true,
                required   : true, 
                placeholder: 'O alerta que será ativado',
                control    : Select,
                options    : []
            }],
            group_key: 'a'
        }
    ]

    constructor (props) {
        super(props)

        this.state = {
            alerts          : [],
            data_fetch_state: DataFetchState.PENDING,
            data_fetch_error: ''
        }
    }

    
    async componentDidMount () {
        try {

            let alerts = await MoteData.load_non_used_alerts(this.props.mote.id)
            console.log('alerts', alerts)
            this.setState({
                alerts          : alerts,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })

        } catch (exception) {
            console.log(exception)
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {

        data.mote_id = this.props.mote.id

        let err_msg = 'Não foi possível guardar os dados'

        try {
            let result = await MoteData.save_alert_use(this.props.mote.id, data)
            if (!result) {
                return err_msg
            } else {
                return true
            }
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }

    render () {

        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            if (this.props.open)
                return (
                    <Dimmer active inverted style={{ marginTop: '10em' }}>
                        <Loader content='A carregar dados' />
                    </Dimmer>
                )
            else return (<span hidden></span>)
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Modal
                    open={this.props.open}
                    style={{marginTop: '0 !important',marginLeft: 'auto',marginRight: 'auto'}}
                    onClose={this.props.handle_close}
                    header='Error'
                    content={
                        <Message negative style={{ margin: '5em' }}>
                            <Message.Header content='Não foi possivel carregar a lista de alertas' />
                            <p>{this.state.data_fetch_error}</p>
                        </Message>
                    }
                    actions={[
                        { key: 'ok', content: 'Ok', positive: true, onClick: this.props.handle_close }
                    ]}
                />
            )
        }

        let title = `Adicionar alerta ao mote ${this.props.mote.name}`

        this.FORM_SPEC[0].fields[0].options = this.state.alerts
            .filter(alert => !alert.uses.find(au => au.mote_id === this.props.mote.id))
            .map(alert => ({
                key: alert.id, value: alert.id, text: alert.name
            }))

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                when_done={this.props.when_done}
            />
        )
    }

}