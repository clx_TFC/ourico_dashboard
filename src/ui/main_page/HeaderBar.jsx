import * as React from 'react'
import {

    Button,
    Container,
    Divider,
    Dropdown,
    Menu

} from 'semantic-ui-react'

import { Link, withRouter as WithRouter } from 'react-router-dom'

import * as Storage  from '../../common/app_storage'
import * as RtEvents from '../../api/rt_events'
import * as Common   from '../../api/common'
import * as Control  from '../../api/control'
import * as Moment   from 'moment'

import EventNotification from './EventNotification'


/**
 * Implements the header bar of the application
 *
 * @prop {string[]} sections sections to where user can navigate in the application
 * @prop {string}   logo     url/path to application logo
 */
export class HeaderBar extends React.Component {

    constructor (props) {
        super(props)

        let active_events = Storage.get(Storage.Global.ACTIVE_EVENTS)
        if (active_events === null)
            active_events = []
        
        console.log('active events ', active_events)
        this.state = {
            active_section: props.active,
            active_events : active_events,

            event_listener: null
        }
    }


    logout = async () => {
        try {
            let token = Common.get_token()
            await Control.logout(token)
            Common.remove_token()
        } catch (exception) {
            console.error(exception)
        }
        this.props.handle_logged_out()
    }


    componentWillMount () {
        // listen to route changes to set the corresponding header
        // item as active
        this.props.history.listen(() => {
            this.setState({ 
                active_section: this.props.history.location.pathname.split('/')[1]
            })
        })
    }


    componentDidMount () {
        let handle = RtEvents.register_listener((e) => this.handle_new_event(e))
        this.setState({ event_listener: handle })
    }


    componentWillUnmount () {
        RtEvents.remove_listener(this.state.event_listener)
    }


    /**
     * Called when new mote event is received
     * 
     * @param {RtEvents.RtEvent} event
     */
    handle_new_event = (event) => {
        event.rx_date_time = Moment().format("HH:mm DD-MM-YYYY")
        console.log('Received event', event)
        let active_events = this.state.active_events.slice()
        if (active_events.length >= 10) {
            active_events.splice(0, active_events.length)
        }
        active_events.push(event)
        this.setState({ active_events: active_events })
        Storage.set(Storage.Global.ACTIVE_EVENTS, active_events)
        new Audio("/notification_sound.mp3").play()
    }


    /**
     * Delete an event
     * @param {number} idx index in the event list
     */
    delete_event = (idx) => {
        let active_events = this.state.active_events.slice()
        active_events.splice(idx, 1)
        this.setState({ active_events: active_events })
        Storage.set(Storage.Global.ACTIVE_EVENTS, active_events)
    }


    delete_all_events = () => {
        this.setState({ active_events: [] })
        Storage.set(Storage.Global.ACTIVE_EVENTS, [])
    }


    render () {
        
        let notifications_class = 'no_notifications'
        let notifications_title = 'Sem Eventos'
        if (this.state.active_events.length > 0) {
            notifications_class = 'active_notifications'
            notifications_title = `${this.state.active_events.length} Evento(s)`
        }

        return (
            <Menu secondary className="header_bar">
                <Container>
                    {/* application logo */}
                    <Menu.Item header position='left'>
                        <img src={this.props.logo} alt='logo'/>
                    </Menu.Item>
                    <Menu.Item>
                        <Divider vertical hidden />
                    </Menu.Item>

                    {/* sections */}
                    {Object.keys(this.props.sections).map(section =>
                        <Menu.Item
                            className='header_bar_item'
                            key={section}
                            name={section}
                            active={this.state.active_section === section}>
                            <Link 
                                to={`/${section}`} 
                                onClick={() => this.setState({ active_section: section })}
                            >
                                {this.props.sections[section]}
                            </Link>
                        </Menu.Item>
                    )}

                    <Menu.Item>
                        <Divider vertical hidden />
                    </Menu.Item>
                    <Menu.Item>
                        <Divider vertical hidden />
                    </Menu.Item>

                    {/* notificacoes de eventos */}
                    <Menu.Menu position='right' style={{ padding: '0.5em' }}>
                        <Dropdown
                            text={`${this.state.active_events.length} Notificações(s)`}
                            icon='alarm'
                            labeled button
                            className={`icon ${notifications_class}`}>
                            <Dropdown.Menu>
                                <Dropdown.Header>
                                    {notifications_title}
                                    {this.state.active_events.length > 0 ? (
                                        <Button basic floated='right' compact onClick={() => this.delete_all_events()}>
                                            Apagar Tudo
                                        </Button>
                                    ) : (null)}
                                </Dropdown.Header>
                                <Dropdown.Divider />
                                {this.state.active_events.map((event, i) => (
                                    <Dropdown.Item style={{ width: '40em' }} key={i}>
                                        <EventNotification 
                                            data={event} 
                                            event_removed={() => this.delete_event(i)}
                                        />
                                    </Dropdown.Item>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                    </Menu.Menu>


                    {/* logout */}
                    <Menu.Menu position='right' style={{ padding: '0.5em' }}>
                        <Dropdown item icon='user' className={`icon header_bar_item header_bar__user`}>
                            <Dropdown.Menu>
                                <Dropdown.Item onClick={this.logout} icon='sign out' content='Logout' />
                            </Dropdown.Menu>
                        </Dropdown>
                    </Menu.Menu>


                </Container>
            </Menu>
        )
    }
}


export default WithRouter(HeaderBar)