import * as React from 'react'
import {

    Header,
    Grid,
    Icon,
    Segment,
    Statistic

} from 'semantic-ui-react'


export default class DashboardMainInfo extends React.Component {
    render () {

        console.log('dashboard main info props', this.props)

        return (
            <Segment
                style={{ marginLeft: '1em', marginRight: '1em' }}
                className='dashboard_segment'>
            <Grid >

                <Grid.Row columns='equal'>

                    <Grid.Column>
                        <Header size='small' dividing>
                            <Icon name='microchip'/>
                            <Header.Content>
                                Motes
                            </Header.Content>
                        </Header>
                    </Grid.Column>

                    <Grid.Column>
                        <Header size='small' dividing>
                            <Icon name='users'/>
                            <Header.Content>
                                Clientes
                            </Header.Content>
                        </Header>
                    </Grid.Column>

                </Grid.Row>

                <Grid.Row columns='equal'>

                    <Grid.Column>
                        <Statistic.Group widths='4' size='small'>

                            <Statistic >
                                <Statistic.Value>
                                    {this.props.mote_summary.nr_active}
                                </Statistic.Value>
                                <Statistic.Label>Motes Ativos</Statistic.Label>
                            </Statistic>

                            <Statistic >
                                <Statistic.Value>
                                    {this.props.mote_summary.nr_inactive}
                                </Statistic.Value>
                                <Statistic.Label>Motes Inativos</Statistic.Label>
                            </Statistic>

                            <Statistic >
                                <Statistic.Value>
                                    {this.props.mote_summary.total_rx_packets}
                                </Statistic.Value>
                                <Statistic.Label>Pacotes Recebidos</Statistic.Label>
                            </Statistic>

                            {this.props.mote_summary.total_errors > 0 ? (
                                <Statistic color='red'>
                                    <Statistic.Value>
                                        <Icon name='exclamation' size='small'/>
                                        {this.props.mote_summary.total_errors}
                                    </Statistic.Value>
                                    <Statistic.Label>Erros</Statistic.Label>
                                </Statistic>
                            ) : (
                                <Statistic color='green'>
                                    <Statistic.Value>
                                        <Icon name='checkmark' size='small'/>
                                        {this.props.mote_summary.total_errors}
                                    </Statistic.Value>
                                    <Statistic.Label>Erros</Statistic.Label>
                                </Statistic>
                            )}

                        </Statistic.Group>
                    </Grid.Column>

                    <Grid.Column>
                        <Statistic.Group widths='4' size='small'>

                            <Statistic >
                                <Statistic.Value>
                                    {this.props.client_summary.nr_users}
                                </Statistic.Value>
                                <Statistic.Label>Utilizadores</Statistic.Label>
                            </Statistic>

                            <Statistic >
                                <Statistic.Value>
                                    {this.props.client_summary.nr_apps}
                                </Statistic.Value>
                                <Statistic.Label>Aplicações</Statistic.Label>
                            </Statistic>

                            <Statistic >
                                <Statistic.Value>
                                    {this.props.client_summary.total_rx_requests}
                                </Statistic.Value>
                                <Statistic.Label>Pedidos Recebidos</Statistic.Label>
                            </Statistic>

                            {this.props.client_summary.total_rejected_reqs > 0 ? (
                                <Statistic  color='red'>
                                    <Statistic.Value>
                                        <Icon name='exclamation' size='small'/>
                                        {this.props.client_summary.total_rejected_reqs}
                                    </Statistic.Value>
                                    <Statistic.Label>Pedidos Falhados</Statistic.Label>
                                </Statistic>
                            ) : (
                                <Statistic color='green'>
                                    <Statistic.Value>
                                        <Icon name='check' size='small'/>
                                        {this.props.client_summary.total_rejected_reqs}
                                    </Statistic.Value>
                                    <Statistic.Label>Pedidos Rejeitados</Statistic.Label>
                                </Statistic>
                            )}

                        </Statistic.Group>
                    </Grid.Column>


                </Grid.Row>

            </Grid>
            </Segment>
        )
    }
}
