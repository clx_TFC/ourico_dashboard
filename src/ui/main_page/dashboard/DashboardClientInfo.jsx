import * as React from 'react'
import {

    Button,
    Dropdown,
    Grid,
    Header,
    Icon,
    Menu,
    Popup,
    Segment,
    Statistic

} from 'semantic-ui-react'

import { Bar } from 'react-chartjs-2'

import * as StaticData    from '../static_data.json'
import * as Settings      from '../../../common/app_settings'
import * as DashboardData from '../../../common/dashboard_data'

import * as Moment from 'moment'


export default class DashboardClientInfo extends React.Component {

    constructor (props) {
        super(props)

        this.state = {
            requests_by: Settings.get(Settings.Dashboard.CLIENT_REQUESTS_CHART_GROUP_BY)
        }
    }

    render () {

        let client_name  = this.props.data.name
        let error_count  = 0
        let request_count = this.props.data.logs.reduce((acc, l) => {
            if (l.level !== 'info') error_count++
            return (acc + 1)
        }, 0)

        let logs = this.props.data.logs.map(p => ({
            ...p, date_time: Moment(p.date_time)
        })).sort((a, b) => {
            if (b.date_time.isAfter(a.date_time)) return -1
            else if (a.date_time.isAfter(b.date_time)) return 1
            else return 0
        })

        let request_per_time = DashboardData.count_data_by_date(
            // count logs by date
            this.state.requests_by, logs, (l) => 1
        )
        let request_types = {}
        this.props.data.logs.forEach((log) => {
            if (request_types[log.operation] === undefined)
                request_types[log.operation] = 0
            else
                request_types[log.operation] += 1
        })

        const requests_per_time_data = {
            labels: request_per_time.labels,
            datasets: [{
                ...StaticData.dashboard.bar_charts_display.green,
                label: 'Número de pedidos',
                data: request_per_time.values
            }]
        }

        const bytes_per_request_data = {
            datasets: [{
                ...StaticData.dashboard.line_charts_display.blue,
                label: 'Frequência de tipos de pedidos',
                data: [] //request_types
            }]
        }

        return (
            <Segment.Group style={{ marginBottom: '2em', display: this.state.display }}>
                <Segment className="dashboard_segment dashboard_segment__header" compact>
                    <Menu secondary fluid>
                        <Menu.Item
                            position='left'
                            fitted
                            className='dashboard_segment__client_name'>
                            <Header as='h5' floated='left'>{client_name}</Header>
                        </Menu.Item>
                        <Menu.Item fitted>
                            <Popup
                                trigger={<Button icon='arrow right' basic floated='right' compact />}
                                content='Ver Dados do Cliente'
                                on='hover'
                            />
                        </Menu.Item>
                        <Menu.Item fitted>
                            <Button icon='x' basic floated='right' compact
                                onClick={this.props.handle_hide} />
                        </Menu.Item>
                    </Menu>
                </Segment>

                <Segment>
                    <Grid>
                        <Grid.Row columns={2}>
                            {/* packets per time(hour, day, week) chart */}
                            <Grid.Column>
                                <span>
                                    Pedidos recebidos por{' '}
                                    <Dropdown
                                        inline
                                        options={StaticData.dashboard.time_chart_grouping_options}
                                        defaultValue={this.state.requests_by}
                                        onChange={(e, { value }) => this.setState({ requests_by: value })}
                                    />
                                </span>
                                <Bar data={requests_per_time_data} height={220}/>
                            </Grid.Column>
                            {/* bytes per packet chart */}
                            <Grid.Column>
                                <Bar data={bytes_per_request_data} height={220}/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={1}>
                            <Grid.Column>
                                <Statistic.Group widths='2' size='tiny'>
                                    <Statistic size='tiny'>
                                        <Statistic.Value content={request_count} />
                                        <Statistic.Label content={'Pedidos recebidos'}/>
                                    </Statistic>
                                    {error_count > 0 ? (
                                        <Statistic color='red' >
                                            <Statistic.Value>
                                                <Icon name='exclamation' size='small'/>
                                                {error_count}
                                            </Statistic.Value>
                                            <Statistic.Label>Pedidos Rejeitados</Statistic.Label>
                                            {/*<Button content='Ver detalhes' basic floated='right' compact />*/}
                                        </Statistic>
                                    ) : (
                                        <Statistic color='green'>
                                            <Statistic.Value>
                                                <Icon name='checkmark' size='small'/>
                                                {error_count}
                                            </Statistic.Value>
                                            <Statistic.Label>Pedidos Rejeitados</Statistic.Label>
                                        </Statistic>
                                    )}
                                </Statistic.Group>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Segment>

            </Segment.Group>
        )
    }

}
