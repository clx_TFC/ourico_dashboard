import * as React  from 'react'
import * as Moment from 'moment'

import Content             from '../Content'
import DateIntervalPicker  from '../DateIntervalPicker'
import DashboardMainInfo   from './DashboardMainInfo'
import DashboardMoteInfo   from './DashboardMoteInfo'
import DashboardClientInfo from './DashboardClientInfo'

import { DataFetchState, date_format } from '../../../common/misc'
import * as DashboardData from '../../../common/dashboard_data'
import * as Settings      from '../../../common/app_settings.js'


import { Dimmer, Grid, Loader, Menu, Message, Tab } from 'semantic-ui-react'

import './dashboard_styles.css'


/**
 * Dashboard section
 */
export default class Dashboard extends React.Component {

    constructor (props) {
        super(props)

        const from = Moment().subtract(8, 'days') // a week ago
        const to   = Moment().add(1, 'days')      // tomorrow

        this.state = {
            data_fetch_state         : DataFetchState.PENDING,
            data_fetch_error         : 'Verifique a sua conexão á Internet',
            data_fetch_interval      : null,

            // get data since this date
            data_from_date           : from,
            // get data until this date
            data_until_date          : to,
            // ids os of motes that user does not want to see
            motes_to_exclude         : {},
            // ids os of clients that user does not want to see
            clients_to_exclude       : {},

            mote_data: {
                summary: {
                    nr_active       : 0,
                    nr_inactive     : 0,
                    total_rx_packets: 0,
                    total_errors    : 0
                },
                motes: []
            },

            client_data: {
                summary: {
                    nr_users           : 0,
                    nr_apps            : 0,
                    total_rx_requests  : 0,
                    total_rejected_reqs: 0
                },
                clients: []
            }
        }
    }


    /**
     * Called when user changes data fetch interval
     * 
     * @param new_dates { startDate, endDate } interval to fetch data
     */
    handle_dates_change = (new_dates) => {
        let start_date = new_dates.startDate !== null ?
            new_dates.startDate.hours(0) : null
        let end_date   = new_dates.endDate !== null ?
            new_dates.endDate.hours(23).minutes(59) : null
        this.setState({
            data_from_date : start_date,
            data_until_date: end_date 
        })
    }


    /**
     * Called when hide button is clicked in mote card
     * 
     * @param mote_id the mote to exclude from the view
     */
    handle_mote_item_hide = (mote_id) => {
        let motes_to_exclude = this.state.motes_to_exclude
        motes_to_exclude[mote_id] = true
        this.setState({ motes_to_exclude: motes_to_exclude })
    }


    /**
     * Called when hide button is clicked in client card
     * 
     * @param client_id the client to exclude from the view
     */
    handle_client_item_hide = (client_id) => {
        let clients_to_exclude = this.state.clients_to_exclude
        clients_to_exclude[client_id] = true
        this.setState({ clients_to_exclude: clients_to_exclude })
    }


    /**
     * Make server request for data
     */
    get_data = async () => {
        try {

            const data = await DashboardData.load(
                date_format(this.state.data_from_date),
                date_format(this.state.data_until_date),
                Object.keys(this.state.motes_to_exclude),
                Object.keys(this.state.clients_to_exclude)
            )
            
            this.setState({
                mote_data       : data.mote_data,
                client_data     : data.client_data,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })


        } catch (exception) {
            console.log(exception)
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
        
    }
    
    async componentDidMount () {
        await this.get_data()
        this.setState({
            data_fetch_interval: setInterval(this.get_data, Settings.get(
                Settings.Dashboard.DATA_REFRESH_RATE
            ))
        })
    }


    componentWillUnmount () {
        clearInterval(this.state.data_fetch_interval)
    }


    render () {
        
        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '4em' }}>
                    <Loader content='A carregar dados'/>
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Content
                    name='Dashboard'
                    with_breadcrumb
                    content={
                        <Message negative style={{ margin: '5em' }}>
                            <Message.Header
                                content='Não foi possivel carregar os dados da Dashboard'
                            />
                            <p>{this.state.data_fetch_error}</p>
                        </Message>
                    }
                />
            )
        }

        const top_right_content = (
            <Menu secondary>
                <Menu.Item>
                    <DateIntervalPicker
                        data_from_date={this.state.data_from_date}
                        data_until_date={this.state.data_until_date}
                        handle_dates_change={this.handle_dates_change}
                        label='Ver dados no intervalo'
                    />
                </Menu.Item>
            </Menu>
        )


        let show_inactive_mote = Settings.get(Settings.Dashboard.SHOW_INACTIVE_MOTES)
        let filter_mote = (mote) =>  (
            (show_inactive_mote || mote.active) && 
            (this.state.motes_to_exclude[mote.id] === undefined)
        )

        // data shown in the pane of mote statistics
        const motes_data_pane = {
            menuItem: 'Motes',
            render: () => (
                <Tab.Pane>
                    <Grid >
                        <Grid.Row columns={2} >
                            {this.state.mote_data.motes
                                .filter(mote => filter_mote(mote))
                                .map(mote => (
                                    <Grid.Column key={mote.id} >
                                        <DashboardMoteInfo
                                            data={mote} 
                                            handle_hide={ () => this.handle_mote_item_hide(mote.id) }
                                        />
                                    </Grid.Column>
                                ))
                            }
                        </Grid.Row>
                    </Grid>
                </Tab.Pane>
            )
        }

        // data shown in the pane of client statistics
        const clients_data_pane = { 
            menuItem: 'Clientes', 
            render: () => (
                <Tab.Pane>
                    <Grid >
                        <Grid.Row columns={2}>
                            {this.state.client_data.clients
                                .filter(client => this.state.clients_to_exclude[client.id] === undefined)
                                .map(client => (
                                    <Grid.Column key={client.id} >
                                        <DashboardClientInfo
                                            data={client}
                                            handle_hide={(e) => this.handle_client_item_hide(client.id, e)}
                                        />
                                    </Grid.Column>
                                ))
                            }
                        </Grid.Row>
                    </Grid>
                </Tab.Pane>
            )
        }

        const the_content = (
            <div style={{ paddingTop: '1em'}}>
                <DashboardMainInfo
                    mote_summary={this.state.mote_data.summary}
                    client_summary={this.state.client_data.summary}
                />
                <Tab
                    panes={[ motes_data_pane, clients_data_pane ]}
                    style={{ marginLeft: '1em', marginRight: '1em' }}
                    className='dashboard_tabbed_content'
                />
            </div>
        )

        return (
            <Content
                name='Dashboard'
                content={the_content}
                top_right_content={top_right_content}
                with_breadcrumb
            />
        )
    }
}
