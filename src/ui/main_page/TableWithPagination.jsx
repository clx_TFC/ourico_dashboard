import React from 'react'

import { 
    Button, 
    Header, 
    Icon,
    Input, 
    Menu, 
    Pagination, 
    Popup,
    Select, 
    Table 
} from 'semantic-ui-react'


/**
 * Table with pagination functionality
 * 
 * @prop {object}   header_fields  column names, with keys to access respective values in data
 * @prop {object[]} data           data to be displayed
 * @prop {object}   footer_fields  items to put in footer
 * @prop {string}   row_key        name of column that should identify a row
 * @prop {number}   items_per_page maximum number of items to show at once
 * @prop {string}   name           name for the table
 * @prop {string}   description    description of the dataset
 * @prop {enum}     actions        what actions to include (possible values: 'edit', 'delete', 'both')
 * @prop {function} handle_edit    called when edit button in clicked
 * @prop {function} handle_delete  called when delete button in clicked
 */
export default class TableWithPagination extends React.Component {
    
    constructor (props) {
        super(props)

        this.state = {
            active_page      : 1,
            // lines filtered by user search
            rows_to_hide : [],
            // attribute to use to filter
            search_by        : this.props.header_fields[0].key
        }
    }


    handle_search = (event, value) => {
        let rows_to_hide = []

        if (value === '' || value === undefined || value === null) {
            console.log('excluding', rows_to_hide)
            this.setState({ rows_to_hide: rows_to_hide })
            return
        }

        this.props.data.forEach(item => {
            let target = item[this.state.search_by]
            if (typeof target === 'string') {
                if (!target.toLocaleLowerCase().includes(value.toLocaleLowerCase()))
                    rows_to_hide.push(item[this.props.row_key])
            } else {
                if (typeof target === 'number')
                    value = +value
                if (target !== value)
                    rows_to_hide.push(item[this.props.row_key])
            }
        })
        console.log('excluding', rows_to_hide)
        this.setState({ rows_to_hide: rows_to_hide })
    }


    new_page = (page) => {
        this.setState({ active_page: page })
    }


    render () {

        let items_to_show   = this.props.data.filter(
            i => !this.state.rows_to_hide.find(r => r === i[this.props.row_key])
        )
        let nr_items_to_show = items_to_show.length
        let nr_pages = Math.ceil(nr_items_to_show/this.props.items_per_page)

        return (
        <div>
            <Menu secondary>
                {this.props.name !== undefined ? (
                    <Menu.Item position='left' >
                        <Header as='h3'>
                            <Header.Content>
                                {this.props.name}
                                {this.props.description !== undefined ? (
                                    <Header.Subheader>
                                        {this.props.description}
                                    </Header.Subheader>
                                ) : (null)}
                            </Header.Content>
                        </Header>
                    </Menu.Item>
                ) : (null)}
                <Menu.Item >
                    <Input
                        action
                        placeholder='Procurar...' 
                        onChange={(e, { value }) => this.handle_search(e, value)} 
                    >
                        <input />
                        <Select
                            onChange={(e, { value }) => this.setState({ search_by: value })}
                            compact
                            options={this.props.header_fields.map(h => ({...h, value: h.key}))} 
                            defaultValue={this.props.header_fields[0].key} />
                    </Input>
                </Menu.Item>
            </Menu>
            <Table celled padded striped  collapsing={this.props.collapsing}>
                <Table.Header>
                    <Table.Row>
                    {this.props.header_fields.map(col => (
                        <Table.HeaderCell key={col.key}>
                            {col.text}
                        </Table.HeaderCell>
                    ))}
                    {this.props.actions !== undefined ? (
                        <Table.HeaderCell>
                            Ações
                        </Table.HeaderCell>
                    ) : (null)}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {items_to_show.length === 0 ? (
                        <Table.Row>
                            <Table.Cell colSpan={this.props.header_fields.length+1} warning textAlign='center' >
                                <i>Sem Dados</i>
                            </Table.Cell>
                        </Table.Row>
                    ) : (items_to_show
                        .slice(
                            (this.state.active_page - 1) * this.props.items_per_page,
                            (this.state.active_page - 1) * this.props.items_per_page + this.props.items_per_page
                        )
                        .map(row =>
                            <Table.Row key={row[this.props.row_key]} {...row.row_props} >
                                {this.props.header_fields.map(c => (
                                    <Table.Cell
                                        key={c.key}
                                        positive={row[`${c.key}_positive`]}
                                        negative={row[`${c.key}_negative`]}
                                    >
                                        {row[c.key]}
                                    </Table.Cell>
                                ))}

                                {this.props.actions !== undefined ? (
                                    (this.props.actions === 'delete' ? (
                                    <Table.Cell collapsing>
                                        <Button.Group compact basic>
                                            <Popup 
                                                trigger={
                                                    <Button icon onClick={row.handle_delete} >
                                                        <Icon name='x' />
                                                    </Button>
                                                }
                                                content='Apagar'
                                            />
                                        </Button.Group>
                                    </Table.Cell>
                                ) : (this.props.actions === 'edit' ? (
                                    <Table.Cell collapsing>
                                        <Button.Group compact basic>
                                            <Popup 
                                                trigger={
                                                    <Button icon onClick={row.handle_edit} >
                                                        <Icon name='edit' />
                                                    </Button>
                                                }
                                                content='Editar'
                                            />
                                        </Button.Group>
                                    </Table.Cell>
                                ) : (
                                    <Table.Cell collapsing>
                                        <Button.Group compact basic>
                                            <Popup 
                                                trigger={
                                                    <Button icon onClick={row.handle_edit} >
                                                        <Icon name='edit' />
                                                    </Button>
                                                }
                                                content='Editar'
                                            />
                                            <Popup 
                                                trigger={
                                                    <Button icon onClick={row.handle_delete} >
                                                        <Icon name='x' />
                                                    </Button>
                                                }
                                                content='Apagar'
                                            />
                                        </Button.Group>
                                    </Table.Cell>
                                )))
                                ) : (null)}
                                
                            </Table.Row>
                    ))}
                </Table.Body>
                <Table.Footer>
                    {this.props.footer_fields !== undefined ? (
                        <Table.Row>
                        {this.props.footer_fields.map(ft => (
                            <Table.HeaderCell {...ft.props} >{ft.content}</Table.HeaderCell>
                        ))}
                        </Table.Row>
                    ) : (null)}
                    <Table.Row>
                        <Table.HeaderCell colSpan={this.props.header_fields.length+1}>
                            <Pagination
                                activePage={this.state.active_page}
                                totalPages={nr_pages}
                                size='mini'
                                onPageChange={(_, { activePage }) => this.new_page(activePage)}
                            />
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        </div>            
        )
    }

}