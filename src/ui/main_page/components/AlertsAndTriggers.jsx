import React from 'react'

import {

    Button,
    Dimmer,
    Grid,
    Icon,
    Loader, 
    Message,
    Modal

} from 'semantic-ui-react'

import { DataFetchState }      from '../../../common/misc'
import * as AlertsTriggersData from '../../../common/alerts_triggers_data'

import TableWithPagination  from '../TableWithPagination'
import AlertAndTriggerModal from './AlertAndTriggerModal'


export default class AlertsAndTriggers extends React.Component {

    ALERTS_TABLE_COLUMNS = [
        { key: 'id',          text: 'Identificador' },
        { key: 'name',        text: 'Nome'          },
        { key: 'description', text: 'Descrição'     },
        { key: 'level',       text: 'Nível'         }
    ]

    TRIGGERS_TABLE_COLUMNS = [
        { key: 'id',          text: 'Identificador' },
        { key: 'name',        text: 'Nome'          },
        { key: 'description', text: 'Descrição'     }
    ]


    constructor (props) {
        super(props)

        this.state = {
            alerts          : [],
            triggers        : [],

            data_fetch_state: DataFetchState.PENDING,
            data_fetch_error: 'Verifique a sua conexão á Internet',

            triggers_modal_open: false,
            alerts_modal_open  : false,
            trigger_to_edit    : undefined,
            alert_to_edit      : undefined,

            // confirm modal state
            confirm_modal_open          : false,
            confirm_modal_header        : '',
            confirm_modal_message       : '',
            confirm_modal_handle_confirm: undefined
        }
    }


    /**
     * Get list of triggers and alerts
     */
    get_data = async () => {
        this.setState({ data_fetch_state: DataFetchState.PENDING })
        try {
            let data = await AlertsTriggersData.load()
            this.setState({
                alerts          : data.alerts,
                triggers        : data.triggers,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    handle_data_change = async () => {
        await this.get_data()
    }


    close_modal = () => {
        this.setState({
            triggers_modal_open: false,
            alerts_modal_open  : false,
            trigger_to_edit    : undefined,
            alert_to_edit      : undefined
        })
    }


    delete_alert = (alert) => {
        this.delete_item(
            alert, 
            'Alerta', 
            _ => AlertsTriggersData.delete_alert(alert.id)
        )
    }

    delete_trigger = (trigger) => {
        this.delete_item(
            trigger, 
            'Trigger', 
            _ => AlertsTriggersData.delete_trigger(trigger.id)
        )
    }


    /**
     * Generic function to confirm action when a delete operation is requested
     * 
     * @param {object}   item          the item to be deleted
     * @param {name}     name          of the model where the item belongs
     * @param {function} server_call function to be called if user confirms action
     */
    delete_item = (item, name, server_call) => {
        let do_delete_item = async () => {
            try {
                let result = await server_call(item)
                if (result === true) {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Sucesso',
                        confirm_modal_message       : `Registo eliminado`,
                        confirm_modal_handle_confirm: () => {
                            this.setState({ confirm_modal_open: false })
                            this.handle_data_change()
                        }
                    })
                } else {
                    // eslint-disable-next-line
                    throw  `Não foi possivel eliminar ${name}`
                }
            } catch (exception) {
                if (typeof exception.error_msg !== 'undefined') {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : exception.error_msg,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                } else {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : `Não foi possivel eliminar ${name}`,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                }
            }   
        }

        this.setState({
            confirm_modal_open          : true,
            confirm_modal_header        : `Tem a certeza que deseja eliminar ${name}`,
            confirm_modal_message       : 'O registo será removido permanentemente',
            confirm_modal_handle_confirm: do_delete_item
        })
    }


    async componentDidMount () {
        await this.get_data()
    }


    render () {

        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Não foi possivel carregar a lista de sensores'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }

        return (
        <div>

            <AlertAndTriggerModal
                open={this.state.alerts_modal_open || this.state.triggers_modal_open}
                when_done={this.handle_data_change}
                handle_close={this.close_modal}
                mode={this.state.alerts_modal_open ? 'alert' : 'trigger'}
                data={this.state.triggers_modal_open ? this.state.trigger_to_edit : this.state.alert_to_edit}
            />

            <Modal 
                style={{ marginTop: '0 !important', marginLeft: 'auto', marginRight: 'auto' }}
                size='mini' 
                open={this.state.confirm_modal_open}
                onClose={() => this.setState({ confirm_modal_open: false })}>
                <Modal.Header>
                    {this.state.confirm_modal_header}
                </Modal.Header>
                <Modal.Content>
                    <p>{this.state.confirm_modal_message}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={() => this.setState({ confirm_modal_open: false })} >Cancelar</Button>
                    <Button onClick={() => this.state.confirm_modal_handle_confirm()}
                        positive icon='checkmark' labelPosition='right' content='Ok' />
                </Modal.Actions>
            </Modal>

            <Grid>
                <Grid.Row style={{ paddingLeft: '1em', paddingRight: '1em' }} divided >

                    <Grid.Column width={8}>
                        <TableWithPagination
                            name='Alertas'
                            items_per_page={10}
                            header_fields={this.ALERTS_TABLE_COLUMNS}
                            row_key='id'
                            data={this.state.alerts.map(alert => ({
                                ...alert,
                                level: ({ WARNING: 'Aviso', NORMAL: 'Normal', CRITICAL: 'Critico' })[alert.level],
                                handle_edit: () => this.setState({
                                    alerts_modal_open: true, alert_to_edit: alert
                                }),
                                handle_delete: () => this.delete_alert(alert)
                            }))}
                            actions='both'
                            footer_fields={[
                                {
                                    props: {
                                        colSpan: this.ALERTS_TABLE_COLUMNS.length + 1,
                                        key: 'add'
                                    },
                                    content: (
                                        <Button 
                                            floated='right' icon 
                                            labelPosition='left' 
                                            primary size='small'
                                            onClick={() => this.setState({
                                                alerts_modal_open: true,
                                                alert_to_edit: undefined
                                            })}
                                        >
                                            <Icon name='plus' /> Novo Alerta
                                        </Button>
                                    )
                                },
                            ]}
                        />
                    </Grid.Column>
                    
                    <Grid.Column width={8}>
                        <TableWithPagination
                            name='Triggers'
                            items_per_page={10}
                            header_fields={this.TRIGGERS_TABLE_COLUMNS}
                            row_key='id'
                            data={this.state.triggers.map(trigger => ({
                                ...trigger,
                                handle_edit: () => this.setState({
                                    triggers_modal_open: true, trigger_to_edit: trigger
                                }),
                                handle_delete: () => this.delete_trigger(trigger)
                            }))}
                            actions='both'
                            footer_fields={[
                                {
                                    props: {
                                        colSpan: this.TRIGGERS_TABLE_COLUMNS.length + 1,
                                        key: 'add'
                                    },
                                    content: (
                                        <Button 
                                            floated='right' icon 
                                            labelPosition='left' 
                                            primary size='small'
                                            onClick={() => this.setState({
                                                triggers_modal_open: true,
                                                trigger_to_edit: undefined
                                            })}
                                        >
                                            <Icon name='plus' /> Novo Trigger
                                        </Button>
                                    )
                                },
                            ]}
                        />
                    </Grid.Column>

                </Grid.Row>
            </Grid>
        </div>
        )
    }

} 