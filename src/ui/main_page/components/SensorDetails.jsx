import React from 'react'

import { Button, Divider, Grid, Header, Icon, Item, Modal } from 'semantic-ui-react'

import TableWithPagination from '../TableWithPagination'
import SensorModal         from './SensorModal'   
import ParameterModal      from './ParameterModal'   

import * as Moment from 'moment'

import * as SensorData from '../../../common/sensor_data'


export default class SensorDetails extends React.Component {

    COLORS = [ 'violet', 'olive', 'teal', 'blue', 'purple', 'yellow', 'green' ]

    PARAMETERS_TABLE_COLUMNS = [
        { text: 'Identificador', key: 'id'          },
        { text: 'Nome',          key: 'name'        },
        { text: 'Unidades',      key: 'units'       },
        { text: 'Descrição',     key: 'description' },
        { text: 'Tipo',          key: 'type_'       }
    ]

    USES_TABLE_COLUMNS = [
        { text: 'Mote',                 key: 'mote_name'     },
        { text: 'Número da interface', key: 'interface_num'  },
        { text: 'Nome da interface',   key: 'interface_name' },
        { text: 'Ativo desde',         key: 'activated_at'   },
        { text: 'Inativo desde',       key: 'deactivated_at' },
        { text: 'Ativo?',              key: 'active'         }
    ]


    constructor (props) {
        super(props)
        
        this.state = {
            sensor_modal_open: false,

            // confirm modal state
            confirm_modal_open          : false,
            confirm_modal_header        : '',
            confirm_modal_message       : '',
            confirm_modal_handle_confirm: undefined
        }
    }



    /**
     * Called to delete the current sensor
     */
    delete_sensor = () => {
        this.delete_item(
            this.props.sensor, 
            'Sensor', 
            _ => SensorData.delete_sensor(this.props.sensor.id)
        )
    }


    /**
     * Called to open modal to edit a parameter
     */
    edit_param = (parameter) => {
        this.setState({
            params_modal_open: true,
            param_to_edit: parameter
        })
    }


    /**
     * Generic function to confirm action when a delete operation is requested
     * 
     * @param {object}   item          the item to be deleted
     * @param {name}     name          of the model where the item belongs
     * @param {function} server_call function to be called if user confirms action
     */
    delete_item = (item, name, server_call) => {
        let do_delete_item = async () => {
            try {
                let result = await server_call(item)
                if (result === true) {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Sucesso',
                        confirm_modal_message       : `Registo eliminado`,
                        confirm_modal_handle_confirm: () => {
                            this.setState({ confirm_modal_open: false })
                            this.props.handle_data_change()
                        }
                    })
                } else {
                    // eslint-disable-next-line
                    throw  `Não foi possivel eliminar ${name}`
                }
            } catch (exception) {
                if (typeof exception.error_msg !== 'undefined') {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : exception.error_msg,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                } else {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : `Não foi possivel eliminar ${name}`,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                }
            }   
        }

        this.setState({
            confirm_modal_open          : true,
            confirm_modal_header        : `Tem a certeza que deseja eliminar ${name}`,
            confirm_modal_message       : 'O registo será removido permanentemente',
            confirm_modal_handle_confirm: do_delete_item
        })
    }



    render() {

        let sensor = this.props.sensor
        console.log('sensor detail data', sensor)

        return (
        <div>

            <SensorModal 
                open={this.state.sensor_modal_open}
                handle_close={() => this.setState({ sensor_modal_open: false })}
                when_done={this.props.handle_data_change}
                data={sensor}
            />
            <ParameterModal 
                sensor={sensor}
                open={this.state.params_modal_open}
                handle_close={() => this.setState({ params_modal_open: false, param_to_edit: undefined })}
                when_done={this.props.handle_data_change}
                data={this.state.param_to_edit}
            />

            <Modal 
                style={{ marginTop: '0 !important', marginLeft: 'auto', marginRight: 'auto' }}
                size='mini' 
                open={this.state.confirm_modal_open}
                onClose={() => this.setState({ confirm_modal_open: false })}>
                <Modal.Header>
                    {this.state.confirm_modal_header}
                </Modal.Header>
                <Modal.Content>
                    <p>{this.state.confirm_modal_message}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={() => this.setState({ confirm_modal_open: false })} >Cancelar</Button>
                    <Button onClick={() => this.state.confirm_modal_handle_confirm()}
                        positive icon='checkmark' labelPosition='right' content='Ok' />
                </Modal.Actions>
            </Modal>
            

            <Grid>

                <Grid.Row style={{ paddingBottom: 0 }} >
                    <Grid.Column width={6} verticalAlign='middle' >
                        <Header  >
                            <Header.Content>
                                {sensor.name}
                                <Header.Subheader content={`#  ${sensor.id}`} />
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <Button.Group compact basic floated='right' size='small'>
                            <Button
                                icon labelPosition='left'
                                onClick={() => this.setState({ sensor_modal_open: true })}
                            >
                                <Icon name='edit' />
                                {'Alterar dados do sensor'}
                            </Button>
                            <Button
                                icon labelPosition='left'
                                onClick={() => this.delete_sensor()}
                            >
                                <Icon name='delete' />
                                {'Eliminar sensor'}
                            </Button>
                        </Button.Group>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}><Divider /></Grid.Column>
                </Grid.Row>


                <Grid.Row>

                    <Grid.Column width={4}>
                        <Item.Group>
                            <Item >
                                <Item.Content>
                                    <Item.Header>Tamanho do output</Item.Header>
                                    <Item.Description>{`${sensor.output_size} bytes`}</Item.Description>
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    </Grid.Column>

                    <Grid.Column width={4}>
                        <Item.Group>
                            <Item >
                                <Item.Content>
                                    <Item.Header>Assíncrono?</Item.Header>
                                    <Item.Description>{sensor.async ? 'Sim' : 'Não'}</Item.Description>
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    </Grid.Column>

                    <Grid.Column width={8}>
                        <Item.Group>
                            <Item >
                                <Item.Content>
                                    <Item.Header>Descrição</Item.Header>
                                    <Item.Description>{sensor.description}</Item.Description>
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    </Grid.Column>

                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <Item.Group>
                            <Item >
                                <Item.Content>
                                    <Item.Header>Formato do output</Item.Header>
                                    {sensor.parameters.length === 0 ? (
                                        <Item.Extra>
                                            Não Definido
                                        </Item.Extra>
                                    ) : (
                                        <Item.Description>
                                            <Button.Group compact>
                                                {sensor.parameters
                                                    .sort((x, y) => x.position > y.position)
                                                    .map((param, i) => (
                                                    <Button
                                                        key={i}
                                                        color={this.COLORS[i % this.COLORS.length]}
                                                        content={param.name}
                                                        label={{ 
                                                            basic: true, 
                                                            content: `${param.size} B`, 
                                                            color: this.COLORS[i % this.COLORS.length] 
                                                        }}
                                                    />
                                                ))}
                                            </Button.Group>
                                        </Item.Description>
                                    )}
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <TableWithPagination
                            items_per_page={10}
                            name='Parâmetros'
                            header_fields={this.PARAMETERS_TABLE_COLUMNS}
                            row_key='id'
                            data={sensor.parameters.map(param => ({
                                ...param,
                                handle_edit: () => this.edit_param(param)
                            }))}
                            actions='edit'
                            footer_fields={[
                                {
                                    props: {
                                        colSpan: this.PARAMETERS_TABLE_COLUMNS.length + 1,
                                        key: 'add'
                                    },
                                    content: (
                                        <Button 
                                            floated='right' icon 
                                            labelPosition='left' 
                                            primary size='small'
                                            onClick={() => this.setState({
                                                params_modal_open: true,
                                                param_to_edit: undefined
                                            })}
                                        >
                                            <Icon name='plus' /> Adicionar Parâmetro
                                        </Button>
                                    )
                                },
                            ]}
                        />
                    </Grid.Column>
                </Grid.Row>


                <Grid.Row>
                    <Grid.Column width={16}>
                        <TableWithPagination
                            items_per_page={10}
                            name='Motes onde está a ser utilizado'
                            header_fields={this.USES_TABLE_COLUMNS}
                            row_key='num'
                            data={sensor.uses.map(su => ({
                                ...su,
                                mote_name      : su.mote.name,
                                interface_num  : su.interface_num === -1 ? '-' : su.interface_num,
                                activated_at   : su.active ? 
                                    Moment(su.activated_at).format('HH:mm DD-MM-YYYY') : '-',
                                deactivated_at : !su.active ? 
                                    Moment(su.deactivated_at).format('HH:mm DD-MM-YYYY') : '-',
                                active         : su.active ? 'Sim' : 'Não',
                                active_positive: su.active,
                                active_negative: !su.active
                            }))}
                        />
                    </Grid.Column>
                </Grid.Row>

            </Grid>
        </div>
        )
    }

}