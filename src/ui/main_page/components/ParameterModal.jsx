import React from 'react'

import FormModal from '../FormModal'

import { Input, Select, TextArea } from 'semantic-ui-react'

import * as SensorData from '../../../common/sensor_data'

export default class ParameterModal extends React.Component {

    FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    type       : 'text',
                    placeholder: 'Nome do parâmetro',
                    control    : Input
                },
                {
                    key        : 'units',
                    label      : 'Unidades',
                    type       : 'text',
                    placeholder: 'Unidades',
                    control    : Input
                },
                { // types
                    key     : 'type_',
                    label   : 'Tipo',
                    required: true,
                    control : Select,
                    options : [
                        { key: 'float',   value: 'float',   text: 'float'   },
                        { key: 'int',     value: 'int',     text: 'int'     },
                        { key: 'string',  value: 'string',  text: 'string'  },
                        { key: 'boolean', value: 'boolean', text: 'boolean' }
                    ]
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                { // bytes produzidos
                    key        : 'size',
                    label      : 'Tamanho',
                    placeholder: 'Bytes produzidos',
                    required   : true,
                    control    : Input,
                    type       : 'number'
                },
                { // types
                    key        : 'position',
                    label      : 'Posição no output do sensor',
                    placeholder: 'A partir de 1',
                    required   : true,
                    control    : Input,
                    type       : 'number'
                },
            ],
            group_key: 'c'
        },
        {
            fields: [
                { // description
                    key     : 'description',
                    label   : 'Descrição',
                    required: false,
                    control : TextArea
                }
            ],
            group_key: 'b'
        }
    ]


    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {

        if (data.posisition < 1) {
            return 'O valor no campo "Posição no output do sensor" deve ser maior que 0'
        }
        if (data.size < 1) {
            return 'O valor no campo "Tamanho" deve ser maior que 0'
        }

        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        try {
            let new_output_size = +this.props.sensor.output_size
            if (!update) {
                new_output_size += (+data.size)
                data.sensor_id   = this.props.sensor.id
            }
            let result = await SensorData.save_parameter(data, this.props.sensor.id, new_output_size, update)
            if (!result) {
                return err_msg
            } else {
                return true
            }

        } catch (exception) {
            console.log(exception)
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }


    render () {
        let title = this.props.data === undefined
            ? `Novo Parâmetro`
            : `Editar parâmetro ${this.props.data.name}`
        
        console.log('this.props.sensor', this.props.sensor)

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }

}