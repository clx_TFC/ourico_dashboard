import React from 'react'

import FormModal from '../FormModal'
import { Input, Radio, TextArea } from 'semantic-ui-react'

import * as SensorData from '../../../common/sensor_data'


export default class SensorModal extends React.Component {

    FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    type       : 'text',
                    placeholder: 'Nome do sensor',
                    control    : Input
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                { // description
                    key     : 'description',
                    label   : 'Descrição',
                    required: false,
                    control : TextArea
                }
            ],
            group_key: 'c'
        },
        {
            fields: [
                { // asynchronous
                    key     : 'async',
                    label   : 'Assíncrono?',
                    required: false,
                    control : Radio,
                    toggle  : true
                }
            ],
            group_key: 'b'
        }
    ]


    /**
     * Commit changes to server
     * 
     * @param {object} data data to save
     */
    save = async (data) => {

        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        if (!update) {
            data.output_size  = 0
            data.async = data.async === undefined || data.async === null ? false : data.async
        }
        
        try {
            
            let result = await SensorData.save_sensor(data, update)
            if (!result) {
                return err_msg
            } else {
                return true
            }

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }


    render () {
        let title = this.props.data === undefined
            ? `Novo sensor`
            : `Editar sensor ${this.props.data.name}`
        
        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={this.FORM_SPEC}
                save={this.save}
                data={this.props.data}
                when_done={this.props.when_done}
            />
        )
    }

}