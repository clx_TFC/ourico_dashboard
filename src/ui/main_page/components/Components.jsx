import React from 'react'


import { Tab }           from 'semantic-ui-react'
import Content           from '../Content'
import Sensors           from './Sensors'
import Actuators         from './Actuators'
import AlertsAndTriggers from './AlertsAndTriggers'

export default class Components extends React.Component {

    render () {

        let sensors_pane = {
            menuItem: 'Sensores',
            render: () => (<Tab.Pane><Sensors /></Tab.Pane>)
        }
        
        let actuators_pane = {
            menuItem: 'Atuadores',
            render: () => (<Tab.Pane><Actuators /></Tab.Pane>)
        }

        let alerts_triggers_pane = {
            menuItem: 'Alertas & Triggers',
            render: () => (<Tab.Pane><AlertsAndTriggers /></Tab.Pane>)
        }


        let the_content = (
            <Tab
                panes={[sensors_pane, actuators_pane, alerts_triggers_pane]}
                style={{ marginTop: '0.5em' }}
                className='dashboard_tabbed_content'
            />
        )

        return (
            <Content name='Componentes' content={the_content} />

        )
    }

}