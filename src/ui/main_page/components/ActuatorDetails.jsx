import React from 'react'

import { Button, Divider, Grid, Header, Icon, Item, Modal } from 'semantic-ui-react'

import TableWithPagination from '../TableWithPagination'
import ActuatorModal       from './ActuatorModal'
import ActionModal         from './ActionModal'

import * as Moment from 'moment'

import * as ActuatorData from '../../../common/actuator_data'


export default class ActuatorDetails extends React.Component {


    ACTIONS_TABLE_COLUMNS = [
        { key: 'name',        text: 'Nome'          },
        { key: 'num',         text: 'Identificador' },
        { key: 'signal',      text: 'Sinal'         },
        { key: 'description', text: 'Descrição'     }
    ]

    USES_TABLE_COLUMNS = [
        { text: 'Mote',                 key: 'mote_name'     },
        { text: 'Número da interface', key: 'interface_num'  },
        { text: 'Nome da interface',   key: 'interface_name' },
        { text: 'Ativo desde',         key: 'activated_at'   },
        { text: 'Inativo desde',       key: 'deactivated_at' },
        { text: 'Ativo?',              key: 'active'         }
    ]


    constructor (props) {
        super(props)

        this.state = {
            actuator_modal_open: false,
            action_modal_open  : false,
            action_to_edit     : undefined,

            // confirm modal state
            confirm_modal_open          : false,
            confirm_modal_header        : '',
            confirm_modal_message       : '',
            confirm_modal_handle_confirm: undefined
        }
    }


    /**
     * Called to delete the current actuator
     * 
     * @param {object} actuator the item to delete
     */
    delete_actuator = (actuator) => {
        this.delete_item(
            this.props.actuator, 
            'Atuador', 
            _ => ActuatorData.delete_actuator(this.props.actuator.id)
        )
    }


    /**
     * Called to delete an action
     */
    delete_action = (action) => {
        this.delete_item(
            action, 
            'Ação', 
            _ => ActuatorData.delete_action(action.num, this.props.actuator.id)
        )
    }


    /**
     * Generic function to confirm action when a delete operation is requested
     * 
     * @param {object} item          the item to be deleted
     * @param {name}   name          of the model where the item belongs
     * @param {function} server_call function to be called if user confirms action
     */
    delete_item = (item, name, server_call) => {
        let do_delete_item = async () => {
            try {
                let result = await server_call(item)
                if (result === true) {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Sucesso',
                        confirm_modal_message       : `Registo eliminado`,
                        confirm_modal_handle_confirm: () => {
                            this.setState({ confirm_modal_open: false })
                            this.props.handle_data_change()
                        }
                    })
                } else {
                    // eslint-disable-next-line
                    throw  `Não foi possivel eliminar ${name}`
                }
            } catch (exception) {
                if (typeof exception.error_msg !== 'undefined') {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : exception.error_msg,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                } else {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : `Não foi possivel eliminar ${name}`,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                }
            }   
        }

        this.setState({
            confirm_modal_open          : true,
            confirm_modal_header        : `Tem a certeza que deseja eliminar ${name}`,
            confirm_modal_message       : 'O registo será removido permanentemente',
            confirm_modal_handle_confirm: do_delete_item
        })
    }


    render () {

        let actuator = this.props.actuator

        return (
        <div>

            <ActuatorModal
                open={this.state.actuator_modal_open}
                handle_close={() => this.setState({ actuator_modal_open: false })}
                when_done={this.props.handle_data_change}
                data={actuator}
            />
            <ActionModal
                open={this.state.action_modal_open}
                handle_close={() => this.setState({ action_modal_open: false })}
                when_done={this.props.handle_data_change}
                data={this.state.action_to_edit}
                actuator={actuator}
            />

            <Modal 
                style={{ marginTop: '0 !important', marginLeft: 'auto', marginRight: 'auto' }}
                size='mini' 
                open={this.state.confirm_modal_open}
                onClose={() => this.setState({ confirm_modal_open: false })}>
                <Modal.Header>
                    {this.state.confirm_modal_header}
                </Modal.Header>
                <Modal.Content>
                    <p>{this.state.confirm_modal_message}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={() => this.setState({ confirm_modal_open: false })} >Cancelar</Button>
                    <Button onClick={() => this.state.confirm_modal_handle_confirm()}
                        positive icon='checkmark' labelPosition='right' content='Ok' />
                </Modal.Actions>
            </Modal>

            <Grid>

                <Grid.Row style={{ paddingBottom: 0 }} >
                    <Grid.Column width={6} verticalAlign='middle' >
                        <Header  >
                            <Header.Content>
                                {actuator.name}
                                <Header.Subheader content={`#  ${actuator.id}`} />
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <Button.Group compact basic floated='right' size='small'>
                            <Button
                                icon labelPosition='left'
                                onClick={() => this.setState({ actuator_modal_open: true })}
                            >
                                <Icon name='edit' />
                                {'Alterar dados do atuador'}
                            </Button>
                            <Button
                                icon labelPosition='left'
                                onClick={() => this.delete_actuator()}
                            >
                                <Icon name='delete' />
                                {'Eliminar atuador'}
                            </Button>
                        </Button.Group>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}><Divider /></Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={8}>
                        <Item.Group>
                            <Item >
                                <Item.Content>
                                    <Item.Header>Descrição</Item.Header>
                                    <Item.Description>{actuator.description}</Item.Description>
                                </Item.Content>
                            </Item>
                        </Item.Group>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <TableWithPagination 
                            name='Ações'
                            items_per_page={10}
                            header_fields={this.ACTIONS_TABLE_COLUMNS}
                            row_key='num'
                            data={actuator.actions.map(action => ({
                                ...action,
                                handle_edit: () => this.setState({
                                    action_modal_open: true,
                                    action_to_edit: action
                                }),
                                handle_delete: () => this.delete_action(action)
                            }))}
                            actions='both'
                            footer_fields={[
                                {
                                    props: {
                                        colSpan: this.ACTIONS_TABLE_COLUMNS.length + 1,
                                        key: 'add'
                                    },
                                    content: (
                                        <Button 
                                            floated='right' icon 
                                            labelPosition='left' 
                                            primary size='small'
                                            onClick={() => this.setState({
                                                action_modal_open: true,
                                                action_to_edit: undefined
                                            })}
                                        >
                                            <Icon name='plus' /> Adicionar Ação
                                        </Button>
                                    )
                                },
                            ]}
                        />
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <TableWithPagination
                            items_per_page={10}
                            name='Motes onde está a ser utilizado'
                            header_fields={this.USES_TABLE_COLUMNS}
                            row_key='num'
                            data={actuator.uses.map(au => ({
                                ...au,
                                mote_name      : au.mote.name,
                                interface_num  : au.interface_num === -1 ? '-' : au.interface_num,
                                activated_at   : au.active ? 
                                    Moment(au.activated_at).format('HH:mm DD-MM-YYYY') : '-',
                                deactivated_at : !au.active ? 
                                    Moment(au.deactivated_at).format('HH:mm DD-MM-YYYY') : '-',
                                active         : au.active ? 'Sim' : 'Não',
                                active_positive: au.active,
                                active_negative: !au.active
                            }))}
                        />
                    </Grid.Column>
                </Grid.Row>

            </Grid>
        </div>
        )
    }

}