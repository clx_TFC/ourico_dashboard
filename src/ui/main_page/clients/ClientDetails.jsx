import React from 'react'

import { Button, Divider, Grid, Header, Icon, Item, Modal, TextArea } from 'semantic-ui-react'

import * as Moment from 'moment'

import TableWithPagination from '../TableWithPagination'
import ClientModal         from './ClientModal'

import * as ClientData from '../../../common/client_data'


export default class ClientDetails extends React.Component {


    LOG_TABLE_COLUMNS = [
        { key: 'level',     text: 'Estado'      },
        { key: 'operation', text: 'Operação'    },
        { key: 'date_time', text: 'Data e Hora' },
        { key: 'message',   text: 'Mensagem'    },
        { key: 'query',     text: 'Ver pedido'  }
    ]

    OPERATION_NAMES = {
        'read'            : 'Leitura',
        'write'           : 'Escrita',
        'update'          : 'Alteração de Dados',
        'delete'          : 'Eliminação de Dados',
        'rt_events'       : 'Eventos RT',
        'mote_instruction': 'Instrução a Mote',
        'control'         : 'Contas & Tokens'
    }


    constructor (props) {
        super(props)

        this.state = {
            query_display_modal_open: false,
            query_to_display        : undefined,

            client_modal_open       : false,

            // confirm modal state
            confirm_modal_open          : false,
            confirm_modal_header        : '',
            confirm_modal_message       : '',
            confirm_modal_handle_confirm: undefined
        }
    }


    /**
     * Called to delete a log entry
     */
    delete_log = (log) => {
        this.delete_item(
            log,
            'Log',
            _ => ClientData.delete_log(log, this.props.client.type)
        )
    }


    /**
     * Called to delete a client
     */
    delete_client = (client) => {
        this.delete_item(
            client,
            'Cliente',
            _ => ClientData.delete_client(client.id, client.type)
        )
    }


    /**
     * Generic function to confirm action when a delete operation is requested
     * 
     * @param {object}   item          the item to be deleted
     * @param {name}     name          of the model where the item belongs
     * @param {function} server_call function to be called if user confirms action
     */
    delete_item = (item, name, server_call) => {
        let do_delete_item = async () => {
            try {
                let result = await server_call(item)
                if (result === true) {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Sucesso',
                        confirm_modal_message       : `Registo eliminado`,
                        confirm_modal_handle_confirm: () => {
                            this.setState({ confirm_modal_open: false })
                            this.props.handle_data_change()
                        }
                    })
                } else {
                    // eslint-disable-next-line
                    throw  `Não foi possivel eliminar ${name}`
                }
            } catch (exception) {
                if (typeof exception.error_msg !== 'undefined') {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : exception.error_msg,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                } else {
                    this.setState({
                        confirm_modal_open          : true,
                        confirm_modal_header        : 'Erro',
                        confirm_modal_message       : `Não foi possivel eliminar ${name}`,
                        confirm_modal_handle_confirm: () => this.setState({ confirm_modal_open: false })
                    })
                }
            }   
        }

        this.setState({
            confirm_modal_open          : true,
            confirm_modal_header        : `Tem a certeza que deseja eliminar ${name}`,
            confirm_modal_message       : 'O registo será removido permanentemente',
            confirm_modal_handle_confirm: do_delete_item
        })
    }


    /**
     * Render user info view
     */
    user_info = (user) => {        
        return (
        <Grid.Row>
            <Grid.Column width={4}>
                <Item.Group>
                    <Item >
                        <Item.Content>
                            <Item.Header>Username</Item.Header>
                            <Item.Description>{user.username}</Item.Description>
                        </Item.Content>
                    </Item>
                </Item.Group>
            </Grid.Column>
            <Grid.Column width={4}>
                <Item.Group>
                    <Item >
                        <Item.Content>
                            <Item.Header>Email</Item.Header>
                            <Item.Description>{user.email}</Item.Description>
                        </Item.Content>
                    </Item>
                </Item.Group>
            </Grid.Column>
        </Grid.Row>
        )
    }


    /**
     * Render app info view
     */
    app_info = (app) => {
        return (
            <Grid.Row>
                <Grid.Column width={6}>
                    <Item.Group>
                        <Item >
                            <Item.Content>
                                <Item.Header>Endereço de Origem Permitido</Item.Header>
                                <Item.Description>
                                    {app.allowed_origin_address !== null ? app.allowed_origin_address : <i>Qualquer</i>}
                                </Item.Description>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>
                <Grid.Column width={9}>
                    <Item.Group>
                        <Item >
                            <Item.Content>
                                <Item.Header>Token de autenticação</Item.Header>
                                <Item.Description>
                                    <TextArea 
                                        style={{ width: '100%', fontFamily: 'monospace' }} 
                                        value={app.access_token}
                                        rows={5}
                                    />
                                </Item.Description>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Grid.Column>
            </Grid.Row>
        )
    }


    render () {

        let client = this.props.client

        console.log('the client', client)

        return (
        <div>

            <ClientModal
                open={this.state.client_modal_open}
                data={client}
                mode={client.type}
                when_done={this.props.handle_data_change}
                handle_close={() => this.setState({ client_modal_open: false })}
            />

            {/* Modal to present operation query */}
            <Modal
                style={{ marginTop: '0 !important', marginLeft: 'auto', marginRight: 'auto' }}
                open={this.state.query_display_modal_open}
                onClose={() => this.setState({ query_display_modal_open: false })}>
                <Modal.Header>Pedido</Modal.Header>
                <Modal.Content>
                    <pre>{this.state.query_to_display}</pre>
                </Modal.Content>
                <Modal.Actions>
                    <Button 
                        onClick={() => this.setState({ query_display_modal_open: false })}
                        icon='checkmark' labelPosition='right' content='Ok' 
                    />
                </Modal.Actions>
            </Modal>

            {/* Modal to confirm delete operation */}
            <Modal
                style={{ marginTop: '0 !important', marginLeft: 'auto', marginRight: 'auto' }}
                size='mini'
                open={this.state.confirm_modal_open}
                onClose={() => this.setState({ confirm_modal_open: false })}>
                <Modal.Header>
                    {this.state.confirm_modal_header}
                </Modal.Header>
                <Modal.Content>
                    <p>{this.state.confirm_modal_message}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={() => this.setState({ confirm_modal_open: false })} >Cancelar</Button>
                    <Button onClick={() => this.state.confirm_modal_handle_confirm()}
                        positive icon='checkmark' labelPosition='right' content='Ok' />
                </Modal.Actions>
            </Modal>
                

            <Grid>
                <Grid.Row style={{ paddingBottom: 0 }} >
                    <Grid.Column width={6} verticalAlign='middle' >
                        <Header  >
                            <Header.Content>{client.name}</Header.Content>
                        </Header>
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <Button.Group compact basic floated='right' size='small'>
                            <Button
                                icon labelPosition='left'
                                onClick={() => this.setState({ client_modal_open: true })}
                            >
                                <Icon name='edit' />
                                {'Alterar dados do cliente'}
                            </Button>
                            <Button
                                disabled={client.admin}
                                icon labelPosition='left'
                                onClick={() => this.delete_client(client)}
                            >
                                <Icon name='delete' />
                                {'Eliminar cliente'}
                            </Button>
                        </Button.Group>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}><Divider /></Grid.Column>
                </Grid.Row>

                {client.type === 'user' ? this.user_info(client) : this.app_info(client)}

                    <Grid.Row>
                        <Grid.Column width={16} >
                            <Item.Group>
                                <Item >
                                    <Item.Content>
                                        <Item.Header>Permissões</Item.Header>
                                        <Item.Description>
                                            <Button.Group compact>
                                                <Button
                                                    color='green' content='Leitura'
                                                    label={{
                                                        basic: true,
                                                        content: (<Icon name='check' />),
                                                        color: 'green'
                                                    }}
                                                />
                                                <Button
                                                    color={client.write_capability ? 'green' : 'red'}
                                                    content='Escrita'
                                                    label={{
                                                        basic: true,
                                                        content: (<Icon name={client.write_capability ? 'check' : 'x'} />),
                                                        color: client.write_capability ? 'green' : 'red'
                                                    }}
                                                />
                                                <Button
                                                    color={client.rt_events_capability ? 'green' : 'red'}
                                                    content='Receber Eventos de Motes'
                                                    label={{
                                                        basic: true,
                                                        content: (<Icon name={client.rt_events_capability ? 'check' : 'x'} />),
                                                        color: client.rt_events_capability ? 'green' : 'red'
                                                    }}
                                                />
                                                <Button
                                                    color={client.compute_capability ? 'green' : 'red'}
                                                    content='Computação'
                                                    label={{
                                                        basic: true,
                                                        content: (<Icon name={client.compute_capability ? 'check' : 'x'} />),
                                                        color: client.compute_capability ? 'green' : 'red'
                                                    }}
                                                />
                                            </Button.Group>
                                        </Item.Description>
                                    </Item.Content>
                                </Item>
                            </Item.Group>
                        </Grid.Column>
                    </Grid.Row>

                <Grid.Row>
                    <Grid.Column width={16}>
                        <TableWithPagination
                            name='Logs'
                            items_per_page={10}
                            header_fields={this.LOG_TABLE_COLUMNS}
                            row_key='date_time'
                            data={client.logs.map(log => ({
                                ...log,
                                row_props: { error: log.level === 'error', warning: log.level === 'warning' },
                                date_time: Moment(log.date_time).format('HH:mm:ss DD-MM-YYYY'),
                                level: log.level === 'info'
                                    ? 'Ok' : (log.level === 'warning' ? 'Aviso' : 'Erro'),
                                operation: this.OPERATION_NAMES[log.operation],
                                handle_delete: () => this.delete_log(log),
                                query: (
                                    <Button
                                        compact basic
                                        content={'Ver Pedido'}
                                        onClick={() => this.setState({
                                            query_display_modal_open: true,
                                            query_to_display        : JSON.stringify(JSON.parse(log.query), null, 3)
                                        })}
                                    />
                                )
                            }))}
                            actions='delete'
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>
        )
    }

}