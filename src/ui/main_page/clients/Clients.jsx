import React from 'react'

import {

    Dimmer,
    Dropdown,
    Grid,
    Icon,
    Input,
    List,
    Loader,
    Message

} from 'semantic-ui-react'

import { DataFetchState } from '../../../common/misc'
import * as ClientData    from '../../../common/client_data'

import ClientDetails from './ClientDetails'
import ClientModal   from './ClientModal'


export default class Clients extends React.Component {


    constructor (props) {
        super(props)

        this.state = {
            data_fetch_state             : DataFetchState.PENDING,
            data_fetch_error             : 'Verifique a sua conexão á Internet',
            client_detail_data_fech_state: null,

            client_to_edit   : undefined,
            client_modal_open: false
        }
    }


    /**
     * Get list of users and apps
     */
    get_data = async () => {
        this.setState({ data_fetch_state: DataFetchState.PENDING })
        try {
            let data = await ClientData.load()
            
            data.users.forEach(client => client.type = 'user')
            data.apps.forEach (client => client.type = 'app')

            this.setState({
                users           : data.users,
                apps            : data.apps,
                data_fetch_state: DataFetchState.DATA_FETCHED
            })
            return data
        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    data_fetch_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ data_fetch_state: DataFetchState.FAILED })
            }
        }
    }


    /**
     * Called when item in the list of clients is clicked
     * Loads the clients logs and presents his data
     * 
     * @param {object} client the client to present
     */
    handle_client_choice = async (client) => {
        this.setState({ client_detail_data_fech_state: DataFetchState.PENDING })

        try {

            if (client.with_details === true) {
                this.setState({
                    client_presenting: client,
                    client_detail_data_fech_state: DataFetchState.DATA_FETCHED
                })
                return // data already fecthed
            }

            let client_details = await ClientData.load_details(client.id, client.type)
            client.with_details = true
            Object.assign(client, client_details)

            this.setState({
                client_presenting: client,
                client_detail_data_fech_state: DataFetchState.DATA_FETCHED
            })

        } catch (exception) {
            if (typeof exception.error_msg !== 'undefined') {
                this.setState({
                    data_fetch_error: exception.error_msg,
                    client_detail_data_fech_state: DataFetchState.FAILED
                })
            } else {
                this.setState({ client_detail_data_fech_state: DataFetchState.FAILED })
            }
        }
    }


    handle_search = (value) => {

    }


    /**
     * Called when data is changed in a sub component
     */
    handle_data_change = async () => {
        let current_id = this.state.client_presenting !== undefined
            ? this.state.client_presenting.id : -1
        this.setState({ client_presenting: undefined })

        let clients = await this.get_data()
        
        if (this.state.current_id !== -1) {
            let new_client_presenting = clients.users.find(u => u.id === current_id)
            if (new_client_presenting !== undefined) {
                this.handle_client_choice(new_client_presenting)
                return
            }
            
            new_client_presenting = clients.apps.find(a => a.id === current_id)
            if (new_client_presenting !== undefined) {
                this.handle_client_choice(new_client_presenting)
                return
            }
        }
    }



    async componentDidMount () {
        await this.get_data()
    }


    render () {

        if (this.state.data_fetch_state === DataFetchState.PENDING) {
            return (
                <Dimmer active inverted style={{ marginTop: '10em' }}>
                    <Loader content='A carregar dados' />
                </Dimmer>
            )
        } else if (this.state.data_fetch_state === DataFetchState.FAILED) {
            return (
                <Message negative style={{ margin: '5em' }}>
                    <Message.Header
                        content='Não foi possivel carregar a lista de clientes'
                    />
                    <p>{this.state.data_fetch_error}</p>
                </Message>
            )
        }
        
        let apps_to_show  = this.state.apps
        let users_to_show = this.state.users

        return (
        <div style={{ marginTop: '2em' }}>

            <ClientModal
                open={this.state.client_modal_open}
                mode={this.state.client_modal_mode}
                when_done={this.handle_data_change}
                handle_close={() => this.setState({ client_modal_open: false })}
            />

            <Grid divided>
                <Grid.Row>

                    <Grid.Column width={5} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                        <span>
                            <Input 
                                iconPosition='left' 
                                placeholder='Procurar...' 
                                onChange={(_, { value }) => this.handle_search(value)} 
                            >
                                <Icon name='search' />
                                <input />
                            </Input>
                            <Dropdown text='Novo Cliente' icon='plus' labeled button className='icon new_user_bt'>
                                <Dropdown.Menu>
                                    <Dropdown.Item 
                                        icon='user' text='Utilizador'
                                        onClick={() => this.setState({ client_modal_open: true, client_modal_mode: 'user' })}
                                    />
                                    <Dropdown.Item 
                                        icon='file code outline' text='Aplicação' 
                                        onClick={() => this.setState({ client_modal_open: true, client_modal_mode: 'app' })}
                                    />
                                </Dropdown.Menu>
                            </Dropdown>
                        </span>

                        <List divided relaxed selection >
                            {users_to_show.map(client => (
                                <List.Item 
                                    active={this.state.client_presenting !== undefined && this.state.client_presenting.id === client.id}
                                    key={client.id}
                                    onClick={() => this.handle_client_choice(client)}
                                >
                                    <List.Icon name={client.admin ? 'user circle' : 'user'} size='large' verticalAlign='middle'/>
                                    <List.Content>
                                        <List.Header>{client.name}</List.Header>
                                        <List.Description>{client.email}</List.Description>
                                    </List.Content>
                                </List.Item>
                            ))}
                            {apps_to_show.map(client => (
                                <List.Item 
                                    active={this.state.client_presenting !== undefined && this.state.client_presenting.id === client.id}
                                    key={client.id}
                                    onClick={() => this.handle_client_choice(client)}
                                >
                                    <List.Icon name='file code outline' size='large' verticalAlign='middle'/>
                                    <List.Content>
                                        <List.Header>{client.name}</List.Header>
                                        <List.Description>{client.allowed_origin_address}</List.Description>
                                    </List.Content>
                                </List.Item>
                            ))}
                        </List>
                    </Grid.Column>

                    <Grid.Column width={11} style={{ paddingLeft: '2em', paddingRight: '2em' }}>
                        {this.state.client_detail_data_fech_state === DataFetchState.PENDING ? (
                            <Dimmer active inverted style={{ marginTop: '10em' }}>
                                <Loader content='A carregar dados' />
                            </Dimmer>
                        ) : (this.state.client_detail_data_fech_state === DataFetchState.FAILED ? (
                            <Message negative style={{ margin: '5em' }}>
                                <Message.Header
                                    content='Não foi possivel carregar os dados do cliente'
                                />
                                <p>{this.state.data_fetch_error}</p>
                            </Message>
                        ) : (this.state.client_presenting !== undefined ? (
                            <ClientDetails
                                client={this.state.client_presenting}
                                handle_data_change={this.handle_data_change}
                            />
                        ) : (
                            <Message info icon>
                                <Icon name='users' />
                                <Message.Content>
                                    <Message.Header content='Clientes' />
                                    <p>{'Clique num item na lista para ver seus dados'}</p>
                                </Message.Content>
                            </Message>
                        )))}
                    </Grid.Column>

                </Grid.Row>
            </Grid>
        </div>
        )

    }

}