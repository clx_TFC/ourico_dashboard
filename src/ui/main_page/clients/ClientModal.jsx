import React from 'react'

import { Input, Select } from 'semantic-ui-react'

import FormModal from '../FormModal'

import * as ClientData from '../../../common/client_data'
import * as Control    from '../../../api/control'

export default class ClientModal extends React.Component {

    USER_FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    dont_update: true,
                    type       : 'text',
                    placeholder: 'Nome do cliente',
                    control    : Input
                },
                {
                    key        : 'email',
                    label      : 'Email',
                    required   : true,
                    dont_update: true,
                    type       : 'email',
                    placeholder: 'Email',
                    control    : Input
                }
                
            ],
            group_key: 'a'
        },
        {
            fields: [
                {
                    key        : 'username',
                    label      : 'Username',
                    required   : true,
                    dont_update: true,
                    type       : 'text',
                    placeholder: 'Nome de utilizador',
                    control    : Input
                },
                {
                    key        : 'password',
                    label      : 'Password',
                    required   : true,
                    type       : 'password',
                    placeholder: 'Palavra passe',
                    control    : Input
                },
            ],
            group_key: 'b'
        },
        {
            fields: [
                {
                    key        : 'capabilities',
                    label      : 'Permissões',
                    required   : true,
                    placeholder: 'Permissões',
                    control    : Select,
                    multiple   : true,
                    options    : [
                        { key: 'write_capability',     value: 'write_capability',     text: 'Escrita'                  },
                        { key: 'rt_events_capability', value: 'rt_events_capability', text: 'Receber eventos de Motes' },
                        { key: 'compute_capability',   value: 'compute_capability',    text: 'Computação'              }
                    ]
                }
            ],
            group_key: 'c'
        }
    ]

    APP_FORM_SPEC = [
        {
            fields: [
                {
                    key        : 'name',
                    label      : 'Nome',
                    required   : true,
                    dont_update: true,
                    type       : 'text',
                    placeholder: 'Nome da aplicação',
                    control    : Input
                }
            ],
            group_key: 'a'
        },
        {
            fields: [
                {
                    key        : 'capabilities',
                    label      : 'Permissões',
                    required   : true,
                    placeholder: 'Permissões',
                    control    : Select,
                    multiple   : true,
                    options: [
                        { key: 'write_capability', value: 'write_capability', text: 'Escrita' },
                        { key: 'rt_events_capability', value: 'rt_events_capability', text: 'Receber eventos de Motes' },
                        { key: 'compute_capability', value: 'compute_capability', text: 'Computação' }
                    ]
                }
            ],
            group_key: 'c'
        }
    ]


    /**
     * Commit changes to server
     */
    save = async (data) => {
        let update = this.props.data !== undefined
        let err_msg = update
            ? 'Não foi possivel atualizar os dados'
            : 'Não foi possível guardar os dados'

        try {

            if (this.props.mode === 'user') {
                if (!update) {
                    data.admin = false
                    if (!Control.is_valid_password(data.password)) {
                        return ('A password deve ter pelo menos 8 caracteres, ' +
                            'ter números, letras minúsculas e letras maiúsculas')
                    }
                } else { // update
                    if (data.password !== 'placeholder' && !Control.is_valid_password(data.password)) {
                        return ('A password deve ter pelo menos 8 caracteres, ' +
                            'ter números, letras minúsculas e letras maiúsculas')
                    } else if (data.password === 'placeholder') {
                        // user does not want to update password
                        delete data.password
                    }
                }
            }

            data.write_capability     = data.capabilities.includes('write_capability')
            data.rt_events_capability = data.capabilities.includes('rt_events_capability')
            data.compute_capability   = data.capabilities.includes('compute_capability')
            delete data.capabilities

            let result = await ClientData.save_client(data, this.props.mode, update)
            if (!result) {
                return err_msg
            } else {
                return true
            }

        } catch (exception) {
            console.log(exception)
            if (typeof exception.error_msg !== 'undefined') {
                return exception.error_msg
            } else {
                return err_msg
            }
        }
    }


    render () {
        let title
        let form_spec

        let data = undefined

        if (this.props.mode === 'user') {
            title = this.props.data === undefined
                ? `Novo utilizador`
                : `Editar utilizador ${this.props.data.name}`
            form_spec = this.USER_FORM_SPEC
        } else {
            title = this.props.data === undefined
                ? `Nova aplicação`
                : `Editar aplicação ${this.props.data.name}`
            form_spec = this.APP_FORM_SPEC
        }

        if (this.props.data !== undefined) {
            let caps = []
            data = {}
            data.id = this.props.data.id
            if (this.props.data.write_capability)     caps.push('write_capability')
            if (this.props.data.rt_events_capability) caps.push('rt_events_capability')
            if (this.props.data.compute_capability)    caps.push('compute_capability')
            data.capabilities = caps
            data.name         = this.props.data.name
            if (this.props.mode === 'user') {
                data.username = this.props.data.username
                data.password = 'placeholder'
                data.email    = this.props.data.email
            }
        }

        return (
            <FormModal
                open={this.props.open}
                handle_close={this.props.handle_close}
                title={title}
                form_groups={form_spec}
                save={this.save}
                data={data}
                when_done={this.props.when_done}
            />
        )
    }

}