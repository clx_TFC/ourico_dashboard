import * as React from 'react'

import HeaderBar from './HeaderBar'
import { BrowserRouter as Router, Route } from 'react-router-dom'


import Dashboard     from './dashboard/Dashboard'
import Motes         from './Motes'
import Components    from './components/Components'
import Clients       from './clients/Clients'
import ExtensionPack from './extension_pack/ExtensionPack'

import './styles.css'

import * as StaticData from './static_data.json'
import * as RtEvents   from '../../api/rt_events'



export default class MainPage extends React.Component {
    
    constructor (props) {
        super(props)
        this.state = {
            sections      : StaticData.main_page.sections,
            active_section: StaticData.main_page.default_active_section
        }
    }

    async componentDidMount () {
        RtEvents.connect(() => RtEvents.sub('all'))
    }

    componentWillUnmount () {
        RtEvents.disconnect()
    }

    render () {
        return (
            <Router>
                <div>
                    <HeaderBar
                        sections={this.state.sections}
                        active={this.state.active_section}
                        logo={this.props.logo}
                        handle_logged_out={this.props.handle_logged_out}
                    />
                    <div>
                        <Route exact path="/" component={Motes} />
                        <Route path="/dashboard" component={Dashboard} />
                        <Route path="/motes" component={Motes} />
                        <Route path="/components" component={Components} />
                        <Route path="/clients" component={Clients} />
                        <Route path="/extension_pack" component={ExtensionPack} />
                    </div>
                </div>
            </Router>
        )
    }
}