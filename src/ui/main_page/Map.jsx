import React    from 'react'
import ReactDOM from 'react-dom'

import { Message } from 'semantic-ui-react'


/**
 * Google maps view
 * 
 * @prop {string}   map_id  identifier for the map container
 * @prop {number}   zoom    default zoom on the map
 * @prop {number}   width   width of the view
 * @prop {number}   height  height of the view
 * @prop {object}   center  central position of the map (lat and lng)
 * @prop {object[]} markers markers to show in map
 */
export default class Map extends React.Component {

    componentDidMount () {

        if (window.google === undefined) {
            ReactDOM.render((
                <Message negative><Message.Header>
                    Mapa não está disponivel
                </Message.Header></Message>
            ), document.getElementById(this.props.map_id))
            return
        }

        const map = new window.google.maps.Map(
            document.getElementById(this.props.map_id),
            {
                center: { lat: this.props.center.lat, lng: this.props.center.lng },
                zoom  : this.props.zoom
            }
        )
        const overlay = new window.google.maps.OverlayView()
        overlay.draw  = () => {}
        overlay.setMap(map)

        if (this.props.markers !== undefined) {
            var markers = this.props.markers.map(m => {
                let marker = new window.google.maps.Marker({
                    position : { lat: m.lat, lng: m.lng },
                    map      : map,
                    label    : m.label,
                    draggable: m.draggable,
                    icon     : m.icon
                })

                if (m.handle_click !== undefined)
                    marker.addListener('click', (e) => m.handle_click(e, overlay))
                return marker
            })
            this.setState({ markers: markers })
        }

        this.setState({ map: map })
    }

    render () {
        let div_style = {
            width       : this.props.width,
            height      : this.props.height,
            border      : '1px solid rgba(1,1,1,0.2)',
            borderRadius: '3px'
        }
        return (
            <div id={this.props.map_id} style={div_style} ></div>
        )
    }

}