import React from 'react'

import { Button, Dropdown, Form, Icon, Message, Modal, Select, Radio } from 'semantic-ui-react'


/**
 * View to create or update an item
 * 
 * @prop {string}   title        title for the modal
 * @prop {number}   data         data to update (in case of update)
 * @prop {function} save         function to be called when user wants to save
 * @prop {function} form_groups  list of form groups
 * @prop {string}   open         indicates mote visibility
 * @prop {function} handle_close called when user requests to close the modal
 */
export default class FormModal extends React.Component {

    constructor (props) {
        super(props)

        this.state = {
            data     : { },
            
            // executing update or write
            executing: false,

            // to show success message
            done     : false,
            done_open: false,

            // error handling 
            with_error   : false,
            error_header : '',
            error_message: '',
            errors       : {}
        }
    }


    /**
     * Reset modal state
     */
    reset_state = () => {
        console.log('form modal reset state')
        this.setState({
            data     : { ...this.props.data },
            executing: false,
            done     : false,
            done_open: false,
            with_error   : false,
            error_header : '',
            error_message: '',
            errors       : {}
        })
    }

    static getDerivedStateFromProps (new_props, old_props) {
        if (new_props.open) {
            return ({
                data         : { ...new_props.data },
                executing    : false,
                done         : false,
                done_open    : false,
                with_error   : false,
                error_header : '',
                error_message: '',
                errors       : {},
                update_mode  : new_props.data !== undefined
            })
        } else {
            return null
        }
    }


    /**
     * Save changes
     */
    save = async () => {

        let is_empty = (val) =>
            (val === undefined || val === null || val === '')

        for (let group of this.props.form_groups) {
            for (let field of group.fields) {
                if (field.required && is_empty(this.state.data[field.key])) {
                    this.setState({
                        with_error: true,
                        errors: { [field.key]: true },
                        error_message: `O campo "${field.label}" é obrigatório`
                    })
                    return
                }
            }
        }

        this.setState({ executing: true })

        let result = await this.props.save(this.state.data)
        console.log('the result', result)
        if (result === true) {
            this.setState({ done: true, done_open: true, executing: false })
        } else {
            this.setState({
                error_message: result,
                with_error   : true,
                executing    : false
            })
        }
    }


    render () {
        let modal_style = {
            marginTop: '0 !important',
            marginLeft: 'auto',
            marginRight: 'auto',
        }


        if (this.state.done) {
            return (
                <Modal
                    closeIcon
                    style={modal_style}
                    open={this.state.done_open}
                    onClose={() => this.setState({ done_open: false })}
                    header='Dados guardados'
                    content=''
                    actions={[
                        {
                            key: 'ok', content: 'Ok', positive: true, 
                            onClick: () =>  this.setState({ done_open: false, is_open: false }, () => {
                                this.props.handle_close()
                                if (this.props.when_done !== undefined)
                                    this.props.when_done() 
                            })
                        },
                    ]}
                />
            )
        }

        return (
        <Modal 
            dimmer='blurring' 
            open={this.props.open} 
            style={modal_style}
            onClose={() => {
                this.setState({ is_open: false })
                this.props.handle_close()
            }}
            onOpen={() => this.reset_state()}
        >
            <Modal.Header >{this.props.title}</Modal.Header>
            <Modal.Content>
                <Form>
                    {this.props.form_groups.map(group => (
                        <Form.Group widths='equal' key={group.group_key} >
                            {group.fields.map(field => (
                                field.control === Radio ? (
                                    <Form.Radio
                                        label={field.label}
                                        error={this.state.errors[field.key]}
                                        placeholder={field.placeholder}
                                        toggle={field.toggle}
                                        key={field.key}
                                        disabled={this.state.update_mode && field.dont_update}
                                        defaultChecked={
                                            this.props.data !== undefined && this.props.data[field.key] !== undefined
                                                ? (this.props.data[field.key] === true)
                                                : undefined
                                        }
                                        onChange={(_, { checked }) =>
                                                this.setState({ data: { ...this.state.data, [field.key]: checked } })}
                                    />
                                ) : (
                                    <Form.Field
                                        fluid
                                        label={field.label}
                                        error={this.state.errors[field.key]}
                                        required={field.required === true}
                                        control={field.control}
                                        type={field.type}
                                        disabled={this.state.update_mode && field.dont_update}
                                        placeholder={field.placeholder}
                                        key={field.key}
                                        defaultValue={
                                            this.props.data !== undefined && this.props.data[field.key] !== undefined
                                                ? this.props.data[field.key]
                                                : undefined
                                        }
                                        onChange={(_, { value }) =>
                                                this.setState({ data: { ...this.state.data, [field.key]: value } })}
                                        onClick={field.control === Button ? field.handle_click : undefined}
                                        
                                        multiple={field.control === Select || field.control === Dropdown 
                                            ? field.multiple : undefined}
                                        options={field.control === Select || field.control === Dropdown 
                                            ? field.options : undefined}
                                    />
                                )
                            ))}
                        </Form.Group>
                    ))}
                </Form>
                <Message
                    key='error_message'
                    attached='bottom' error hidden={!this.state.with_error}
                    header={this.state.error_header}
                    content={this.state.error_message}
                />
                <Message icon hidden={!this.state.executing} attached='bottom' >
                    <Icon name='circle notched' loading />
                    <Message.Content key='loading_msg_content'>
                        <Message.Header>A executar operação</Message.Header>
                        Espere um momento...
                    </Message.Content>
                </Message>
            </Modal.Content>
            <Modal.Actions>
                <Button basic color='red' onClick={this.props.handle_close} 
                    disabled={this.state.executing} >
                    <Icon name='remove' /> Sair
                </Button>
                <Button color='green' onClick={this.save}
                    disabled={this.state.executing} >
                    <Icon name='checkmark' /> Guardar
                </Button>
            </Modal.Actions>
        </Modal>
        )
    }

}