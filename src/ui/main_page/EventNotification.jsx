import React from 'react'

import { Button, Header, Icon, Grid, Popup } from 'semantic-ui-react'


/**
 * An item in the event notification list
 *
 * @prop {object} data data associated with the specific type of event
 */
export default class EventNotificaton extends React.Component {

    /**
     * Attributes to render each type of event
     */
    event_attrs = {
        'alert': {
            name: 'Alerta',
            icon: 'idea'
        },
        'alert_change_state': {
            name: 'Estado de alerta alterado',
            icon: 'idea'
        },
        'trigger': {
            name: 'Trigger',
            icon: 'idea'
        },
        'trigger_change_state': {
            name: 'Estado de trigger alterado',
            icon: 'idea'
        },
        'measurements': {
            name: 'Medições',
            icon: 'thermometer three quarters'
        },
        'status_report': {
            name: 'Relatório de estado',
            icon: 'file alternate outline'
        },
        'packet_error': {
            name: 'Erro',
            icon: 'warning'
        },
        'action_result': {
            name: 'Resultado de Ação',
            icon: 'error'
        },
        'mote_error': {
            name: 'Erro',
            icon: 'warning'
        },
        'mote_active': {
            name: 'Mote Ativo',
            icon: 'lightbulb'
        },
        'mote_inactive': {
            name: 'Mote Inativo',
            icon: 'lightbulb outline'
        },
        'sensor_change_state': {
            name: 'Estado de sensor alterado',
            icon: 'compass outline'
        },
        'actuator_change_state': {
            name: 'Estado de atuador alterado',
            icon: 'wrench'
        },
        'conf_set': {
            name: 'Parâmetros de configuração alterados',
            icon: 'cogs'
        }
    }

    render () {
        console.log('event data', this.props.data)
        return (
            <Grid>

                <Grid.Row>
                    <Grid.Column width={1} textAlign='center' verticalAlign='middle'>
                        <Icon name={`${this.event_attrs[this.props.data.data.msg].icon}`} />
                    </Grid.Column>
                    <Grid.Column width={13} verticalAlign='middle'>
                        <Header as='h5'>
                            <Header.Content>
                                {`${this.event_attrs[this.props.data.data.msg].name}`}
                                {this.props.data.extra_data != null && this.props.data.extra_data.name != null ?
                                     ` - ${this.props.data.extra_data.name}` : ''}
                                <Header.Subheader>
                                    {
                                        `${this.props.data.mote.name}, ${this.props.data.rx_date_time}` +
                                        (this.props.data.data.active != null ?
                                            (this.props.data.data.active ? ' - Ativado' : ' - Desativado')
                                            : ''
                                        )
                                    }
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                    {/*<Grid.Column width={1}>
                        <Popup
                            trigger={<Button icon='arrow right' size='mini' basic compact />}
                            content={'Ver detalhes'}
                        />
                    </Grid.Column>*/}
                    <Grid.Column width={1}>
                        <Popup
                            trigger={<Button
                                icon='x' size='mini' basic compact onClick={this.props.event_removed}
                            />}
                            content={'Ignorar'}
                        />
                    </Grid.Column>
                </Grid.Row>

            </Grid>
        )
    }

}