import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import * as Moment from 'moment'

import * as StaticData from './static_data.json'
import * as Control from './api/control'
import * as Common  from './api/common'


import './index.css';
import './ui/semantic/dist/semantic.min.css';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';


window.login_fn = async () => {
    const response = await Control.login('ourico', 'the_password')
    if (response.is_error()) {
        console.warn('failed to login')
        console.warn(response.error_msg)
    } else {
        console.debug('login successful')
        Common.set_token(response.data)
    }
}
// login_fn()

Moment.locale('pt-PT', StaticData.moment_locale_conf)
window.moment = Moment

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
